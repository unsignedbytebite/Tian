///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Runtime/Input.h"
#include "Screen.h"
#include "../Utils/Timer.h"

namespace Tian
{
	class Screen;

	/// <summary>
	/// Blueprint class for rendering engines.
	/// </summary>
	class EngineGFX
	{
	public:
		EngineGFX() : mWindowWidth{ 256 }, mWindowHeight{ 256 }, mName{ "empty" }, mQuit{ false }, mScreen{ NULL } {}

		/// <summary>
		/// Initialise the engine with parameters.
		/// </summary>
		/// <param name="windowWidth">Width of the display window</param>
		/// <param name="windowHeight">Height of the display window</param>
		/// <param name="name">Name to be displayed on the window</param>
		virtual void ini(const us windowWidth = 640, const us windowHeight = 320, const str name = "Tian Framework");

		/// <summary>
		/// Initialise the engine then call execution. A short hand for running and starting an engine instance.
		/// </summary>
		/// <param name="screen">Screen to display</param>
		/// <param name="windowWidth">Width of the display window</param>
		/// <param name="windowHeight">Height of the display window</param>
		/// <param name="name">Name to be displayed on the window</param>
		virtual void iniExecute(Screen* screen, const us windowWidth = 640, const us windowHeight = 320, const str name = "Tian Framework");


		/// <summary>
		/// Core execution of the engine loop. Manages input, update and render calls.
		/// </summary>
		/// <param name="screen">The parent screen to display and render</param>
		virtual void execute(Screen* screen);

		/// <summary>
		/// Impliment when load is ready to occur.
		/// </summary>
		virtual void preload() = 0;

		/// <summary>
		/// Impliment when loading. 
		/// </summary>
		virtual void load() = 0;

		/// <summary>
		/// Impliment when load has completed.
		/// </summary>
		virtual void postload() = 0;

		/// <summary>
		/// Impliment when updating the engine. 
		/// </summary>
		virtual void update() = 0;

		/// <summary>
		/// Impliment when rendering the engine.
		/// </summary>
		virtual void render() = 0;

		/// <summary>
		/// Impliment when releasing resources. This call is empty.
		/// </summary>
		virtual void free();

		/// <summary>
		/// Get the input. Returns the pointer to the input assinged to this engine.
		/// </summary>
		/// <returns>The input handler</returns>
		virtual Input* getInput() { return mInput.get(); };

		/// <summary>
		/// Get the name of the window.
		/// </summary>
		/// <returns>The window name</returns>
		const str getName() const { return mName; }

		/// <summary>
		///  Get the width of the window.
		/// </summary>
		/// <returns>Window width</returns>
		const us getWindowWidth() const { return mWindowWidth; }

		/// <summary>
		/// Get the height of the window. 
		/// </summary>
		/// <returns>Window height</returns>
		const us getWindowHeight() const { return mWindowHeight; }

		/// <summary>
		/// Get the window details as a string. Name and window dimensions.
		/// </summary>
		/// <returns>Window parameters as a string</returns>
		const str getDebugText();

		/// <summary>
		/// Get the delta time between frames. Use get the a mount of time that has passed since the last rendering of a frame.
		/// </summary>
		const float getDelta() { return mDeltaTime; };

		/// <summary>
		/// Get FPS. Get the current frames per second.
		/// </summary>
		const float getFPS() { return 1.f / mDeltaTime; };

		/// <summary>
		/// Returns if the engine is to end.
		/// </summary>
		/// <returns>Core loop quit flag</returns>
		const bool getQuit() const { return mQuit; }

		/// <summary>
		/// Set the engine to quit. When true will begin ending the core loop of the engine and free managed resources.
		/// </summary>
		/// <param name="val">Quit switch flag</param>
		void setQuit(const bool val) { mQuit = val; }
	protected:
		us mWindowWidth; ///< Width of the display window.
		us mWindowHeight = 0; ///< Height of the display window.
		str mName; ///< Window name. Presented as the window name.
		shared_ptr<Input> mInput; ///< Input controller. Controller for the mouse and keyboard.
		Screen* mScreen; ///< The current screen to be presented in the window.
		Timer mTimer; ///< Timer for delta time. Works using the SDL's get tick.
		float mDeltaTime; ///< The current stored delta time.
		bool mQuit; ///< Engine quit switch.

		/// <summary>
		/// Impliment when starting the render process.
		/// </summary>
		virtual void start() = 0;

		/// <summary>
		/// Impliment when ending the render process.
		/// </summary>
		virtual void end() = 0;
	};
}

