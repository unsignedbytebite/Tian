#include "Texture.h"

namespace Tian
{
	Texture::Texture(const str path, SDL_Renderer* renderer)
	{
		loadFromFile(path, renderer);
	}

	Texture::~Texture()
	{
		free();
	}

	void Texture::loadFromFile(const str path, SDL_Renderer* renderer)
	{
		// Get rid of preexisting texture
		//free();

		// The final texture
		SDL_Texture* newTexture = NULL;

		//Load image at specified path
		SDL_Surface* loadedSurface = IMG_Load(path.c_str());
		if (loadedSurface == NULL)
		{
			throw Error("Texture cannot be loaded: " + path, __func__);
		}
		else
		{
			mPath = path;

			// Color key alpha
			SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0xFF, 0, 0xFF));

			//Create texture from surface pixels
			newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
			if (newTexture == NULL)
			{
				throw Error("Texture cannot be created from surface: " + path, __func__);
			}
			else
			{
				//Get image dimensions
				mWidth = loadedSurface->w;
				mHeight = loadedSurface->h;
			}

			//Get rid of old loaded surface
			SDL_FreeSurface(loadedSurface);

			// Set blend mode
			SDL_SetTextureBlendMode(newTexture, SDL_BLENDMODE_BLEND);
		}

		// Return success
		mTex = newTexture;
	}

	void Texture::free()
	{
		//Free texture if it exists
		if (mTex != NULL)
		{
			SDL_DestroyTexture(mTex);
			mTex = NULL;
		}
	}

	void Texture::render(SDL_Renderer* renderer, const int x, const int y)
	{
		// Render clip size
		const SDL_Rect clip{ x, y, mWidth, mHeight };

		//Render to screen
		SDL_RenderCopyEx(renderer, mTex, NULL, &clip, 0, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
	}

	void Texture::render(SDL_Renderer* renderer, const Vector2& location)
	{
		render(renderer, (int)location.x, (int)location.y);
	}

	void Texture::render(SDL_Renderer* renderer, const SDL_Rect& clip)
	{
		//Render to screen
		SDL_RenderCopyEx(renderer, mTex, NULL, &clip, 0, NULL, SDL_RendererFlip::SDL_FLIP_NONE);
	}

	void Texture::render(SDL_Renderer* renderer, const int x, const int y, const SDL_Rect* mask, const float angle, const Point& pivot, const SDL_RendererFlip flip)
	{
		// Render clip size
		const SDL_Rect clip{ x, y, mask->w, mask->h };

		//Render to screen
		SDL_RenderCopyEx(renderer, mTex, mask, &clip, angle, &pivot, flip);
	}

	const int Texture::getWidth() const
	{
		return mWidth;
	}

	const int Texture::getHeight() const
	{
		return mHeight;
	}

	void Texture::create(int width, int height, SDL_Renderer* renderer, SDL_TextureAccess access)
	{
		//Create uninitialized texture
		mTex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, access, width, height);

		mWidth = width;
		mHeight = height;

		SDL_SetTextureBlendMode(mTex, SDL_BLENDMODE_BLEND);
	}

	void Texture::startTarget(SDL_Renderer* renderer)
	{
		SDL_SetRenderTarget(renderer, mTex);
	}

	void Texture::stopTarget(SDL_Renderer * renderer)
	{
		SDL_SetRenderTarget(renderer, NULL);
	}

	void Texture::clear(SDL_Renderer* renderer)
	{
		startTarget(renderer);
		SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
		SDL_RenderFillRect(renderer, NULL);
		stopTarget(renderer);
	}

	void Texture::setColour(const Uint8 r, const Uint8 g, const Uint8 b)
	{
		mColour = { r, g, b };
		SDL_SetTextureColorMod(mTex, r, g, b);
	}

	void Texture::setColour(const SDL_Color colourRGBA)
	{
		mColour = colourRGBA;
		SDL_SetTextureColorMod(mTex, colourRGBA.r, colourRGBA.g, colourRGBA.b);
		SDL_SetTextureAlphaMod(mTex, colourRGBA.a);
	}
	SDL_Color Texture::getColour() const
	{
		return mColour;
	}
	void Texture::moveColour(const char r, const char g, const char b, const char a)
	{
		mColour.a += a;
		mColour.r += r;
		mColour.g += g;
		mColour.b += b;
		SDL_SetTextureColorMod(mTex, mColour.r, mColour.g, mColour.b);
		SDL_SetTextureAlphaMod(mTex, mColour.a);
	}
}
