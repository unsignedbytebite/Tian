#include "TextureAnimated.h"

namespace Tian
{
	TextureAnimated::TextureAnimated()
	{}

	TextureAnimated::TextureAnimated(const str texanimPath, SDL_Renderer* renderer)
	{
		// Load frameset data
		FileIO file{ texanimPath, FileIO::READ };
		file.start();
		const str texPath = file.read<str>();
		mFrameSets.serializeIn(file);
		file.end();

		// Load texture data
		loadFromFile(texPath, renderer);
		mPath = texanimPath;
	}

	TextureAnimated::TextureAnimated(Texture* texture, const FrameSets frameSets) : mTexture(texture), mFrameSets(frameSets)
	{}
	void TextureAnimated::loadFromFile(const str path, SDL_Renderer* renderer)
	{
		mTexture = std::shared_ptr<Texture>(new Texture{ path, renderer });
		mPath = path;
	}
	void TextureAnimated::addFrameSet(FrameSet set)
	{
		mFrameSets.add(set);
	}
	const us TextureAnimated::getCurrentFrameSet()
	{
		return mCurrentFrameSet;
	}
	void TextureAnimated::setCurrentFrameSet(const us index)
	{
		mCurrentFrameSet = index;
	}
	FrameSet& TextureAnimated::currentFrameSet()
	{
		return mFrameSets.at(mCurrentFrameSet);
	}
	FrameSets TextureAnimated::getFrameSets() const
	{
		return mFrameSets;
	}
	FrameSets& TextureAnimated::frameSets()
	{
		return mFrameSets;
	}
	void TextureAnimated::setFrameSets(const FrameSets sets)
	{
		mFrameSets = sets;
	}
	void TextureAnimated::render(SDL_Renderer* renderer, const Vector2& location, const float angle, const SDL_Point& pivot, const SDL_RendererFlip flip)
	{
		mFrameSets.at(mCurrentFrameSet).step();
		const int x = (const int)(location.getX());
		const int y = (const int)(location.getY());
		render(renderer, x, y, angle, pivot, flip);
	}
	void TextureAnimated::render(SDL_Renderer* renderer, const int x, const int y, const float angle, const SDL_Point& pivot, const SDL_RendererFlip flip)
	{
		mFrameSets.at(mCurrentFrameSet).step();
		mTexture->render(renderer, x, y, mFrameSets.at(mCurrentFrameSet).current(), angle, pivot, flip);
	}
	Texture* TextureAnimated::textureData() const
	{
		return mTexture.get();
	}
	void TextureAnimated::setAnimationPaused(const bool pause)
	{
		mFrameSets.at(mCurrentFrameSet).setPause(pause);
	}
	void TextureAnimated::setFrameDelay(const us delay)
	{
		for (auto& set : mFrameSets.get())
		{
			set.setFrameDelay(delay);
		}
	}
	void TextureAnimated::serializeOut(FileIO & file) const
	{
		// The path to the texture
		file.write(mTexture.get()->getPath());

		// The framesets
		mFrameSets.serializeOut(file);
	}
}
