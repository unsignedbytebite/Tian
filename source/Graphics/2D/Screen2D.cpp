#include "Screen2D.h"

namespace Tian
{
	Screen2D::Screen2D(EngineGFX2D* engine, AssetManager* assets) : Screen(engine, assets), mCurrentCamera{ 0 }
	{
		// Create the default camera
		createCamera(new Camera2D(*engine->getRenderer(),
			engine->getWindowWidth(),
			engine->getWindowHeight()));
	}
	Screen2D::Screen2D(ScreenManager* manager, AssetManager* assets) : Screen(manager, assets), mCurrentCamera{ 0 }
	{
		auto* engine = static_cast<EngineGFX2D*>(manager->getEngine());
		mEngine = engine;

		// Create the default camera
		createCamera(new Camera2D(*engine->getRenderer(),
			engine->getWindowWidth(),
			engine->getWindowHeight()));
	}
	void Screen2D::update()
	{
		for (auto& obj : mObjs)
		{
			obj->update();
		}
	}
	void Screen2D::render()
	{
		mCameras[mCurrentCamera]->expose();
		for (auto& obj : mObjs)
		{
			obj->draw();
		}
		mCameras[mCurrentCamera]->present();
	}
	Camera2D& Screen2D::camera()
	{
		return *mCameras[mCurrentCamera];
	}
	Camera2D& Screen2D::camera(const uc index)
	{
		return *mCameras[index];
	}
	Camera2D& Screen2D::createCamera(Camera2D* camera)
	{
		mCameras.push_back(shared_ptr<Camera2D>(camera));
		return *camera;
	}
	void Screen2D::preload()
	{}
	void Screen2D::load()
	{}
	void Screen2D::postload()
	{}
	void Screen2D::free()
	{}
	void Screen2D::reset()
	{}
}