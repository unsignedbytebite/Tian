#include "Editor.h"

namespace Tian
{
	Editor::Editor(Input& input, Camera2D& camera, World& world) :
		mInput(input), mCam(camera), mWorld(world), mMouse({ 0, 0 })
	{
		/* creates a blank cursor */
		//SDL_ShowCursor(SDL_DISABLE);

		ConBox::Log::title("EDITOR");
		mSelector.add("Load");
		mSelector.add("Save");
		mSelector.add("Clear");
		mSelector.add("Snap");
		ConBox::Log::label(mSelector.get());
	}
	void Editor::update()
	{
		updateMouse();
		updateKeyboard();
	}
	void Editor::updateMouse()
	{
		// Set the mouse position
		mMouse.set(
			(((mInput.getMouseX() / (int)mCam.getScale().getX()) + (int)mCam.getLocal().getX()) / mSnap) * mSnap,
			(((mInput.getMouseY() / (int)mCam.getScale().getY()) + (int)mCam.getLocal().getY()) / mSnap) * mSnap);

		// Mouse event
		if (mInput.isMousetap())
		{
			eventMouse();
		}
	}
	void Editor::eventBackspace()
	{
		if (mPlacePoints.size() > 0)
		{
			mPlacePoints.pop_back();
		}
	}
	void Editor::eventMouse()
	{
		if (mMouseEventMode == POINTS)
		{
			if (mPlacePoints.size() < 7)
			{
				mPlacePoints.push_back(SDL_Point{ mMouse.x, mMouse.y });
				mMarkerPoint = SDL_Point{ mMouse.x, mMouse.y };

				ConBox::Log::p("[ " + mMouse.getString() + " ] ", ConBox::Colours::DIM, false);
			}
			else
			{
				ConBox::Log::error("Too many points");
			}
		}
	}
	void Editor::eventBracketRight()
	{
		++mSelector;
		ConBox::Log::label(mSelector.get());
	}
	void Editor::eventBracketLeft()
	{
		--mSelector;
		ConBox::Log::label(mSelector.get());
	}
	void Editor::eventSubSelect()
	{}
	void Editor::eventChevronLeft()
	{}
	void Editor::eventChevronRight()
	{}
	void Editor::eventReturn()
	{
		if (mSelector.get() == "Save")
		{
			ConBox::Log::p("File name: ", ConBox::Colours::BRIGHT, true);
			const auto fileName = ConBox::Log::read<str>();
			save(fileName);
		}
		else if (mSelector.get() == "Load")
		{
			ConBox::Log::p("File name: ", ConBox::Colours::BRIGHT, true);
			const auto fileName = ConBox::Log::read<str>();
			load(fileName);
		}
		else if (mSelector.get() == "Clear")
		{
			ConBox::Log::p("Remove all Y/N?: ", ConBox::Colours::BRIGHT, true);
			const auto option = ConBox::Log::read<str>();

			if (option == "y" || option == "Y")
			{
				mWorld.remove();
				ConBox::Log::p("Remove complete");
			}
		}
		else if (mSelector.get() == "Snap")
		{
			ConBox::Log::p("Current Snap level: " + std::to_string(mSnap));
			ConBox::Log::p("Enter new snap level: ", ConBox::Colours::BRIGHT, true);
			mSnap = ConBox::Log::read<ui>();
		}
		flush();
	}
	void Editor::updateKeyboard()
	{
		// Keyboard events
		if (mInput.isKeytap(SDLK_RETURN))
		{
			eventReturn();
		}
		else if (mInput.isKeytap(SDLK_BACKSPACE))
		{
			eventBackspace();
		}
		else if (mInput.isKeytap(SDLK_DELETE))
		{
			eventDelete();
		}
		else if (mInput.isKeytap(']'))
		{
			eventBracketRight();
		}
		else if (mInput.isKeytap('['))
		{
			eventBracketLeft();
		}
		else if (mInput.isKeytap('.'))
		{
			eventChevronRight();
		}
		else if (mInput.isKeytap(','))
		{
			eventChevronLeft();
		}
	}
	void Editor::draw()
	{
		//drawGrid();

		drawObjects();

		if (mPlacePoints.size() > 0)
		{
			mCam.drawPrimitive(mPlacePoints, { 156, 12, 32, 255 });
		}
		drawMouse();
		drawMarker();
	}
	void Editor::drawObjects()
	{
		mWorld.drawDebug();
	}
	void Editor::drawMarker()
	{
		// Draw mouse
		{
			SDL_Point points[2]
			{
				SDL_Point{ mMarkerPoint.x, mMarkerPoint.y + 3 },
				SDL_Point{ mMarkerPoint.x, mMarkerPoint.y - 3 },
			};
			mCam.drawPrimitive(points, 2, { 255, 24, 63, 255 });
		}
		{
			SDL_Point points[2]
			{
				SDL_Point{ mMarkerPoint.x + 3, mMarkerPoint.y },
				SDL_Point{ mMarkerPoint.x - 3, mMarkerPoint.y },
			};
			mCam.drawPrimitive(points, 2, { 255, 24, 63, 255 });
		}
	}
	void Editor::drawGrid()
	{
		const auto spacing = mSnap;

		for (auto x = 0; x < mCam.viewport().getRect().w; x += spacing)
		{
			SDL_Point points[2]
			{
				SDL_Point{ x, 0xfffff },
				SDL_Point{ x, -0xfffff },
			};
			mCam.drawPrimitive(points, 2, { 255, 100, 255, 25 });
		}

		for (auto y = 0; y < mCam.viewport().getRect().h; y += spacing)
		{
			SDL_Point points[2]
			{
				SDL_Point{ 0xfffff, y },
				SDL_Point{ -0xfffff, y },
			};
			mCam.drawPrimitive(points, 2, { 255, 100, 255, 25 });
		}
	}
	void Editor::save(const str filename)
	{
		try
		{
			// Go through each data and save out
			FileIO file{ filename, FileIO::WRITE };
			file.start();
			saveEvent(file);
			file.end();
			ConBox::Log::p("Save complete");
		}
		catch (Error& e)
		{
			e.print();
		}
	}
	void Editor::saveEvent(FileIO& file)
	{
		mWorld.serializeOut(file);
	}
	void Editor::load(const str filename)
	{
		try
		{
			// Go through each data and save out
			FileIO file{ filename };
			file.start();
			loadEvent(file);
			file.end();
			ConBox::Log::p("Load complete");
		}
		catch (Error& e)
		{
			e.print();
		}
	}
	void Editor::loadEvent(FileIO& file)
	{
		mWorld.serializeIn(file);
	}
	void Editor::eventDelete()
	{
		if (mObjects.size() > 0)
		{
			mWorld.remove(*mObjects.back());
			mObjects.pop_back();
		}
	}
	void Editor::flush()
	{
		mPlacePoints.clear();
		mMarkerPoint.x = 0xfffffff;
		mMarkerPoint.y = 0xfffffff;
	}
	void Editor::drawMouse()
	{
		// Draw mouse
		{
			SDL_Point points[2]
			{
				SDL_Point{ mMouse.x, 0xfffff },
				SDL_Point{ mMouse.x, -0xfffff },
			};
			mCam.drawPrimitive(points, 2, { 255, 255, 255, 50 });
		}
		{
			SDL_Point points[2]
			{
				SDL_Point{ 0xfffff, mMouse.y },
				SDL_Point{ -0xfffff, mMouse.y },
			};
			mCam.drawPrimitive(points, 2, { 255, 255, 255, 50 });
		}
	}
}

