///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Screen.h"
#include "EngineGFX2D.h"
#include "ObjectGFX2D.h"

namespace Tian
{
	/// <summary>
	/// A screen with a focus on 2D object rendering.
	/// </summary>
	class Screen;

	class Screen2D :
		public Screen
	{
	public:
		using Objects = vector<shared_ptr<ObjectGFX2D>>;
		using Cameras = vector<shared_ptr<Camera2D>>;

		/// <summary>
		/// Create a new Screen2D with an engine.
		/// </summary>
		/// <param name="engine">Engine handle</param>
		/// <param name="assets">Asset manager handle</param>
		Screen2D(EngineGFX2D* engine, AssetManager* assets = NULL);

		/// <summary>
		/// Create a new Screen2D with a screen manager
		/// </summary>
		/// <param name="manager">Screen manager handle</param>
		/// <param name="assets">Assetmanager handle</param>
		Screen2D(ScreenManager* manager, AssetManager* assets = NULL);

		/// <summary>
		/// Update the screen. All objects call update.
		/// </summary>
		virtual void update() override;

		/// <summary>
		/// Render the screen. All objects call render.
		/// </summary>
		virtual void render() override;

		/// <summary>
		/// Add a new object to the screen. Objects are collected in mObjs.
		/// </summary>
		/// <param name="newObject">New object to add.</param>
		/// <returns>Handle to the object that was created</returns>
		template<class T>
		T& addObject(T* newObject)
		{
			newObject->setCamera(&camera());
			newObject->created();
			mObjs.push_back(shared_ptr<ObjectGFX2D>(newObject));
			return *newObject;
		}

		/// <summary>
		/// Add a default new object to the screen. Objects are collected in mObjs.
		/// </summary>
		/// <returns>Handle to the object that was created</returns>
		template<class T>
		T& addObject()
		{
			T* newObject = new T();
			newObject->setCamera(&camera());
			newObject->created();
			mObjs.push_back(shared_ptr<ObjectGFX2D>(newObject));
			return *newObject;
		}

		/// <summary>
		/// Gets the reference the current camera used;
		/// </summary>
		/// <returns>Camera handle</returns>
		Camera2D& camera();

		/// <summary>
		/// Get the camera at the index
		/// </summary>
		/// <param name="index">Camera index</param>
		/// <returns>Camera handle</returns>
		Camera2D& camera(const uc index);

		/// <summary>
		/// Create a new camera. The camera gets added to the camera collection in this screen.
		/// </summary>
		/// <param name="camera">New camera to create</param>
		/// <returns>Camera handle</returns>
		Camera2D& createCamera(Camera2D* camera);
	protected:
		Cameras mCameras; ///< The collection of cameras used by this screen.
		uc mCurrentCamera; ///< The index of the current camera being used.
		Objects mObjs; ///< A collection of render objects used by the screen.

		// Inherited via Screen
		virtual void preload() override;
		virtual void load() override;
		virtual void postload() override;
		virtual void free() override;
		virtual void reset() override;
	};
}

