#include "SpriteFont.h"

namespace Tian
{
	SpriteFont::SpriteFont(Font& font) : mFont(new Font2D{ font })
	{}
	SpriteFont::SpriteFont(const AssetManager& assets, const us assetIndex) : mFont(new Font2D{ *assets.get<Font>(assetIndex) }) {}
	void SpriteFont::draw()
	{
		mFont->render(&mCam->renderer(), (int)(mLocal.x - mCam->getLocal().x), (int)(mLocal.y - mCam->getLocal().y));
	}
	void SpriteFont::setString(const str string, const Vector2 location, const Colour colour)
	{
		mLocal = location;
		mFont->spriteFont(string, &mCam->renderer(), Colours::White);
		mFont->setColour(colour);
	}
	Font2D& SpriteFont::font()
	{
		return *mFont.get();
	}
}
