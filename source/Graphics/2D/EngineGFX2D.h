///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../../Common.h"
#include "../EngineGFX.h"

namespace Tian
{
	/// <summary>
	/// SDL based engine. Uses the Screen object for buisness code.
	/// </summary>
	class EngineGFX2D :
		public EngineGFX
	{
	public:
		EngineGFX2D() :
			EngineGFX()
		{};
		~EngineGFX2D() { free(); };

		/// <summary>
		/// Starts the SDL window and renderer. Audio is also Initialised.
		/// </summary>
		/// <param name="windowWidth">Width of the display window</param>
		/// <param name="windowHeight">Height of the display window</param>
		/// <param name="name">Name to be displayed on the window</param>
		virtual void ini(const us windowWidth = 640, const us windowHeight = 320, const str name = "Tian framework") override;

		/// <summary>
		/// Calls preload on the screen.
		/// </summary>
		virtual void preload() override;

		/// <summary>
		/// Calls load on the screen.
		/// </summary>
		virtual void load() override;

		/// <summary>
		/// Calls post load on the screen.
		/// </summary>
		virtual void postload() override;

		/// <summary>
		/// Call update the screen.
		/// </summary>
		virtual void update() override;

		/// <summary>
		/// Start the rendering process.
		/// </summary>
		virtual void start() override;

		/// <summary>
		/// End the render process and present the render buffer.
		/// </summary>
		virtual void end() override;

		/// <summary>
		/// Render the screen.
		/// </summary>
		virtual void render() override;

		/// <summary>
		/// Close and release the window, renderer and SDL. Free is also called via the constructor.
		/// </summary>
		virtual void free();

		/// <summary>
		/// Set a renderer's render colour.
		/// </summary>
		/// <param name="renderer">The renderer handle</param>
		/// <param name="colour">The render colour to set</param>
		static void setRenderColour(SDL_Renderer* renderer, const SDL_Color& colour);

		/// <summary>
		/// Get the pointer to SDL renderer.
		/// </summary>
		/// <returns>The renderer</returns>
		SDL_Renderer* getRenderer() const { return mRenderer; }

		/// <summary>
		/// Set the SDL renderer.
		/// </summary>
		/// <param name="val">The renderer</param>
		void setRenderer(SDL_Renderer* val) { mRenderer = val; }

		/// <summary>
		/// Get a pointer to the SDL window.
		/// </summary>
		/// <returns>The window handle</returns>
		SDL_Window* getWindow() const { return mWindow; }

		/// <summary>
		/// Set the SDL window.
		/// </summary>
		/// <param name="val">The SDL window</param>
		void setWindow(SDL_Window* val) { mWindow = val; }
	protected:
		SDL_Window* mWindow; ///< The SDL window handle.
		SDL_Renderer* mRenderer; ///< The SDL renderer.
	};
}

