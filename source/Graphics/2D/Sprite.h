///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "ObjectGFX2D.h"
#include "../TextureAnimated.h"
#include "../../Utils/FrameSet.h"
#include "Camera2D.h"

namespace Tian
{
	/// <summary>
	/// A sprite object contains an animated texture for rendering. The body is used for logic positional data where the Sprite class is and parents are for the rendering display.
	/// </summary>
	class Sprite :
		public ObjectGFX2D
	{
	public:
		/// <summary>
		/// Create a new sprite with an animated texture and id.
		/// </summary>
		/// <param name="texture">Animated texture to use.</param>
		/// <param name="id">Id code</param>
		Sprite(const TextureAnimated texture, const us id = 0xffff);

		/// <summary>
		/// Draw with the assigned renderer
		/// </summary>
		virtual void draw() override;

		/// <summary>
		/// Draw the sprite attached to the body with a scaling factor.
		/// </summary>
		/// <param name="worldScale">World units to the render pixels</param>
		virtual void drawInWorld(const float worldScale = 1.0f) override;

		/// <summary>
		/// Update the sprite
		/// </summary>
		virtual void update() override;

		/// <summary>
		/// Set the current frame set.
		/// </summary>
		/// <param name="index">The index of the current frame set</param>
		virtual void setCurrentFrameSet(const us index);

		/// <summary>
		/// Set the animation paused state.
		/// </summary>
		/// <param name="pause">True to be set paused</param>
		virtual void setAnimationPaused(const bool pause);

		/// <summary>
		/// Set the delay between frames in cycle counts.
		/// </summary>
		/// <param name="delay">Counts between delay</param>
		void setFrameDelay(const us delay);

		/// <summary>
		/// Set the angle of the sprite
		/// </summary>
		/// <param name="angle">Angle</param>
		void setAngle(const float angle);

		/// <summary>
		/// Get the angle,
		/// </summary>
		/// <returns>Angle</returns>
		float getAngle() const;

		/// <summary>
		/// The pivot point in the sprite. The sprite will rotate around this point by the angle amount.
		/// </summary>
		/// <returns>Handle to the pivot point</returns>
		Point& pivot();

		/// <summary>
		/// Set the flip mode.
		/// </summary>
		/// <param name="flip">Flip mode</param>
		void setFlip(const SDL_RendererFlip flip);

		/// <summary>
		/// Get the flip mode of the sprite.
		/// </summary>
		/// <returns>The flip mode</returns>
		SDL_RendererFlip getFlip() const;

		/// <summary>
		/// Sync the texture colour to the sprite's colour.
		/// </summary>
		void updateTextureColour();
	protected:
		TextureAnimated mTex; ///< The animated texture of the sprite.
		Vector2 mBodyPos; ///< The position of the body in scaled render space.
		float mAngle; ///< Angle to rotate the sprite
		Point mPivot; ///< The pivot point to rotate the sprite around
		SDL_RendererFlip mFlip; ///< The flip state of the texture
		Colour mColour; ///< The sprite render colour
	};
}

