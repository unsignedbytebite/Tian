#include "EngineGFX2D.h"
#include "../../Utils/Maths.h"

using namespace ConBox;

namespace Tian
{
	void EngineGFX2D::ini(const us windowWidth /*= 640*/, const us windowHeight /*= 320*/, const str name /*= "Tian Framework"*/)
	{
		EngineGFX::ini(windowWidth, windowHeight, name);

		//Initialize SDL 
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		{
			throw Error("SDL initialise fail", __func__);
		}
		else
		{
			//Create window 
			mWindow = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN);
			if (mWindow == NULL)
			{
				throw Error("Failed to create window", __func__);
			}
			else
			{
				//Create renderer for window
				mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
				if (mRenderer == NULL)
				{
					throw Error("Failed to assign renderer", __func__);
				}
				else
				{
					//Initialize renderer color
					SDL_SetRenderDrawColor(mRenderer, 9, 9, 9, 0xFF);

					//Initialize PNG loading
					int imgFlags = IMG_INIT_PNG;
					if (!(IMG_Init(imgFlags) & imgFlags))
					{
						throw Error("Failed to initialise PNG", __func__);
					}

					//Initialize SDL_ttf
					if (TTF_Init() == -1)
					{
						throw Error("Failed to initialise TTF", __func__);
					}
				}
			}
		}
	}

	void EngineGFX2D::preload()
	{
		mScreen->preload();
	}

	void EngineGFX2D::load()
	{
		mScreen->load();
	}

	void EngineGFX2D::postload()
	{
		mScreen->postload();
	}

	void EngineGFX2D::update()
	{
		mScreen->update();
	}

	void EngineGFX2D::start()
	{
		//Clear screen
		SDL_SetRenderDrawColor(mRenderer, 9, 9, 9, 0xFF);
		SDL_RenderClear(mRenderer);
	}

	void EngineGFX2D::end()
	{
		//Update screen
		SDL_RenderPresent(mRenderer);
	}

	void EngineGFX2D::render()
	{
		mScreen->render();
	}

	void EngineGFX2D::free()
	{
		EngineGFX::free();

		// Free window
		if (mWindow != NULL)
		{
			SDL_DestroyWindow(mWindow);
			mWindow = NULL;
		}

		// Free renderer
		if (mRenderer != NULL)
		{
			SDL_DestroyRenderer(mRenderer);
			mRenderer = NULL;
		}

		// Quit SDL
		IMG_Quit();
		TTF_Quit();
		SDL_Quit();
	}

	void EngineGFX2D::setRenderColour(SDL_Renderer* renderer, const SDL_Color& colour)
	{
		SDL_SetRenderDrawColor(renderer, colour.r, colour.g, colour.b, colour.a);
	}
}