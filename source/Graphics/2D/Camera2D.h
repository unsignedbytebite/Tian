///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../../Utils/Vector2.h"
#include "../Texture.h"
#include "../../Logic2D/Object2D.h"
#include "../Colours.h"

namespace Tian
{
	/// <summary>
	/// Camera2D acts as a render target. Render calls of other items can be captured using expose() and stopExpose() and showing the render texture using present().
	/// </summary>
	class Camera2D : public Object2D
	{
	public:
		/// <summary>
		/// Create a new camera with a renderer and the size of the render target.
		/// </summary>
		/// <param name="renderer">Renderer to use</param>
		/// <param name="frameWidth">The width of the render target</param>
		/// <param name="frameHeight">The height of the render target</param>
		Camera2D(SDL_Renderer& renderer, const us frameWidth = 1024, const us frameHeight = 1024);
		~Camera2D() {}

		/// <summary>
		/// Begin the camera for exposing to draw calls.
		/// </summary>
		void expose();

		/// <summary>
		/// Render the image captured by the camera. This call also stops the exposure.
		/// </summary>
		void present();

		/// <summary>
		/// Stop the exposure of the camera
		/// </summary>
		void stopExpose();

		/// <summary>
		/// Get the scale of the camera.
		/// </summary>
		/// <returns>Camera scale</returns>
		Vector2 getScale() const;

		/// <summary>
		/// Set the scale of the camera.
		/// </summary>
		/// <param name="val">New camera scale</param>
		void setScale(const Vector2& val);

		/// <summary>
		/// Set the scale of the camera.
		/// </summary>
		/// <param name="val">New camera scale</param>
		void setScale(const float val);

		/// <summary>
		/// Get the viewport of the camera scaled with current location.
		/// </summary>
		/// <returns></returns>
		Box& viewport();

		/// <summary>
		/// The camera's referenced renderer
		/// </summary>
		/// <returns>the renderer used by the camera</returns>
		SDL_Renderer& renderer();

		/// <summary>
		/// Move the scale by an amount and recalculate the viewport.
		/// </summary>
		/// <param name="scale">Delta scale</param>
		void moveScale(const Vector2 scale);

		/// <summary>
		/// Move the camera's render texture by an amount.
		/// </summary>
		/// <param name="r">Delta red</param>
		/// <param name="g">Delta green</param>
		/// <param name="b">Delta blue</param>
		/// <param name="a">Delta alpha</param>
		void moveColour(const char r, const char g, const char b, const char a = 0);

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(std::vector<Point>& points, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(Vector2Points&, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(Point* points, const uc size, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(const Point& point, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitiveBox(const SDL_Rect& rect, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitiveBoxFill(const SDL_Rect& rect, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(const int x, const int y, std::vector<Point>& points, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(const int x, const int y, Vector2Points& points, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(const int x, const int y, Point* points, const uc size, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitive(const int x, const int y, const Point& point, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitiveBox(const int x, const int y, const SDL_Rect& rect, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Draw a primitive with this camera
		/// </summary>
		void drawPrimitiveBoxFill(const int x, const int y, const SDL_Rect& rect, const SDL_Color& colour = { Colours::White }) const;

		/// <summary>
		/// Create a new physics body for the camera and place it within a world. The default physics parameters is for a 0 density sensor.
		/// </summary>
		/// <param name="world">The world to place the body into</param>
		/// <param name="width">The width of the camera body</param>
		/// <param name="height">The height of the camera body</param>
		void createBody(b2World& world, const float32 width, const float32 height);

		/// <summary>
		/// Sync the camera body location to the camera location with a scaling factor. The scaling factor is normally used when converting from world space to pixel rendering space. 
		/// </summary>
		/// <param name="scale">The locational scale factor</param>
		void setLocalToBodyPosition(const float scale);
	protected:
		Vector2 mScale{ 1, 1 }; ///< The camera scale.
		shared_ptr<Texture> mTex; ///< The render target texture.
		SDL_Renderer& mRenderer; ///< The assigned renderer.
	};
}