///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "ObjectGFX2D.h"
#include "Camera2D.h"
#include "../../Utils/Map2D.h"
#include "../TextureAnimated.h"

namespace Tian
{
	/// <summary>
	/// This is a render target for large dimensions. It uses data chunks for mosaic of textures in a grid. Useful in business code for large static pixel maps such as a level.
	/// </summary>
	class Canvas :
		public ObjectGFX2D
	{
	public:
		/// <summary>
		/// Create a new canvas.
		/// </summary>
		/// <param name="cam">Handle to the camera</param>
		/// <param name="width">Width of the canvas</param>
		/// <param name="height">Height of the canvas</param>
		/// <param name="dividesX">The lateral divisions in the canvas</param>
		/// <param name="dividesY">The horizontal divisions in the canvas</param>
		/// <param name="id">Id code</param>
		Canvas(Camera2D* cam, const us width = 256, const us height = 256, const us dividesX = 1, const us dividesY = 1, const us id = 0xffff);

		/// <summary>
		/// Draw with the assigned renderer.
		/// </summary>
		virtual void draw() override;

		/// <summary>
		/// Fill the canvas with a default texture.
		/// </summary>
		virtual void fillWithCanvasTexture();

		/// <summary>
		/// Draw the canvas attached to the body to a scale.
		/// </summary>
		/// <param name="worldScale">World units to pixels</param>
		virtual void drawInWorld(const float worldScale = 1.0f) override;

		/// <summary>
		/// Expose a canvas chunk for draw calls.
		/// </summary>
		/// <param name="chunkX">Chunk X</param>
		/// <param name="chunkY">Chunk Y</param>
		virtual void startExposure(const us chunkX = 0, const us chunkY = 0);

		/// <summary>
		/// Stop exposing a canvas chunk for draw calls.
		/// </summary>
		/// <param name="chunkX">Chunk X</param>
		/// <param name="chunkY">Chunk Y</param>
		virtual void stopExposure(const us chunkX = 0, const us chunkY = 0);

		/// <summary>
		/// Draw a texture on the canvas at a location.
		/// </summary>
		/// <param name="texture">Texture to be drawn</param>
		/// <param name="location">Location on the canvas to draw</param>
		virtual void drawTexture(Texture& texture, Vector2& location);

		/// <summary>
		/// Draw a animated texture on the canvas at a location.
		/// </summary>
		/// <param name="texture">Animated texture to be drawn</param>
		/// <param name="location">Location on the canvas to draw</param>
		virtual void drawTexture(TextureAnimated& texture, Vector2& location);

		/// <summary>
		/// Clear the canvas with alpha.
		/// </summary>
		virtual void clear();
	protected:
		using Chunks = Map2D<Texture*>;

		/// <summary>
		/// Returns a chunk index found in the search location. 0xffff, 0xffff if not found.
		/// </summary>
		/// <param name="searchLocation">Location on the canvas to search</param>
		/// <returns>2D index of the found chunk</returns>
		virtual Us2 find(const Vector2& searchLocation);

		Chunks mChunks; ///< The chunks the canvas is made of.
		vector<shared_ptr<Texture>> mChunkCache; ///< The chunk cache collection.
		us mWidth; ///< The canvas width dimension.
		us mHeight; ///< The canvas height dimension.
		us mDividesX; ///< The canvas divides along the X axis.
		us mDividesY; ///< The canvas divides along the Y axis.
	};
}

