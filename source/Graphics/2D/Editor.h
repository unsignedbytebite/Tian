///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../../Common.h"
#include "../../Runtime/Input.h"
#include "Camera2D.h"
#include "../../Logic2D/World.h"
#include "../../Utils/Buffer.h"
#include "../../IO/FileIO.h"

namespace Tian
{
	/// <summary>
	/// An editor object can be implemented to edit a world. Requires extending.
	/// </summary>
	class Editor
	{
	public:
		/// <summary>
		/// Create a new editor.
		/// </summary>
		/// <param name="input">The input controller</param>
		/// <param name="camera">The camera handle</param>
		/// <param name="world">Reference to the world</param>
		Editor(Input& input, Camera2D& camera, World& world);

		/// <summary>
		/// Update the editor.
		/// </summary>
		virtual void update();

		/// <summary>
		/// Draw the editor.
		/// </summary>
		virtual void draw();
	protected:
		/// <summary>
		/// Draw the object layer.
		/// </summary>
		virtual void drawObjects();

		/// <summary>
		/// Save data for a world.
		/// </summary>
		/// <param name="filename">File path</param>
		virtual void save(const str filename);

		/// <summary>
		/// Load data for a world.
		/// </summary>
		/// <param name="filename">File path</param>
		virtual void load(const str filename);

		/// <summary>
		/// Called when the save event has been triggered.
		/// </summary>
		/// <param name="file">File path</param>
		virtual void saveEvent(FileIO& file);

		/// <summary>
		/// Called when the load event has been triggered.
		/// </summary>
		/// <param name="file">File path</param>
		virtual void loadEvent(FileIO& file);

		/// <summary>
		/// Called when the go back event has been triggered.
		/// </summary>
		virtual void eventBackspace();

		/// <summary>
		/// Called when the mouse click event has been triggered.
		/// </summary>
		virtual void eventMouse();

		/// <summary>
		/// Called when a selection event has been triggered.
		/// </summary>
		virtual void eventReturn();

		/// <summary>
		/// Called when a sub selection event has been triggered.
		/// </summary>
		virtual void eventSubSelect();

		/// <summary>
		/// Called when a chevron left event has been triggered.
		/// </summary>
		virtual void eventChevronLeft();

		/// <summary>
		/// Called when a chevron right event has been triggered.
		/// </summary>
		virtual void eventChevronRight();

		/// <summary>
		/// Called when a left bracket event has been triggered.
		/// </summary>
		virtual void eventBracketLeft();

		/// <summary>
		/// Called when a right bracket event has been triggered.
		/// </summary>
		virtual void eventBracketRight();

		/// <summary>
		/// Called when a delete event has been triggered.
		/// </summary>
		virtual void eventDelete();

		/// <summary>
		/// Remove all the current editing GUI elements.
		/// </summary>
		virtual void flush();

		/// <summary>
		/// Update the mouse input.
		/// </summary>
		virtual void updateMouse();

		/// <summary>
		/// Update the keyboard input.
		/// </summary>
		virtual void updateKeyboard();

		/// <summary>
		/// Draw the mouse.
		/// </summary>
		virtual void drawMouse();

		/// <summary>
		/// Draw the point marker
		/// </summary>
		virtual void drawMarker();

		/// <summary>
		/// Draw the editor grid
		/// </summary>
		virtual void drawGrid();

		enum MouseEventPlacementMode { NONE = 0, POINTS, POINT, COUNT }; ///< The mouse event placement modes.

		MouseEventPlacementMode mMouseEventMode{ NONE }; ///< The current mouse event placement mode.
		std::vector<Object2D*> mObjects; ///< List of objects created by the editor.
		std::vector<SDL_Point> mPlacePoints; ///< The current points being placed.
		SDL_Point mMarkerPoint; ///< The last interaction point on the editor.
		Input& mInput; ///< Handle to the input controller.
		Camera2D& mCam; ///< Handle to the cam used for the editor.
		Int2 mMouse; ///< The current mouse location.
		World& mWorld; ///<  The handle to the world currently editing.
		Buffer<str> mSelector; ///<  The menu option selector.
		int mSnap{ 1 }; ///< The snap level.
	};
}


