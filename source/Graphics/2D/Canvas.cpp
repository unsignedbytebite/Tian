#include "Canvas.h"
#include "../../Utils/Buffer.h"

namespace Tian
{
	Canvas::Canvas(Camera2D* cam, const us width, const us height, const us dividesX, const us dividesY, const us id) :
		ObjectGFX2D(id, cam), mDividesX(dividesX), mDividesY(dividesY), mWidth(width), mHeight(height), mChunks(dividesX, dividesY)
	{
		const us chunkWidth = mWidth / mDividesX;
		const us chunkHeight = mHeight / mDividesY;

		for (us x = 0; x < mChunks.getWidth(); ++x)
		{
			for (us y = 0; y < mChunks.getHeight(); ++y)
			{
				auto* tex = new Texture();
				tex->create(chunkWidth, chunkHeight, &mCam->renderer());
				mChunkCache.push_back(shared_ptr<Texture>(tex));
				mChunks.set(x, y, tex);
			}
		}
	}

	void Canvas::draw()
	{
		const us chunkWidth = mWidth / mDividesX;
		const us chunkHeight = mHeight / mDividesY;
		Vector2 local{ 0, 0 };

		for (us x = 0; x < mChunks.getWidth(); ++x)
		{
			for (us y = 0; y < mChunks.getHeight(); ++y)
			{
				const auto& chunk = *mChunks.get(x, y);
				chunk->render(&mCam->renderer(), mLocal - mCam->getLocal() + local);

				local.move(0, chunkHeight);
			}

			local.y = 0;
			local.move(chunkWidth, 0);
		}
	}
	void Canvas::fillWithCanvasTexture()
	{
		for (us x = 0; x < mChunks.getWidth(); ++x)
		{
			for (us y = 0; y < mChunks.getHeight(); ++y)
			{
				const auto& chunk = *mChunks.get(x, y);
				//chunk->render(&mCam->renderer(), mLocal - mCam->getLocal() + Vector2{ (float)x * canvasWidth, (float)y * canvasHeight });

				chunk->startTarget(&mCam->renderer());
				// Draw hatch
				Buffer<SDL_Color> colourSwap;
				colourSwap.add({ 100, 100, 100, 255 });
				colourSwap.add({ 80, 80, 80, 255 });

				for (auto x2 = 0; x2 < chunk->getWidth(); ++x2)
				{
					for (auto y2 = 0; y2 < chunk->getHeight(); ++y2)
					{
						mCam->drawPrimitive(x2, y2, { 0, 0 }, colourSwap.get());
						colourSwap.stepIndex(1);
					}
					colourSwap.stepIndex(1);
				}

				// Draw outline
				mCam->drawPrimitiveBox(0, 0, { 0, 0, chunk->getWidth(), chunk->getHeight() }, { 60, 60, 60, 255 });
				chunk->stopTarget(&mCam->renderer());
			}
		}
	}
	void Canvas::startExposure(const us chunkX, const us chunkY)
	{
		const auto& chunk = *mChunks.get(chunkX, chunkY);
		chunk->startTarget(&mCam->renderer());
	}
	void Canvas::stopExposure(const us chunkX, const us chunkY)
	{
		const auto& chunk = *mChunks.get(chunkX, chunkY);
		chunk->stopTarget(&mCam->renderer());
	}
	void Canvas::drawTexture(Texture& texture, Vector2& location)
	{
		const auto texBounds = Box{ location, { 0, 0, texture.getWidth(), texture.getHeight() } };

		Us2 foundChunks[4]
		{
			find(texBounds.topLeft()),
			find(texBounds.topRight()),
			find(texBounds.bottomRight()),
			find(texBounds.bottomLeft())
		};

		// For each of the found texture chunks
		for (uc i = 0; i < 4; ++i)
		{
			// If in bounds then render to
			if (mChunks.isInBounds(foundChunks[i].x, foundChunks[i].y))
			{
				const us chunkWidth = mWidth / mDividesX;
				const us chunkHeight = mHeight / mDividesY;

				// Calculate the offset
				const Vector2 chunkPos{
					foundChunks[i].x*(float)chunkWidth,
					foundChunks[i].y*(float)chunkHeight };

				// Render time
				auto& chunk = **mChunks.get(foundChunks[i]);
				chunk.startTarget(&mCam->renderer());
				texture.render(&mCam->renderer(), location - chunkPos);
				chunk.stopTarget(&mCam->renderer());
			}
		}
	}
	void Canvas::drawTexture(TextureAnimated& texture, Vector2& location)
	{
		const auto texBounds = Box{ location, { 0, 0, texture.textureData()->getWidth(), texture.textureData()->getHeight() } };

		Us2 foundChunks[4]
		{
			find(texBounds.topLeft()),
			find(texBounds.topRight()),
			find(texBounds.bottomRight()),
			find(texBounds.bottomLeft())
		};

		// For each of the found texture chunks
		texture.currentFrameSet().setPause(true);
		for (uc i = 0; i < 4; ++i)
		{
			// If in bounds then render to
			if (mChunks.isInBounds(foundChunks[i].x, foundChunks[i].y))
			{
				const us chunkWidth = mWidth / mDividesX;
				const us chunkHeight = mHeight / mDividesY;

				// Calculate the offset
				const Vector2 chunkPos{
					foundChunks[i].x*(float)chunkWidth,
					foundChunks[i].y*(float)chunkHeight };

				// Render time
				auto& chunk = **mChunks.get(foundChunks[i]);
				chunk.startTarget(&mCam->renderer());
				texture.render(&mCam->renderer(), location - chunkPos);
				chunk.stopTarget(&mCam->renderer());
			}
		}
		texture.currentFrameSet().setPause(false);
	}
	void Canvas::clear()
	{
		for (us x = 0; x < mChunks.getWidth(); ++x)
		{
			for (us y = 0; y < mChunks.getHeight(); ++y)
			{
				const auto& chunk = *mChunks.get(x, y);
				chunk->clear(&mCam->renderer());
			}
		}
	}
	Us2 Canvas::find(const Vector2& searchLocation)
	{
		const us chunkWidth = mWidth / mDividesX;
		const us chunkHeight = mHeight / mDividesY;

		const auto indeX = (us)(searchLocation.getX() / chunkWidth);
		const auto indexY = (us)(searchLocation.getY() / chunkHeight);

		if (mChunks.isInBounds(indeX, indexY))
		{
			return{ indeX, indexY };
		}

		return{ 0xffff, 0xffff };
	}
	void Canvas::drawInWorld(const float worldScale)
	{
		const auto bodyPos = Vector2{ mBody->GetPosition().x * worldScale, mBody->GetPosition().y * worldScale };
		//mTex->render(&mCam->renderer(), mLocal - mCam->getLocal() + bodyPos);
	}
}
