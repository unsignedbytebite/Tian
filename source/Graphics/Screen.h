///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "EngineGFX.h"
#include "../Runtime/Input.h"
#include "../Runtime/AssetManager.h"
#include "ScreenManager.h"

namespace Tian
{
	class EngineGFX;
	class ScreenManager;

	/// <summary>
	/// The main presentation class for a engine.
	/// </summary>
	class Screen
	{
	protected:
		str mName; ///< The name of the screen. Not used.
		EngineGFX* mEngine; ///< Pointer the parent engine.
		AssetManager* mAssets; ///< Pointer to the asset manager to use.
		ScreenManager* mManager; ///< Pointer the parent asset manager.

	public:
		/// <summary>
		/// Create a new screen.
		/// </summary>
		/// <param name="engine">The parent engine this screen works with</param>
		/// <param name="assets">Handle to the asset manager</param>
		Screen(EngineGFX* engine, AssetManager* assets = NULL) :
			mEngine(engine), mAssets(assets)
		{};

		/// <summary>
		/// Create a new screen using the screen manager. 
		/// </summary>
		/// <param name="manager">Parent screen manager</param>
		/// <param name="assets">Handle to the asset manager</param>
		Screen(ScreenManager* manager, AssetManager* assets = NULL) :
			mManager(manager), mAssets(assets)
		{};

		///<summary>
		/// Called by the parent engine before the load call.
		///</summary>
		virtual void preload() = 0;

		///<summary>
		/// Called by the parent engine when loading.
		///</summary>
		virtual void load() = 0;

		///<summary>
		/// Called by the parent engine after the load call.
		///</summary>
		virtual void postload() = 0;

		///<summary>
		/// Called by the parent engine to update the screen.
		///</summary>
		virtual void update() = 0;

		///<summary>
		/// Called by the parent engine to render assets.
		///</summary>
		virtual void render() = 0;

		///<summary>
		/// To be implemented for any freeing of resources. Called when the engine begins execution.
		///</summary>
		virtual void free() = 0;

		///<summary>
		/// To be implemented if reseting is required.
		///</summary>
		virtual void reset() = 0;

		/// <summary>
		/// Get the name of the screen.
		/// </summary>
		/// <returns>Screen name</returns>
		str getName() const { return mName; }

		/// <summary>
		/// Set the name of the screen.
		/// </summary>
		/// <param name="name">Screen name</param>
		void setName(const str& name) { mName = name; }

		/// <summary>
		/// Get the parent engine of this screen. 
		/// </summary>
		/// <returns>Parent Engine</returns>
		EngineGFX& getEngine() const { return *mEngine; }

		/// <summary>
		/// Set the parent engine of the screen.
		/// </summary>
		/// <param name="engine">New engine</param>
		void setEngine(EngineGFX* engine) { mEngine = engine; }

		/// <summary>
		/// Get the assigned input controller for this screen.
		/// </summary>
		/// <returns>Input controller : Input*</returns>
		Input* getInput();

		/// <summary>
		/// Get the delta time between render frames.
		/// </summary>
		/// <returns>Frame delta time</returns>
		const float getDelta();

		/// <summary>
		/// Get the current frames per second.
		/// </summary>
		/// <returns>Current FPS</returns>
		const float getFPS();

		/// <summary>
		/// Get the current frames per second as a string.
		/// </summary>
		/// <returns>FPS as a string</returns>
		const str getFPSString();
	};
}

