///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Texture.h"
#include "../Utils/FrameSets.h"
#include "../Runtime/Asset.h"
#include "../IO/FileIO.h"

namespace Tian
{
	/// <summary>
	/// Uses a texture to present frames from it. Can be used as a animated texture and/or as a sprite sheet where only sections of the whole image need to be drawn.
	/// </summary>
	class TextureAnimated :
		public Asset
	{
	public:
		/// <summary>
		/// Default constructor
		/// </summary>
		TextureAnimated();

		/// <summary>
		/// Load a animated texture binary file. TianPacker can be used to create animated texture binaries for use in loading in this constructor.
		/// </summary>
		/// <param name="texanimPath">Path to the animated texture binary</param>
		/// <param name="renderer">Renderer to use</param>
		TextureAnimated(const str texanimPath, SDL_Renderer* renderer);

		/// <summary>
		/// Create a animated texture from a texture and frames.
		/// </summary>
		/// <param name="texture">Texture to bind to</param>
		/// <param name="frameSets">The sets of frames that render sections of the texture</param>
		TextureAnimated(Texture* texture, const FrameSets frameSets);

		/// <summary>
		/// Load a texture from a file. This function does not load nor create frame sets.
		/// </summary>
		/// <param name="path">Path of the texture</param>
		/// <param name="renderer">Renderer to use</param>
		virtual void loadFromFile(const str path, SDL_Renderer* renderer);

		/// <summary>
		/// Add a frame set to the frame set collection
		/// </summary>
		/// <param name="set">New frame set</param>
		virtual void addFrameSet(FrameSet set);

		/// <summary>
		/// Get the current frame set being presented.
		/// </summary>
		/// <returns>Current frame set</returns>
		virtual const us getCurrentFrameSet();

		/// <summary>
		/// Set the current frame set to be presented. Use to change animations.
		/// </summary>
		/// <param name="index">Index of the frame set to use</param>
		virtual void setCurrentFrameSet(const us index);

		/// <summary>
		/// The handle to the current frame set
		/// </summary>
		/// <returns>Current frame set</returns>
		FrameSet& currentFrameSet();

		/// <summary>
		/// Returns all the frame sets used by this texture.
		/// </summary>
		/// <returns>Frame sets </returns>
		FrameSets getFrameSets() const;

		/// <summary>
		/// Handle to all the frame sets used by this animated texture.
		/// </summary>
		/// <returns></returns>
		FrameSets& frameSets();

		/// <summary>
		/// Set the frame sets used by this animated texture.
		/// </summary>
		/// <param name="sets">New frame sets</param>
		void setFrameSets(const FrameSets sets);

		/// <summary>
		/// Render the current frame from the texture.
		/// </summary>
		/// <param name="renderer">The renderer to draw the texture with</param>
		/// <param name="location">The screen position to render at</param>
		/// <param name="angle">The angle to draw the texture</param>
		/// <param name="pivot">The angle pivot point to rotate around</param>
		/// <param name="flip">Set the flip render state</param>
		virtual void render(SDL_Renderer* renderer, const Vector2& location, const float angle = 0.f, const SDL_Point& pivot = { 0, 0 }, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);

		/// <summary>
		/// Render the current frame from the texture.
		/// </summary>
		/// <param name="renderer">The renderer to draw the texture with</param>
		/// <param name="x">The x screen position</param>
		/// <param name="y">The y screen position</param>
		/// <param name="angle">The angle to draw the texture</param>
		/// <param name="pivot">The angle pivot point to rotate around</param>
		/// <param name="flip">Set the flip render state</param>
		virtual void render(SDL_Renderer* renderer, const int x, const int y, const float angle = 0.f, const SDL_Point& pivot = { 0, 0 }, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);

		/// <summary>
		/// Handle to the loaded texture data.
		/// </summary>
		/// <returns>Texture handle</returns>
		Texture* textureData() const;

		/// <summary>
		/// Set the animation pause state.
		/// </summary>
		/// <param name="pause">True to pause the animation</param>
		virtual void setAnimationPaused(const bool pause);

		/// <summary>
		/// Set the delay between frames in cycle calls.
		/// </summary>
		/// <param name="delay">Delay in cycles</param>
		void setFrameDelay(const us delay);

		/// <summary>
		/// Serialize out this object to a file.
		/// </summary>
		/// <param name="file">Path to save to</param>
		void serializeOut(FileIO& file) const;
	protected:
		std::shared_ptr<Texture> mTexture; ///< The texture to render from.
		FrameSets mFrameSets; ///< The collection of frame sets to render masks from the texture.
		us mCurrentFrameSet{ 0 }; ///< The current frame set to use
	};
}

