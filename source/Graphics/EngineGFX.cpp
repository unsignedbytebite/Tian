#include "EngineGFX.h"
#include <SDL_events.h>

namespace Tian
{
	void EngineGFX::free()
	{}
	const str EngineGFX::getDebugText()
	{
		return str{ mName + " [" + to_string(mWindowWidth) + ", " + to_string(mWindowHeight) + "]" };
	}

	void EngineGFX::ini(const us windowWidth, const us windowHeight, const str name)
	{
		mWindowWidth = windowWidth;
		mWindowHeight = windowHeight;
		mName = name;
		mQuit = false;
		mInput = std::shared_ptr<Input>(new Input());
	}

	void EngineGFX::iniExecute(Screen* screen, const us windowWidth /*= 640*/, const us windowHeight /*= 320*/, const str name /*= "Tian Framework"*/)
	{
		ini(windowWidth, windowHeight, name);
		execute(screen);
	}

	void EngineGFX::execute(Screen* screen)
	{
		if (mScreen != NULL)
		{
			mScreen->free();
		}

		mScreen = screen;

		// Pre core loop load calls
		preload();
		load();
		postload();


		//Event handler
		SDL_Event e;

		mTimer.splitTime();
		mDeltaTime = (float)(mTimer.getMs() / 1000);

		//While application is running
		do
		{
			getInput()->setKeysDown(0);

			//Handle events on queue
			while (SDL_PollEvent(&e) != 0)
			{
				switch (e.type)
				{
				case SDL_QUIT:
					mQuit = true;
					break;
				case SDL_KEYDOWN:
					getInput()->setKey(e.key.keysym.sym, true);
					break;
				case SDL_KEYUP:
					getInput()->setKey(e.key.keysym.sym, false);
					break;
				case SDL_MOUSEBUTTONDOWN:
					getInput()->setMouse(e.button.button, true);
					break;
				case SDL_MOUSEBUTTONUP:
					getInput()->setMouse(e.button.button, false);
					break;
				case SDL_MOUSEMOTION:
					getInput()->setMouseXY();
					break;
				}
			}

			mTimer.splitTime();
			update();
			start();
			render();
			end();
			mDeltaTime = mTimer.getMs() / 1000.f;

		} while (!mQuit);
	}
}
