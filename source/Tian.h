///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once

// Headers
#include "ConBox/ConBox.h"
#include "Box2D/Box2D.h"
#include "Box2D/Box2DebugDraw.h"
#include "Common.h"

#include "../source/Graphics/2D/Screen2D.h"
#include "../source/Graphics/2D/Font2D.h"
#include "../source/Utils/Timer.h"
#include "../source/Utils/Buffer.h"
#include "../source/Utils/FrameSets.h"
#include "../source/Graphics/TextureAnimated.h"
#include "../source/Runtime/EventSequence.h"
#include "../source/Runtime/EventMsg.h"
#include "../source/Logic2D/World2D.h"
#include "../source/Graphics/2D/Sprite.h"
#include "../source/Runtime/AssetManager.h"
#include "../source/Utils/SimplexNoise.h"
#include "../source/Graphics/2D/Canvas.h"
#include "../source/Runtime/Loader.h"
#include "../source/Graphics/Colours.h"
#include "../source/Utils/Counter.h"
#include "../source/Graphics/2D/SpriteFont.h"
#include "../source/Graphics/2D/Actor.h"
#include "../source/Graphics/2D/Editor.h"
#include "../source/Audio/AudioEngine.h"

namespace Tian
{
	const static char* NAME = "Tian v0.3.3"; ///< Framework version name.
}

