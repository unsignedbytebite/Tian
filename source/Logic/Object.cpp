#include "Object.h"

namespace Tian
{
	const bool Object::operator==(const Object& obj) const
	{
		return this == &obj;
	}
	void Object::setID(const ui id)
	{
		mID = id;
	}
	const ui Object::getID() const
	{
		return mID;
	}
}
