#include "AudioEngine.h"

namespace Tian
{
	AudioEngine::AudioEngine()
	{
		//Initialize SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			throw Error{ "Cannnot start the audio engine" };
		}
	}
	AudioEngine::~AudioEngine()
	{
		Mix_Quit();
	}
	void AudioEngine::load(const str& filePath)
	{
		mCues.emplace_back(new AudioCue{ filePath });
	}
	void AudioEngine::cue(const us index, const int channel, const int loops)
	{
		mCues[index]->play(channel, loops);
	}
	void AudioEngine::cueOnce(const us index, const int channel, const int loops)
	{
		mCues[index]->playOnce(channel, loops);
	}
}
