#include "Event.h"

namespace Tian
{
	Event::Event(const ui eventStart, const ui eventEnd) :
		mEventStart(eventStart), mEventEnd(eventEnd)
	{}
	bool Event::isInWindow(const Timer& timer)
	{
		return mEventStart >= timer.getMs() && mEventStart <= timer.getMs();
	}
	ui Event::getEventStart() const
	{
		return mEventStart;
	}
	ui Event::getEventEnd() const
	{
		return mEventEnd;
	}
	void Event::update() const
	{}
	void Event::completed() const
	{}
}