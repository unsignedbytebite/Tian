///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// A generic asset type that used by the assetmanager.
	/// </summary>
	class Asset
	{
	public:
		/// <summary>
		/// Create a new asset defining the asset location.
		/// </summary>
		/// <param name="path">Asset location</param>
		Asset(const str path = "");

		/// <summary>
		/// Get the path this asset was loaded from.
		/// </summary>
		/// <returns>Asset path</returns>
		str getPath() const;

		/// <summary>
		/// Set the path this asset was loaded from
		/// </summary>
		/// <param name="path">Asset path</param>
		void setPath(const str path);
	protected:
		str mPath; ///< The path to the asset
	};

	using Asset_ = std::shared_ptr<Asset>;
}

