#include "Asset.h"

namespace Tian
{
	Asset::Asset(const str path) : mPath(path)
	{}

	void Asset::setPath(const str path)
	{
		mPath = path;
	}

	str Asset::getPath() const
	{
		return mPath;
	}
}