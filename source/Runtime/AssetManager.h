///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "Asset.h"
#include "../Common.h"

namespace Tian
{
	using namespace std;

	using Assets = vector<Asset_>;

	/// <summary>
	/// The asset manager manages assets loaded through. All assets are cached and removed safely. 
	/// </summary>
	class AssetManager
	{
	public:
		/// <summary>
		/// Create a new asset manager.
		/// </summary>
		AssetManager();

		/// <summary>
		/// Create a new asset manager defining the number of assets to hold.
		/// </summary>
		/// <param name="assetsSize">Max asset storage</param>
		AssetManager(const us assetsSize);

		/// <summary>
		/// Add a new asset to the manager.
		/// </summary>
		/// <param name="asset">New asset</param>
		/// <returns>Newly created asset</returns>
		template<class T>
		T* add(T* asset)
		{
			mAssets.push_back(shared_ptr<T>(asset));
			return asset;
		}

		/// <summary>
		/// Add a new asset to the manager at in index in cache.
		/// </summary>
		/// <param name="asset">New asset</param>
		/// <param name="index">Index to inser the asset at</param>
		/// <returns>Newly created asset</returns>
		template<class T>
		T* add(T* asset, const us index)
		{
			mAssets.at(index) = shared_ptr<T>(asset);
			return asset;
		}

		/// <summary>
		/// Get an asset from the manager by index. No asset is returned as NULL.
		/// </summary>
		/// <param name="index">Index in the cached to return asset</param>
		/// <returns>Handle to cached asset</returns>
		template<typename T>
		T* get(const us index) const
		{
			if (index >= mAssets.size() || index < 0)
			{
				return NULL;
			}

			return static_cast<T*>(mAssets.at(index).get());
		}

		/// <summary>
		/// Get an asset from the manager by its name. No asset is returned as NULL.
		/// </summary>
		/// <param name="index">Name of the asset to return</param>
		/// <returns>Handle to cached asset</returns>
		template<typename T>
		T* get(const str name) const
		{
			for (auto& asset : mAssets)
			{
				if (asset->getPath() == name)
				{
					return static_cast<T*>(asset.get());
				}
			}

			return NULL;
		}

		/// <summary>
		/// Get an asset index from the manager by name. No asset is returned as 0xffff.
		/// </summary>
		/// <param name="index">Name of the asset</param>
		/// <returns>Index to cached asset</returns>
		us getIndex(const str name) const
		{
			for (us i = 0; i < mAssets.size(); ++i)
			{
				if (mAssets[i]->getPath() == name)
				{
					return i;
				}
			}

			return 0xffff;
		}
	protected:
		Assets mAssets; ///< Assets cache.
	};
}
