///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Event.h"

namespace Tian
{
	/// <summary>
	/// Extends Event to present a string message to the console when its trigger time.
	/// </summary>
	class EventMsg : public Event
	{
	public:
		/// <summary>
		/// Create the event message with a start and end time and message to display.
		/// </summary>
		/// <param name="eventStart">Time to start ms</param>
		/// <param name="eventEnd">Time to end ms</param>
		/// <param name="msg">String message to print to the console</param>
		EventMsg(const ui eventStart, const ui eventEnd, const str msg);

		/// <summary>
		/// Update the event.
		/// </summary>
		virtual void update() const;

		/// <summary>
		/// Called when the event has ended.
		/// </summary>
		virtual void completed() const;
	protected:
		str mMsg; ///< String message to be displayed.
	};
}

