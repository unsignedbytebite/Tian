#include "Input.h"


namespace Tian
{
	Input::Input()
	{
		for (us i = 0; i < 127 + 255; ++i)
		{
			mKeyBuffer[i] = 0;
		}
	}
	void Input::setKey(const SDL_Keycode key, const bool flag)
	{
		mKeyBuffer[(key > 128) ? key - (1073741881 - 128) : key] = flag;
	}

	void Input::setMouse(const Uint8 key, bool flag)
	{
		mMouseBuffer[key] = flag;
	}

	bool Input::isKeydown(const SDL_Keycode code)
	{
		mKeysDown += mKeyBuffer[(code > 128) ? code - (1073741881 - 128) : code] == 1;
		return mKeyBuffer[code] == 1;
	}

	bool Input::isKeytap(const SDL_Keycode code)
	{
		if (isKeydown(code))
		{
			mKeyBuffer[(code > 128) ? code - (1073741881 - 128) : code] = 2;
			return true;
		}

		return false;
	}

	bool Input::isMousedown(const Uint8 key)
	{
		return (mMouseBuffer[key] == 1) ? true : false;
	}

	bool Input::isMousedown()
	{
		return mMouseBuffer[0] + mMouseBuffer[1] + mMouseBuffer[2] + mMouseBuffer[2] > 0;
	}

	bool Input::isMousetap()
	{
		for (Uint8 i = 0; i < 4; i++)
		{
			if (isMousedown(i))
			{
				mMouseBuffer[i] = 2;
				return true;
			}
		}

		return false;
	}

	bool Input::isMousetap(const Uint8 key)
	{
		if (isMousedown(key))
		{
			mMouseBuffer[key] = 2;
			return true;
		}

		return false;
	}

	void Input::setMouseXY()
	{
		SDL_GetMouseState(&mMouseX, &mMouseY);
	}
}