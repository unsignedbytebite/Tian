#pragma once
#include <exception>
#include "Log.h"

namespace ConBox
{
	struct ConBoxExec : public std::exception
	{
		ConBoxExec(const str msg = "", str function = "") : exception(msg.c_str())
		{
			mFunction = function;
		}

		virtual void ConBoxExec::print() = 0;

	protected:
		// Name of the function where called from
		str mFunction = "";
	};


	struct ConBoxError : public ConBoxExec
	{
		ConBoxError(const str msg = "", str function = "") : ConBoxExec(msg, function)
		{}

		// Print the error 
		virtual void ConBoxError::print()
		{
			Log::p(175, Colours::FAIL, false);

			Log::p(what(), Colours::FAIL, false);
			if (!mFunction.empty())
			{
				Log::p(" @ " + mFunction + "() : ", Colours::FAIL, false);
			}
			Log::p("");
		}
	};

	struct ConBoxWarn : public ConBoxExec
	{
		ConBoxWarn(const str msg = "", str function = "") : ConBoxExec(msg, function)
		{}

		// Print the error 
		virtual void ConBoxWarn::print()
		{
			Log::p(175, Colours::WARNING, false);

			Log::p(what(), Colours::WARNING, false);
			if (!mFunction.empty())
			{
				Log::p(" @ " + mFunction + "()", Colours::WARNING, false);
			}
			Log::p("");
		}
	};

	struct ConBoxOk : public ConBoxExec
	{
		ConBoxOk(const str msg = "", str function = "") : ConBoxExec(msg, function)
		{}

		// Print the error 
		virtual void ConBoxOk::print()
		{
			Log::p(175, Colours::CORRECT, false);

			Log::p(what(), Colours::CORRECT, false);
			if (!mFunction.empty())
			{
				Log::p(" @ " + mFunction + "() : ", Colours::CORRECT, false);
			}
			Log::p("");
		}
	};
}
