#pragma once
#include <iostream>
#include <string>
#include <vector>

#if _WIN32 || _WIN64
#include <windows.h>
#endif

namespace ConBox
{
	using str = std::string;

	// Registered colours
	enum Colours
	{
		NORMAL = 0, TITLE, LABEL, CORRECT, FAIL, DIM, BRIGHT, WARNING, SIZE
	};

	class Log
	{
	public:
		//Log();
		//~Log();

		static void startColour(unsigned char colour)
		{
			// Index of colours used by the platform's console
			const unsigned char COLOURS[SIZE]
			{
#if _WIN32 || _WIN64
				0x07, 0x5d, 0x1b, 0x2a, 0x4c, 0x08, 0x0f, 0x6e
#endif
			};

#if _WIN32 || _WIN64
			HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
			CONSOLE_SCREEN_BUFFER_INFO csbi;
			GetConsoleScreenBufferInfo(hstdout, &csbi);
			SetConsoleTextAttribute(hstdout, COLOURS[colour]);
#endif
		}

		static void endColour()
		{
#if _WIN32 || _WIN64
			HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
			CONSOLE_SCREEN_BUFFER_INFO csbi;
			GetConsoleScreenBufferInfo(hstdout, &csbi);
			SetConsoleTextAttribute(hstdout, csbi.wAttributes);
#endif
		}

		// Print out
		template <typename T>
		static void p(const T& val, unsigned char colour = NORMAL, const bool newLine = true)
		{
			startColour(colour);
			std::cout << val;

			if (newLine)
			{
				std::cout << std::endl;
			}
			endColour();
		}

		// Print a list
		template<typename T, typename... List>
		static void pList(const T& firstEntry, const List&... list)
		{
			p(firstEntry);
			pList(list...);
		}

		// Print out bool
		static void p(const bool& val, unsigned char colour = NORMAL, const bool newLine = true)
		{
			p(((val) ? "true" : "false"), colour, newLine);
		}

		//template <typename T>
		//void operator<<(const T& val)
		//{
		//	p(val);
		//}

		// Press any key to continue prompt
		static void pause()
		{
			p("");
			p("Press any key...", Colours::DIM);
			std::cin.ignore();
			std::cin.get();
		}

		// Display title
		static void title(const str& text)
		{
			p("[  " + text + "  ]", Colours::TITLE);
		}

		// Display an error
		static void error(const str& text)
		{
			p(text, Colours::FAIL);
		}


		// Display a label
		static void label(const str& text)
		{
			p("#  " + text, Colours::LABEL);
		}

		// Test the parity of two values 
		template <typename T>
		static bool test(const str& msg, const T& expectedValue, const T& actualValue, const str& function = "")
		{
			const Colours colour = (actualValue == expectedValue) ? ConBox::CORRECT : Colours::FAIL;

			if (!function.empty())
			{
				p(function + "() : ", colour, false);
			}

			p(msg + " = ", colour, false);
			p(actualValue, colour, false);
			p("(", colour, false);
			p(expectedValue, colour, false);
			p(")", colour);

			return expectedValue == actualValue;
		}

		template<typename T>
		static T read()
		{
			T r;
			Log::p(">> ", Colours::BRIGHT, false);
			Log::p("", Colours::NORMAL, false);
			std::cin >> r;
			return r;
		}

		//// Print out array
		//static void printArray(const int argc, char* argv[])
		//{
		//	for (unsigned int i = 0; i < (unsigned int)argc; i++)
		//	{
		//		p(std::to_string(i) + ". " + str{ argv[i] });
		//	}
		//}

		//static void printVector(const std::vector<str>* textVector)
		//{
		//	for (size_t i = 0; i < textVector->size(); i++)
		//	{
		//		p(std::to_string(i) + ". " + textVector->at(i));
		//	}
		//}

		//static void printFailVector(const std::vector<str>* textVector)
		//{
		//	for (unsigned int i = 0; i < textVector->size(); i++)
		//	{
		//		p(std::to_string(i) + ". " + textVector->at(i), FAIL);
		//	}
		//}

		//// Convert a char array to a vector of strings
		//static std::vector<str> convertToVectorString(const int argc, char* argv[])
		//{
		//	std::vector<str> r;

		//	for (unsigned int i = 0; i < (unsigned int)argc; i++)
		//	{
		//		r.push_back(str{ argv[i] });
		//	}

		//	return r;
		//}
	};
}

