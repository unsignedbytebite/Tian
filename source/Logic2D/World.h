///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Utils/Vector2.h"
#include "../Utils/Box.h"
#include "../Logic2D/Object2D.h"
#include "../Box2d/Box2D.h"
#include "../Box2d/Box2DebugDraw.h"
#include "../IO/FileIO.h"

namespace Tian
{
	/// <summary>
	/// A world a physics body to allow for the creation of objects that behave to the rules of these physics.
	/// </summary>
	class World
	{
	public:
		/// <summary>
		/// Default constructor creates the physics world.
		/// </summary>
		World();

		/// <summary>
		/// Create a new object in the world that hooks up the object's body to the physics model.
		/// </summary>
		/// <param name="object">The new object to add to the world</param>
		template<class T>
		T& createObject(Object2D* object)
		{
			b2BodyDef bodyDef;
			bodyDef.position.Set(0, 0);
			auto& body = *mPhysWorld->CreateBody(&bodyDef);
			object->setBody(body);
			object->created();
			body.SetUserData(object);
			mObjects.push_back(std::shared_ptr<Object2D>(object));
			return *static_cast<T*>(object);
		}

		/// <summary>
		/// Create the debug renderer for the world. This debug renderer will show a wireframe of the physic bodies.
		/// </summary>
		/// <param name="debugDraw">The debug renderer to use</param>
		void createDebugRenderer(Box2DebugDraw* debugDraw);

		/// <summary>
		/// Add a static fixed object into the world. Ideally suited for walls.
		/// </summary>
		/// <param name="type">The object type to allow for filtering during contact events</param>
		/// <param name="x">The x position in world space</param>
		/// <param name="y">The y position in world space</param>
		/// <param name="width">The width position in world space</param>
		/// <param name="height">The height position in world space</param>
		/// <param name="density">The density of the object</param>
		/// <param name="isTrigger">True to set the body as a sensor</param>
		virtual Object2D& createStatic(const us type, const float x, const float y, const float width, const float height, const float density = 1, const bool isTrigger = false);

		/// <summary>
		/// Add a static fixed object into the world. Ideally suited for walls.
		/// </summary>
		/// <param name="type">The object type to allow for filtering during contact events</param>
		/// <param name="x">The x position in world space</param>
		/// <param name="y">The y position in world space</param>
		/// <param name="points">The polygonal collection of points that make up the body</param>
		/// <param name="height">The height position in world space</param>
		/// <param name="density">The density of the object</param>
		/// <param name="isTrigger">True to set the body as a sensor</param>
		virtual Object2D& createStatic(const us type, const float x, const float y, const Vector2Points points, const float density = 1, const bool isTrigger = false);

		/// <summary>
		/// Remove a object from the world.
		/// </summary>
		/// <param name="object">Object to remove</param>
		void remove(Object2D& object);

		/// <summary>
		/// Remove all objects from the world.
		/// </summary>
		void remove();

		/// <summary>
		///  Update the world.
		/// </summary>
		virtual void update();

		/// <summary>
		/// Draw the debug renderer.
		/// </summary>
		void drawDebug();

		/// <summary>
		/// Save the serialization of the world. All objects are rendered out.
		/// </summary>
		/// <param name="file">Path to save out to</param>
		virtual void serializeOut(FileIO& file);

		/// <summary>
		/// Load in a world. To be implemented.
		/// </summary>
		/// <param name="file">File to load in</param>
		virtual void serializeIn(FileIO& file);
	protected:
		std::shared_ptr<b2World> mPhysWorld; ///< The physics world.
		std::shared_ptr<Box2DebugDraw> mDebugDraw; ///< The world debug renderer.
		std::vector<std::shared_ptr<Object2D>> mObjects; ///< All world object cache.
	};
}

