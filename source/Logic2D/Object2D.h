///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Logic/Object.h"
#include "../Utils/Vector2.h"
#include "../Utils/Boxes.h"
#include "../Utils/Maths.h"
#include "../Box2D/Box2D.h"
#include "../IO/FileIO.h"

namespace Tian
{
	/// <summary>
	/// Generic class of a object in a 2D world. This object contains physics properties.
	/// </summary>
	class Object2D :
		public Object
	{
	public:
		/// <summary>
		/// Create the object with an id.
		/// </summary>
		/// <param name="id">Object id</param>
		Object2D(const ui id = 0) : Object(id), mLocal({ 0, 0 }), mDir(0)
		{};

		/// <summary>
		/// Get the location of the object.
		/// </summary>
		/// <returns>Object location</returns>
		const Vector2 getLocal() const;

		/// <summary>
		/// Set the location of the object. Moved() is also called within this function.
		/// </summary>
		/// <param name="val">New location to set</param>
		void setLocal(const Vector2 val);

		/// <summary>
		/// A handle to the location of the object.
		/// </summary>
		/// <returns>Object location</returns>
		Vector2& local();

		/// <summary>
		/// Get the direction of the object.
		/// </summary>
		/// <returns>Object direction</returns>
		const float getDirection() const;

		/// <summary>
		/// Set the direction of the object.
		/// </summary>
		/// <param name="val">Object direction</param>
		void setDirection(const float val);

		/// <summary>
		/// Handle to the object direction.
		/// </summary>
		/// <returns>Reference to the object direction</returns>
		float& direction();

		/// <summary>
		/// Get a bounding box by index.
		/// </summary>
		/// <param name="index">Index of the bounding box</param>
		/// <returns>Handle to the bounding box</returns>
		Box& getBounds(const us index = 0);

		/// <summary>
		/// Set the bounding boxes for the object
		/// </summary>
		/// <param name="val">Collection of boxes</param>
		void setBounds(const Boxes val);

		/// <summary>
		/// Handle to the bounding box collection
		/// </summary>
		/// <returns>Handle to the object bounds</returns>
		Boxes& bounds();

		/// <summary>
		/// Called when the object has been created. If created by a world object then this function is called from there.
		/// </summary>
		virtual void created();

		/// <summary>
		/// Move the object by an velocity vector.
		/// </summary>
		/// <param name="velocity">Vector to move on</param>
		virtual void move(const Vector2& velocity);

		/// <summary>
		/// Move the object in a direction by a velocity.
		/// </summary>
		/// <param name="direction">Direction to move in</param>
		/// <param name="velocity">Velocity of the object</param>
		virtual void move(const float direction, const float velocity);

		/// <summary>
		/// Move the object in the current set direction.
		/// </summary>
		/// <param name="velocity">Velocity of the object</param>
		virtual void move(const float velocity);

		/// <summary>
		/// Called when after move() has been called. Can be implemented as a callback.
		/// </summary>
		virtual void moved();

		/// <summary>
		/// Add a bounding box using this object's location as reference in the box.
		/// </summary>
		/// <param name="rect">Box bounds</param>
		virtual void addBounding(const SDL_Rect rect);

		/// <summary>
		/// Set the location of the body position. The body is used in the physics processing and separate from the object position and orientation. 
		/// </summary>
		/// <param name="x">The x position</param>
		/// <param name="y">The y position</param>
		/// <param name="rot">Rotation</param>
		void setBodyLocation(const float x, const float y, const float rot = 0);

		/// <summary>
		/// Set the location of the body position. The body is used in the physics processing and separate from the object position and orientation. 
		/// </summary>
		/// <param name="location">The vector location position</param>
		/// <param name="rot">Rotation</param>
		void setBodyLocation(const Vector2& location, const float rot = 0);

		/// <summary>
		/// Get the handle to the physics body.
		/// </summary>
		/// <returns>The object's physics body</returns>
		b2Body* body();

		/// <summary>
		/// Get the handle to the physics body.
		/// </summary>
		/// <returns>The object's physics body</returns>
		b2Body& getBody() const;

		/// <summary>
		/// Set the physics body.
		/// </summary>
		/// <param name="body">Body handle</param>
		void setBody(b2Body& body);

		/// <summary>
		/// Serialize in this object. To be implemented.
		/// </summary>
		/// <param name="file">File object</param>
		virtual void serializeIn(FileIO& file);

		/// <summary>
		/// Serialize out this object. To be implemented.
		/// </summary>
		/// <param name="file">File object</param>
		virtual void serializeOut(FileIO& file);

		/// <summary>
		/// Set the bitmask filter for the body fixture. In the physics model objects that are filtered do not collide.
		/// </summary>
		/// <param name="category">The category of the body</param>
		/// <param name="mask">Which other bodies to mask against</param>
		/// <param name="groupIndex">The group setting for this body</param>
		virtual void setFilter(const us category = 0x0001, const us mask = 0xFFFF, const us groupIndex = 0);
	protected:
		b2Body* mBody; ///< The physics model body.
		Vector2 mLocal; ///< The object location.
		Boxes mBounds; ///< Bounding box collection.
		float mDir; ///< Direction of the object.
	};
}
