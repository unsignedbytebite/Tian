///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "World.h"
#include "../Graphics/2D/Actor.h"
#include "../Graphics/2D/Canvas.h"

namespace Tian
{
	/// <summary>
	/// World 2D extends World and adds common 2D business code requirements. Canvas, camera and creating Actor objects.
	/// </summary>
	class World2D : public World
	{
	public:
		World2D(Camera2D& camera, const float worldUnitsToPixels = 1.f);

		/// <summary>
		/// Create a new Actor in the world that hooks up the object's body to the physics model.
		/// </summary>
		/// <param name="object">The new Actor to add to the world</param>
		Actor& createObject(Actor* object)
		{
			b2BodyDef bodyDef;
			bodyDef.position.Set(0, 0);
			auto& body = *mPhysWorld->CreateBody(&bodyDef);
			object->setBody(body);
			object->created();
			body.SetUserData(object);
			mObjects.push_back(std::shared_ptr<Actor>(object));
			return *object;
		}

		/// <summary>
		/// Update the game world
		/// </summary>
		virtual void update();

		/// <summary>
		/// Draw the world and it's objects
		/// </summary>
		virtual void draw();

		/// <summary>
		/// Draw a animated texture to the canvas
		/// </summary>
		/// <param name="gfx">Animated texture to use</param>
		/// <param name="location">Location to draw the texture</param>
		void drawToCanvas(TextureAnimated& gfx, Vector2& location);

		/// <summary>
		///  Clear the canvas with alpha.
		/// </summary>
		void clearCanvas();
	protected:
		std::vector<std::shared_ptr<Actor>> mObjects; ///< All actors held by the world.
		std::unique_ptr<Canvas> mCanvas; ///< The world canvas layer.
		const float mWorldUnitsToPixels; ///< World units to screen pixels ration.
		Camera2D& mCam; ///< The camera attached to the world
	};
}

