#include "World2D.h"

namespace Tian
{
	World2D::World2D(Camera2D& camera, const float worldUnitsToPixels) : World(), mCam { camera }, mWorldUnitsToPixels { worldUnitsToPixels }
	{
		// Setup default can
		mCanvas = std::unique_ptr<Canvas>(new Canvas{ &mCam });
	}

	void World2D::update()
	{
		for (auto& obj : mObjects)
		{
			obj->update();
		}
		World::update();
	}

	void World2D::draw()
	{
		mCanvas->draw();

		for (auto& obj : mObjects)
		{
			obj->drawInWorld(mWorldUnitsToPixels);
		}
	}

	void World2D::drawToCanvas(TextureAnimated& gfx, Vector2& location)
	{
		mCanvas->drawTexture(gfx, location);
	}

	void World2D::clearCanvas()
	{
		mCanvas->fillWithCanvasTexture();
	}
}
