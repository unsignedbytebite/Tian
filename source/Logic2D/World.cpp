#include "World.h"

namespace Tian
{
	World::World()
	{
		mPhysWorld = std::shared_ptr<b2World>(new b2World{ { 0, 0 } });
	}
	void World::createDebugRenderer(Box2DebugDraw* debugDraw)
	{
		mDebugDraw = std::shared_ptr<Box2DebugDraw>(debugDraw);
		mPhysWorld->SetDebugDraw(mDebugDraw.get());
	}
	Object2D& World::createStatic(const us type,
		const float x, const float y, const float width, const float height, const float density, const bool isTrigger)
	{
		// Create a new attached object for the body
		auto& obj = createObject<Object2D>(new Object2D{ type });
		obj.setBodyLocation(x, y);
		obj.body()->SetType(b2_staticBody);

		// Define the ground box shape.
		b2PolygonShape box;
		box.SetAsBox(width, height);
		auto& fixture = *obj.body()->CreateFixture(&box, density);

		// Set is sensor
		fixture.SetSensor(isTrigger);

		return obj;
	}
	Object2D& World::createStatic(const us type, const float x, const float y, const Vector2Points points, const float density, const bool isTrigger)
	{
		// Create a new attached object for the body
		auto& obj = createObject<Object2D>(new Object2D{ type });
		obj.setBodyLocation(x, y);
		obj.body()->SetType(b2_staticBody);

		// Define the ground box shape.
		b2PolygonShape box;
		box.Set(&points[0], (int)points.size());
		auto& fixture = *obj.body()->CreateFixture(&box, density);

		// Set is sensor
		fixture.SetSensor(isTrigger);

		return obj;
	}
	void World::remove(Object2D& object)
	{
		for (auto i = 0; i < (us)mObjects.size(); ++i)
		{
			if (mObjects[i].get() == &object)
			{
				mPhysWorld->DestroyBody(&object.getBody());
				mObjects.erase(mObjects.begin() + i);
				return;
			}
		}
	}
	void World::remove()
	{
		for (auto& obj : mObjects)
		{
			mPhysWorld->DestroyBody(obj->body());
		}

		mObjects.clear();
	}
	void World::update()
	{
		const auto timeStep = 1.0f / 60.0f;
		const auto velocityIterations = 6;
		const auto positionIterations = 2;
		mPhysWorld->Step(timeStep, velocityIterations, positionIterations);
	}
	void World::drawDebug()
	{
		mPhysWorld->DrawDebugData();
	}
	void World::serializeOut(FileIO& file)
	{
		// Number of objects
		const us objectsCount = (us)mObjects.size();
		file.write(objectsCount);

		// Serialize data entry
		for (auto i = 0; i < objectsCount; ++i)
		{
			const auto& obj = mObjects[i].get();
			obj->serializeOut(file);
		}
	}
	void World::serializeIn(FileIO&)
	{}
}
