#include "Boxes.h"

namespace Tian
{
	Boxes::Boxes(const BoxList boxes) : mBoxes(boxes)
	{}

	Boxes::Boxes()
	{}

	Boxes::~Boxes()
	{}

	void Boxes::add(const Box box)
	{
		mBoxes.push_back(box);
	}

	void Boxes::clear()
	{
		mBoxes.clear();
	}

	void Boxes::updateLocation(const Vector2 location)
	{
		for (ui i = 0; i < mBoxes.size(); i++)
		{
			mBoxes[i].setLocation(location);
		}
	}

	void Boxes::setBoxes(const BoxList boxes)
	{
		mBoxes = boxes;
	}

	BoxList& Boxes::boxes()
	{
		return mBoxes;
	}
	bool Boxes::contains(const int targetX, const int targetY) const
	{
		for (const Box& box : mBoxes)
		{
			if (box.contains(targetX, targetY))
			{
				return true;
			}
		}

		return false;
	}
	bool Boxes::contains(const Vector2& point) const
	{
		for (const Box& box : mBoxes)
		{
			if (box.contains(point))
			{
				return true;
			}
		}

		return false;
	}
	bool Boxes::contains(const Box& box) const
	{
		for (const Box& b : mBoxes)
		{
			if (b.contains(box))
			{
				return true;
			}
		}

		return false;
	}
	bool Boxes::intersects(const Box& box) const
	{
		for (const Box& b : mBoxes)
		{
			if (b.intersects(box))
			{
				return true;
			}
		}

		return false;
	}
	bool Boxes::intersects(const Boxes& boxes) const
	{
		for (const Box& box : mBoxes)
		{
			for (const Box& box2 : boxes.mBoxes)
			{
				if (box.intersects(box2))
				{
					return true;
				}
			}
		}

		return false;
	}
	bool Boxes::contains(const Boxes& boxes) const
	{
		for (const Box& box : mBoxes)
		{
			for (const Box& box2 : boxes.mBoxes)
			{
				if (box.contains(box2))
				{
					return true;
				}
			}
		}

		return false;
	}
}