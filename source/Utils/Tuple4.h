///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	template<class T>
	class Tuple4
	{
	public:
		T x{ 0 };
		T y{ 0 };
		T w{ 0 };
		T h{ 0 };

		Tuple4(const T setX = 0, const T setY = 0, const T setW = 0, const T setH = 0)
		{
			set(setX, setY, setW, setH);
		};
		Tuple4(const Tuple4& copyVal)
		{
			set(copyVal);
		}
		~Tuple4()
		{};

		// Operators
		Tuple4 operator+(const Tuple4& a) const
		{
			return Tuple4{ x + a.x, y + a.y, w + a.w, h + a.h };
		}

		Tuple4 operator-(const Tuple4& a) const
		{
			return Tuple4{ x - a.x, y - a.y, w - a.w, h - a.h };
		}

		Tuple4 operator*(const Tuple4& a) const
		{
			return Tuple4{ x * a.x, y * a.y, w * a.w, h * a.h };
		}

		Tuple4 operator*(const T& a) const
		{
			return Tuple4{ x * a, y * a, w * a, h * a };
		}

		Tuple4 operator=(const T& a) const
		{
			set(a);
			return this;
		}

		bool operator==(const T& a) const
		{
			return equals(a);
		}

		str getText() const
		{
			return "[" + toString(x) + ", " + toString(y) + ", " + toString(w) + ", " + toString(h) + "]";
		}

		// Set the new values
		void set(const T setX, const T setY, const T setW, const T setH)
		{
			x = setX;
			y = setY;
			w = setW;
			h = setH;
		}
		void set(const Tuple4& val)
		{
			x = val.x;
			y = val.y;
			w = val.w;
			h = val.h;
		}

		// Get the tuple values
		T getIntX() const
		{
			return x;
		}
		T getIntY() const
		{
			return y;
		}
		T getIntW() const
		{
			return w;
		}
		T getIntH() const
		{
			return h;
		}

		// Move the current values
		void move(const T moveX, const T moveY, const T moveW, const T moveH)
		{
			x += moveX;
			y += moveY;
			w += moveW;
			h += moveH;
		}
		void move(const Tuple4& val)
		{
			x += val.x;
			y += val.y;
			w += val.w;
			h += val.h;
		}

		// Does val equal this point
		bool equals(const Tuple4& val) const
		{
			return (val.x == x && val.y == y && val.w == w && val.h == h);
		}

		// Round a Tuple4
		static Tuple4 doRound(const Tuple4& val)
		{
			return{ round(val.x), round(val.y), round(val.w), round(val.h) };
		}

		// Floor a Tuple4
		static Tuple4 doFloor(const Tuple4& val)
		{
			return{ floor(val.x), floor(val.y), floor(val.w), floor(val.h) };
		}

		// Return the sum of two values
		static Tuple4 add(const Tuple4& valA, const Tuple4& valB)
		{
			return Tuple4{ valA.x + valB.x, valA.y + valB.y, valA.w + valB.w, valA.h + valB.h };
		}
	};

	// Typedefines
	typedef Tuple4<double> Double4;
	typedef Tuple4<float> Float4;
	typedef Tuple4<int> Int4;
}

