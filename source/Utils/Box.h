///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Vector2.h"

namespace Tian
{
	class Box;
	using BoxList = std::vector<Box>;

	/// <summary>
	/// A rect object with a referenced location that. Use to have a location synced box that can be used a for bounding box.
	/// </summary>
	class Box
	{
	public:
		/// <summary>
		/// Create a new box with a location reference and a defined rect.
		/// </summary>
		/// <param name="location">The location reference to use</param>
		/// <param name="rect">New rect object</param>
		Box(Vector2& location, const SDL_Rect rect);

		/// <summary>
		/// Create box with a default location and a rect.
		/// </summary>
		/// <param name="rect">New rect object</param>
		Box(const SDL_Rect rect);

		/// <summary>
		/// Default box using the default location handle.
		/// </summary>
		Box();

		/// <summary>
		/// Check if a box equals another.
		/// </summary>
		/// <param name="box">Box to test against</param>
		/// <returns>True if the same</returns>
		virtual const Box operator=(const Box& box);

		/// <summary>
		/// Divide the rect by a Vector2.
		/// </summary>
		/// <param name="Vector2">Vector2 divide value</param>
		/// <returns>New box divided by value</returns>
		virtual Box operator/(const Vector2& Vector2);

		/// <summary>
		/// Add the rect by a Vector2.
		/// </summary>
		/// <param name="Vector2">Vector2 add value</param>
		/// <returns>New box added by value</returns>
		virtual Box operator+(const Vector2& Vector2);

		/// <summary>
		/// Set the rect object.
		/// </summary>
		/// <param name="rect">New rect object</param>
		void setRect(const SDL_Rect rect);

		/// <summary>
		/// Get the SDL rect.
		/// </summary>
		/// <returns>The rect used by the box</returns>
		SDL_Rect getRect() const;

		/// <summary>
		/// Get the location offset rect. Can be used as a bounding box.
		/// </summary>
		/// <returns>Location offset rect</returns>
		SDL_Rect getBounds() const;

		/// <summary>
		/// Set the location reference of the box
		/// </summary>
		/// <param name="location">New location reference</param>
		void setLocation(const Vector2& location);

		/// <summary>
		/// Get the location reference used by the box.
		/// </summary>
		/// <returns>Box reference</returns>
		Vector2& getLocal() const;

		/// <summary>
		/// Check is a point is contained within the box.
		/// </summary>
		/// <param name="targetX">X position</param>
		/// <param name="targetY">Y position</param>
		/// <returns>True if the point is contained on the box</returns>
		bool contains(const int targetX, const int targetY) const;

		/// <summary>
		/// Check is a point is contained within the box.
		/// </summary>
		/// <param name="point">Position</param>
		/// <returns>True if the point is contained on the box<</returns>
		bool contains(const Vector2& point) const;

		/// <summary>
		/// Test if another box is contained this box.
		/// </summary>
		/// <param name="box">Box to test against</param>
		/// <returns>True if the box is contained</returns>
		bool contains(const Box& box) const;

		/// <summary>
		/// Test if another box intersects this box.
		/// </summary>
		/// <param name="box">Box to test against</param>
		/// <returns>True if the box is intersects</returns>
		bool intersects(const Box& box) const;

		/// <summary>
		/// Get the left side of the box.
		/// </summary>
		/// <returns>Location offset left side</returns>
		const int left() const;

		/// <summary>
		/// Get the right side of the box.
		/// </summary>
		/// <returns>Location offset right side</returns>
		const int right() const;

		/// <summary>
		/// Get the top side of the box.
		/// </summary>
		/// <returns>Location offset top side</returns>
		const int top() const;

		/// <summary>
		/// Get the bottom side of the box.
		/// </summary>
		/// <returns>Location offset bottom side</returns>
		const int bottom() const;

		/// <summary>
		/// Get the centreX of the box.
		/// </summary>
		/// <returns>Location offset centreX</returns>
		const int centreX() const;

		/// <summary>
		/// Get the centreY of the box.
		/// </summary>
		/// <returns>Location offset centreY</returns>
		const int centreY() const;

		/// <summary>
		/// Get the top left XY position of the box.
		/// </summary>
		/// <returns>Top left position</returns>
		const Vector2 topLeft() const;

		/// <summary>
		/// Get the top right XY position of the box.
		/// </summary>
		/// <returns>Top right position</returns>
		const Vector2 topRight() const;

		/// <summary>
		/// Get the bottom left XY position of the box.
		/// </summary>
		/// <returns>Bottom left position</returns>
		const Vector2 bottomLeft() const;

		/// <summary>
		/// Get the bottom right XY position of the box.
		/// </summary>
		/// <returns>Bottom right position</returns>
		const Vector2 bottomRight() const;
	private:
		SDL_Rect mRect; ///< The rect of the box.
		Vector2& mLoco; ///< The reference location to offset the erect against.
		static Vector2 mDefault; ///< Default location for a box without at location reference.
	};
}