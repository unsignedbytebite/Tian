///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// A circular buffer of defined type. 
	/// </summary>
	template<class T>
	class Buffer
	{
	public:
		/// <summary>
		/// Create a new buffer with the option to wrap changes in value.
		/// </summary>
		/// <param name="wrap">Wrap values</param>
		Buffer(const bool wrap = true) : isWrapped{ wrap }
		{}

		/// <summary>
		/// Create a new buffer with a collection of values to cycle through.
		/// </summary>
		/// <param name="values">The data collection to act as the buffer</param>
		/// <param name="wrap">Wrap values</param>
		Buffer(const std::vector<T>& values, const bool wrap = true) : isWrapped{ wrap }
		{
			add(values);
		}

		/// <summary>
		/// Step through the buffer by 1 step.
		/// </summary>
		/// <returns>The current buffer handle</returns>
		T& operator++()
		{
			stepIndex(1);
			return get();
		}

		/// <summary>
		/// Step through the buffer by -1 step.
		/// </summary>
		/// <returns>The current buffer handle</returns>
		T& operator--()
		{
			stepIndex(-1);
			return get();
		}

		/// <summary>
		/// Get the value at current index.
		/// </summary>
		/// <returns>Buffer value in the list.</returns>
		T& get()
		{
			return mList.at(mIndex);
		}

		/// <summary>
		/// Add a value onto the buffer
		/// </summary>
		/// <param name="value">Value to add onto the buffer</param>
		void add(const T value)
		{
			mList.push_back(value);
		}

		/// <summary>
		/// Set the size of the buffer
		/// </summary>
		/// <param name="size">Reserve size</param>
		template<class NewType>
		void populate(const ui size)
		{
			this->mList.reserve(size);
		}

		/// <summary>
		/// Add values onto the buffer
		/// </summary>
		/// <param name="values">Values to push onto the buffer</param>
		void add(const std::vector<T>& values)
		{
			for (T element : values)
			{
				mList.push_back(element);
			}
		}

		/// <summary>
		/// Step through the buffer by an amount.
		/// </summary>
		/// <param name="stepAmount">Step amount</param>
		void stepIndex(const int stepAmount)
		{
			const int sum = stepAmount + mIndex;

			// Check for less
			if (sum < 0)
			{
				if (isWrapped)
				{
					mIndex = (stepAmount + (int)mList.size());
				}
				else
				{
					mIndex = (ui)mList.size() - 1;
				}
			}
			// Check for upper
			else if ((unsigned int)sum >= mList.size())
			{
				if (isWrapped)
				{
					mIndex = (stepAmount + (ui)mList.size()) - ((ui)mList.size() + 1);
				}
				else
				{
					mIndex = (ui)mList.size() - 1;
				}
			}
			else
			{
				mIndex = sum;
			}
		}

		/// <summary>
		/// Get the current buffer index.
		/// </summary>
		/// <returns>Current buffer index</returns>
		ui getCurrentIndex() const
		{
			return mIndex;
		}

		/// <summary>
		/// Get the data collection of the buffer.
		/// </summary>
		/// <returns>Buffer collection</returns>
		std::vector<T>& getList()
		{
			return mList;
		}

		/// <summary>
		/// Clear the buffer.
		/// </summary>
		void clear()
		{
			mIndex = 0;
			mList.clear();
		}

		/// <summary>
		/// Set the index of the buffer
		/// </summary>
		/// <param name="index">New buffer index</param>
		void set(const ui index)
		{
			mIndex = index;
		}

	private:
		std::vector<T> mList; ///< Buffer data collection.
		ui mIndex{ 0 }; ///< Current buffer index.
		bool isWrapped; ///< Buffer warpping flag.
	};
}
