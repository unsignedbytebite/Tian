#include "FrameSets.h"

namespace Tian
{
	void FrameSets::serializeIn(FileIO& file)
	{
		// read number of sets
		const auto frameSetsCount = file.read<us>();

		for (us i = 0; i < frameSetsCount; ++i)
		{
			FrameSet set;

			// Frame delay
			const auto delay = file.read<us>();
			set.setFrameDelay(delay);

			const auto setSize = file.read<us>();

			for (us c = 0; c < setSize; ++c)
			{
				const SDL_Rect rect = file.read<SDL_Rect>();
				set.add(rect);
			}
			mSets.push_back(set);
		}
	}
	void FrameSets::serializeOut(FileIO& file) const
	{
		// Write number of sets
		const auto frameSets = mSets.size();
		file.write<us>((us)frameSets);

		for (us i = 0; i < frameSets; ++i)
		{
			// Frame delay
			const auto delay = mSets.at(i).getFrameDelay();
			file.write(delay);

			const FrameSet set = mSets.at(i);
			const auto setSize = set.getFrames().size();

			file.write<us>((us)setSize);

			for (us c = 0; c < setSize; ++c)
			{
				file.write<SDL_Rect>(set.getFrames()[c]);
			}
		}
	}
	void FrameSets::add(const FrameSet set)
	{
		mSets.push_back(set);
	}
	std::vector<FrameSet>& FrameSets::get()
	{
		return mSets;
	}
	const us FrameSets::size() const
	{
		return (us)mSets.size();
	}
	FrameSet& FrameSets::at(const us index)
	{
		return mSets.at(index);
	}
}
