#pragma once
#include "Tuple2.h"
#include "../Common.h"

namespace Tian
{
	using namespace std;

	/// <summary>
	/// A 2D array of defined type.
	/// </summary>
	template<class T>
	class Map2D
	{
	public:
		/// <summary>
		/// Create a new map of width and height.
		/// </summary>
		/// <param name="width">Map width</param>
		/// <param name="height">Map height</param>
		Map2D(const us width, const us height)
		{
			mWidth = width;
			mHeight = height;

			mMap = new T[width * height];

			for (us x = 0; x < width; x++)
			{
				for (us y = 0; y < height; y++)
				{
					mMap[width * y + x] = T{};
				}
			}
		};
		~Map2D()
		{
			delete mMap;
			mMap = 0;
		};

		/// <summary>
		/// Get the width of the map.
		/// </summary>
		/// <returns>MAp width</returns>
		us getWidth()
		{
			return mWidth;
		}

		/// <summary>
		/// Get the height of the map
		/// </summary>
		/// <returns>Map height</returns>
		us getHeight()
		{
			return mHeight;
		}

		/// <summary>
		/// Get an element at the point location.
		/// </summary>
		/// <param name="index">2D index location on the map</param>
		/// <returns>Data type at the map location</returns>
		T* get(const Us2& index)
		{
			return &mMap[mWidth * index.y + index.x];
		}

		/// <summary>
		/// Set the node at a location.
		/// </summary>
		/// <param name="indexX">Map location X</param>
		/// <param name="indexY">Map location Y</param>
		/// <param name="node">Node data type to insert into the map</param>
		void set(const us indexX, const us indexY, const T node)
		{
			mMap[mWidth * indexY + indexX] = node;
		}

		/// <summary>
		/// Get an element at the point location.
		/// </summary>
		/// <param name="indexX">Map location X</param>
		/// <param name="indexY">Map location Y</param>
		/// <returns>Data type at the map location</returns>
		T* get(const us indexX, const us indexY)
		{
			return &mMap[mWidth * indexY + indexX];
		}

		/// <summary>
		/// Checks if the index is in bounds of the map.
		/// </summary>
		/// <param name="indexX">Map location X</param>
		/// <param name="indexY">Map location Y</param>
		/// <returns>True if in bounds</returns>
		const bool isInBounds(const us indexX, const us indexY) const
		{
			return indexX < mWidth && indexY < mHeight;
		}

		/// <summary>
		/// Set the node at a location
		/// </summary>
		/// <param name="index">2D index location on the map</param>
		/// <param name="node">Node data type to insert into the map</param>
		void set(const Us2& index, const T node)
		{
			mMap[mWidth * index.y + index.x] = node;
		}

		/// <summary>
		/// Get a list of neighbors at a location on the map.
		/// </summary>
		/// <param name="location">2D index location on the map</param>
		/// <returns>Collection of nodes.</returns>
		vector<T*> getNeighbors(const Us2& location)
		{
			vector<T*> returns;

			const Short2 up{ 0, 1 };
			const Short2 down{ 0, -1 };
			const Short2 right{ 1, 0 };
			const Short2 left{ -1, 0 };

			// Generate directions
			const Short2 dirs[8]{
				left,
				right,
				up,
				down,
				left + up,
				left + down,
				right + up,
				right + down };

			for (us i = 0; i < 8; i++)
			{
				// Check if in bounds & node is not null
				const Us2 calulatedDir{
					(location.x + dirs[i].x),
					(location.y + dirs[i].y) };

				if (calulatedDir.isBounded(mWidth, mHeight))
				{
					T* r = at(calulatedDir);

					// Check for null
					if (r != NULL)
					{
						returns.push_back(r);
					}
				}
			}

			return returns;
		}

	protected:
		T* mMap; ///< Map space array
		us mWidth, mHeight; ///< Map dimensions
	};
}

