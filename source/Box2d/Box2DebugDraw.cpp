#include "Box2DebugDraw.h"

namespace Tian
{
	Box2DebugDraw::Box2DebugDraw(Camera2D& cam) : b2Draw(), mCam(cam)
	{
		m_drawFlags = b2Draw::e_shapeBit | b2Draw::e_centerOfMassBit | b2Draw::e_jointBit | b2Draw::e_pairBit; // | b2Draw::e_aabbBit
	}
	Box2DebugDraw::~Box2DebugDraw()
	{}
	void Box2DebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
	{
		DrawSolidPolygon(vertices, vertexCount, color);
	}
	void Box2DebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
	{
		SDL_SetRenderDrawColor(&mCam.renderer(),
			(Uint8)(255 * color.g),
			(Uint8)(255 * color.r),
			(Uint8)(255 * color.b),
			(Uint8)(255 * color.a));

		// All points
		for (auto i = 0; i < vertexCount - 1; i++)
		{
			const auto camOffset = b2Vec2{
				vertices[i].x - mCam.local().x,
				vertices[i].y - mCam.local().y
			};

			const auto camOffset2 = b2Vec2{
				vertices[i + 1].x - mCam.local().x,
				vertices[i + 1].y - mCam.local().y
			};

			SDL_RenderDrawLine(&mCam.renderer(), (int)camOffset.x, (int)camOffset.y, (int)camOffset2.x, (int)camOffset2.y);
		}

		const auto camOffset = b2Vec2{
			vertices[0].x - mCam.local().x,
			vertices[0].y - mCam.local().y
		};

		const auto camOffset2 = b2Vec2{
			vertices[vertexCount - 1].x - mCam.local().x,
			vertices[vertexCount - 1].y - mCam.local().y
		};

		SDL_RenderDrawLine(&mCam.renderer(), (int)camOffset.x, (int)camOffset.y, (int)camOffset2.x, (int)camOffset2.y);
	}
	void Box2DebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
	{
		SDL_SetRenderDrawColor(&mCam.renderer(),
			(Uint8)(255 * color.g),
			(Uint8)(255 * color.r),
			(Uint8)(255 * color.b),
			(Uint8)(255 * color.a));

		auto d_a = (3.14f * 2) / 20.f;
		auto angle = d_a;

		const auto camOffset = b2Vec2{
			center.x - mCam.local().x,
			center.y - mCam.local().y
		};

		b2Vec2 start, end;
		end.x = radius;
		end.y = 0.0f;
		end = end + camOffset;

		// Circumfrance
		for (auto i = 0; i != 20; i++)
		{
			start = end;
			end.x = cos(angle) * radius;
			end.y = sin(angle) * radius;
			end = end + camOffset;
			angle += d_a;
			SDL_RenderDrawLine(&mCam.renderer(), (int)start.x, (int)start.y, (int)end.x, (int)end.y);
		}
	}
	void Box2DebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2&, const b2Color& color)
	{
		SDL_SetRenderDrawColor(&mCam.renderer(),
			(Uint8)(255 * color.g),
			(Uint8)(255 * color.r),
			(Uint8)(255 * color.b),
			(Uint8)(255 * color.a));

		auto d_a = (3.14f * 2) / 20.f;
		auto angle = d_a;

		const auto camOffset = b2Vec2{
			center.x - mCam.local().x,
			center.y - mCam.local().y
		};

		b2Vec2 start, end;
		end.x = radius;
		end.y = 0.0f;
		end = end + camOffset;

		// Circumfrance
		for (auto i = 0; i != 20; i++)
		{
			start = end;
			end.x = cos(angle) * radius;
			end.y = sin(angle) * radius;
			end = end + camOffset;
			angle += d_a;
			SDL_RenderDrawLine(&mCam.renderer(), (int)start.x, (int)start.y, (int)end.x, (int)end.y);

			// Fan inside
			if (i == 0)
			{
				start.x = camOffset.x;
				start.y = camOffset.y;
				SDL_RenderDrawLine(&mCam.renderer(), (int)start.x, (int)start.y, (int)end.x, (int)end.y);
			}
		}
	}
	void Box2DebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
	{
		SDL_SetRenderDrawColor(&mCam.renderer(),
			(Uint8)(255 * color.g),
			(Uint8)(255 * color.r),
			(Uint8)(255 * color.b),
			(Uint8)(255 * color.a));

		const auto camOffset = b2Vec2{
			p1.x - mCam.local().x,
			p1.y - mCam.local().y
		};

		const auto camOffset2 = b2Vec2{
			p2.x - mCam.local().x,
			p2.y - mCam.local().y
		};

		SDL_RenderDrawLine(&mCam.renderer(),
			(int)camOffset.x, (int)camOffset.y,
			(int)camOffset2.x, (int)camOffset2.y);
	}
	void Box2DebugDraw::DrawTransform(const b2Transform&)
	{
		SDL_SetRenderDrawColor(&mCam.renderer(),
			(Uint8)(255 * 1),
			(Uint8)(255 * 1),
			(Uint8)(255 * 1),
			(Uint8)(255 * 1));
	}
	void Box2DebugDraw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
	{
		SDL_SetRenderDrawColor(&mCam.renderer(),
			(Uint8)(255 * color.g),
			(Uint8)(255 * color.r),
			(Uint8)(255 * color.b),
			(Uint8)(255 * color.a));

		const auto camOffset2 = b2Vec2{
			p.x - mCam.local().x,
			p.y - mCam.local().y
		};

		DrawCircle(camOffset2, size, color);
	}
}
