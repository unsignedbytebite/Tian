﻿#include "source\Engine.h"

int main(int, char *[])
{
#ifdef DEV_MODE
	// Run the texture baker
	system("scripts\\bake.bat");
#endif

	DemoZelda::Engine engine;
	try
	{
		engine.run();
	}
	catch (Error& e)
	{
		e.print();
		Log::pause();
	}
	return 0;
} 

//TODO:
// Canvas with rendering tile gfxs'
// Attack boxes etc
// trigger events + pushables trigger events
// Conversation system = timed events: goto, say, look at
// animation on background
// 