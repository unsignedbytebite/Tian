@echo off
echo [ TianPacker ]
TianPacker.exe -p scripts\engine.structure scripts\engine.values assets\engine
TianPacker.exe -p scripts\sprite1.structure scripts\sprite1.values assets\sprite1.gfx
TianPacker.exe -p scripts\sprite2.structure scripts\sprite2.values assets\sprite2.gfx
TianPacker.exe -p scripts\tile.structure scripts\tile.values assets\tile.gfx
echo [ Bake Complete! ]

