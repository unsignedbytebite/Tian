#include "Engine.h"
#include "ScreenGame.h"

namespace DemoZelda
{
	// Game params
	//ui Engine::gScore;

	void Engine::loadAssets()
	{
		loadAsset(new Font{ GFXFont, 10 });
		loadAsset(new TextureAnimated{ GFXSprite1, mEngine.getRenderer() });
		loadAsset(new TextureAnimated{ GFXSprite2, mEngine.getRenderer() });
	}
	void Engine::registerScreens()
	{
		addScreen<ScreenGame>();	
	}
	void Engine::loadParams()
	{
		// Load engine properties
		try
		{
			FileIO file{ "assets\\engine" };
			file.start();
			mWindowWidth = file.read<us>();
			mWindowHeight = file.read<us>();
			mName = file.read<str>();

			file.end();
		}
		catch (Error& e)
		{
			e.print();
		}
	}
}
