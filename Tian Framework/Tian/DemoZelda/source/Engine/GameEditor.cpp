#include "GameEditor.h"
#include "../Engine.h"

namespace DemoZelda
{
	GameEditor::GameEditor(Input& input, Camera2D& camera, World& world, AssetManager& assetManager) :
		Editor(input, camera, world), mAssets(assetManager), mGameWorld(static_cast<GameWorld&>(mWorld))
	{
		// Draw options
		mSelector.add("Draw Walls");
		mSelector.add("Draw Triggers");
		mSelector.add("Add Player");
		mSelector.add("Paint Tiles");
		mSelector.add("Load GFX");

		loadGFX("assets\\tile.gfx");
		mGFXIndex = -1;
		mSnap = 16;
	}
	GameEditor::~GameEditor()
	{}
	void GameEditor::loadGFX(const str path)
	{
		try
		{
			auto* gfx = mAssets.add<TextureAnimated>(new TextureAnimated{ path, &mCam.renderer() });
			mTextures.push_back(gfx);
			mGFXIndex = (short)mTextures.size() - 1;
			Log::p(gfx->getPath() + " loaded", ConBox::CORRECT);
		}
		catch (Error& e)
		{
			e.print();
		}
	}
	void GameEditor::eventReturn()
	{
		// Convert to vec2
		Vector2Points vecs;
		for (auto& point : mPlacePoints)
		{
			vecs.push_back({ (float)point.x, (float)point.y });
		}

		if (mSelector.get() == "Draw Walls")
		{
			if (mPlacePoints.size() > 2)
			{
				Log::p("Wall created");
				mObjects.push_back(&mWorld.createStatic(WALL, 0, 0, vecs));
				mMouseEventMode = POINTS;
			}
			else
			{
				ConBox::Log::error("Need at least 3 points");
			}
		}
		else if (mSelector.get() == "Draw Triggers")
		{
			if (mPlacePoints.size() > 2)
			{
				Log::p("Wall created");
				mObjects.push_back(&mWorld.createStatic(TRIGGER, 0, 0, vecs));
			}
			else
			{
				Log::error("Need at least 3 points");
			}
		}
		else if (mSelector.get() == "Add Player")
		{
			if (mPlacePoints.size() >= 1)
			{
				Log::p("Player Added");
				mPlayer = mPlacePoints[0];
			}
			else
			{
				Log::error("Need at least 3 points");
			}
		}
		else if (mSelector.get() == "Load GFX")
		{
			Log::p("File name: ", ConBox::Colours::BRIGHT, true);
			const auto fileName = ConBox::Log::read<str>();
			loadGFX(fileName);
		}
		Editor::eventReturn();
	}
	void GameEditor::drawObjects()
	{
		Editor::drawObjects();

		// Draw the player spawn
		mCam.drawPrimitive(mPlayer, Colours::CornflowerBlue);

		// Draw the held gfx
		if (mGFXIndex != -1)
		{
			const Vector2 loco = { (float)mMouse.x - mCam.local().x, (float)mMouse.y - mCam.local().y };
			mTextures[mGFXIndex]->render(&mCam.renderer(), loco);
		}
	}
	void GameEditor::saveEvent(FileIO & file)
	{
		Editor::saveEvent(file);

		// Player spawn point
		file.write<us>((us)mPlayer.x);
		file.write<us>((us)mPlayer.y);

		// Write out the paint commands
		auto paintCommanSize = (us)mPaintCmds.size();
		file.write<us>(paintCommanSize);
		for (us i = 0; i < paintCommanSize; ++i)
		{
			file.write<str>(mPaintCmds[i].sheet);
			file.write<us>(mPaintCmds[i].set);
			file.write<us>(mPaintCmds[i].frame);
			file.write<float>(mPaintCmds[i].paintLoco.x);
			file.write<float>(mPaintCmds[i].paintLoco.y);
		}
	}
	void GameEditor::loadEvent(FileIO & file)
	{
		Editor::loadEvent(file);

		// Player spawn point
		mPlayer.x = file.read<us>();
		mPlayer.y = file.read<us>();

		// Read in paitn commands
		auto paintCommanSize = file.read<us>();
		for (us i = 0; i < paintCommanSize; ++i)
		{
			mPaintCmds[i].sheet = file.read<str>();
			mPaintCmds[i].set = file.read<us>();
			mPaintCmds[i].frame = file.read<us>();
			mPaintCmds[i].paintLoco.x = file.read<float>();
			mPaintCmds[i].paintLoco.y = file.read<float>();
		}

		// Redraw all the paint commands
		for (auto& cmd : mPaintCmds)
		{
			auto& texture = *mAssets.get<TextureAnimated>(cmd.sheet);
			texture.setCurrentFrameSet(cmd.set);
			texture.currentFrameSet().setFrameHead(cmd.frame);
			mGameWorld.drawToCanvas(texture, cmd.paintLoco);
		}
	}
	void GameEditor::eventMouse()
	{
		Editor::eventMouse();

		if (mSelector.get() == "Paint Tiles")
		{
			auto* texture = mTextures[mGFXIndex];
			Vector2 loco = { (float)mMouse.x, (float)mMouse.y };
			mGameWorld.drawToCanvas(*texture, loco);

			// Save the paint command
			{
				PiantCommand paintCmd;
				paintCmd.sheet = texture->getPath();
				paintCmd.paintLoco = loco;
				paintCmd.set = texture->getCurrentFrameSet();
				paintCmd.frame = texture->getFrameSets().at(paintCmd.set).getFrameHead();
				mPaintCmds.push_back(paintCmd);
			}
		}
	}
	void GameEditor::eventBracketRight()
	{
		Editor::eventBracketRight();

		if (mSelector.get() == "Draw Walls" || mSelector.get() == "Draw Triggers")
		{
			mMouseEventMode = POINTS;
		}

		if (mSelector.get() == "Paint Tiles")
		{
			mMouseEventMode = POINT;
			mGFXIndex = (short)mTextures.size() - 1;
			if (mTextures.size() == 0)
			{
				Log::error("No GFX loaded");
			}
		}
		else
		{
			mGFXIndex = -1;
		}
	}
	void GameEditor::eventBackspace()
	{
		if (mSelector.get() == "Paint Tiles")
		{
			// Pop off the last paint command
			mPaintCmds.pop_back();

			// Clear the canvas
			mGameWorld.clearCanvas();

			// Redraw all the paint commands
			for (auto& cmd : mPaintCmds)
			{
				auto& texture = *mAssets.get<TextureAnimated>(cmd.sheet);
				texture.setCurrentFrameSet(cmd.set);
				texture.currentFrameSet().setFrameHead(cmd.frame);
				mGameWorld.drawToCanvas(texture, cmd.paintLoco);
			}
		}
		else
		{
			Editor::eventBackspace();
		}
	}
	void GameEditor::eventBracketLeft()
	{
		Editor::eventBracketLeft();

		if (mSelector.get() == "Draw Walls" || mSelector.get() == "Draw Triggers")
		{
			mMouseEventMode = POINTS;
		}

		if (mSelector.get() == "Paint Tiles")
		{
			mMouseEventMode = POINT;
			mGFXIndex = (short)mTextures.size() - 1;
			if (mTextures.size() == 0)
			{
				Log::error("No GFX loaded");
			}
		}
		else
		{
			mGFXIndex = -1;
		}
	}
	void GameEditor::eventChevronRight()
	{
		if (mSelector.get() == "Paint Tiles")
		{
			const int index = (mSheetSet >= (ui)(mTextures[mGFXIndex]->getFrameSets().size() - 1)) ?
				mTextures[mGFXIndex]->getFrameSets().size() - 1 : mSheetSet + 1;
			
			mTextures[mGFXIndex]->setCurrentFrameSet((us)index);
		}
	}
	void GameEditor::eventChevronLeft()
	{
		if (mSelector.get() == "Paint Tiles")
		{
			const auto index = (mSheetSet > 0) ? mSheetSet - 1 : 0;
			mTextures[mGFXIndex]->setCurrentFrameSet((us)index);
		}
	}
}
