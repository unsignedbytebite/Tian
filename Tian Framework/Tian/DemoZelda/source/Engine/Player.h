#pragma once
#include "../Engine.h"
#include "GameActor.h"

namespace DemoZelda
{
	class Player : public GameActor
	{
	public:
		Player();
		~Player();

		// Set the input controller for the player
		void setInputController(Input* input);

		virtual void update() override;

		virtual void created() override;
	protected:
		// Input reference
		Input* mInput;
	};
}

