#pragma once
#include "../Engine.h"
#include "Player.h"
#include "Pushable.h"

namespace DemoZelda
{
	class GameWorld :
		public World2D
	{
	public:
		GameWorld(Camera2D& camera);

		void create(Input* inputController);
		void serializeIn(FileIO& file, Input* inputController);
		void serializeOut(FileIO & file);
	protected:
		//vector<Pushable*> mPushables;
		//vector<Object2D*> mWalls;
		//Player* mPlayer;
	};
}

