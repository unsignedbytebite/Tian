#include "Player.h"
#include "../Engine.h"

namespace DemoZelda
{
	Player::Player() : GameActor(GameTypes::PLAYER, Engine::get->getAsset<TextureAnimated>(DemoZelda::GFXSprite1))
	{}
	Player::~Player()
	{}
	void Player::setInputController(Input* input)
	{
		mInput = input;
	}
	void Player::update()
	{
		GameActor::update();
		GameActor::processContacts();

		if (mInput->isKeydown('a'))
		{
			setDirection(1);
			move({ -1, 0 }, 1);
		}
		else if (mInput->isKeydown('d'))
		{
			setDirection(3);
			move({ 1, 0 }, 3);
		}

		if (mInput->isKeydown('s'))
		{
			setDirection(0);
			move({ 0, 1 }, 0);
		}
		else if (mInput->isKeydown('w'))
		{
			setDirection(2);
			move({ 0, -1 }, 2);
		}

		if (mInput->isKeytap(SDLK_SPACE))
		{
			GameActor::attack();
		}
	}
	void Player::created()
	{
		// Set physics
		body()->SetType(b2_dynamicBody);
		body()->SetLinearDamping(20);
		body()->SetAngularDamping(20);

		b2CircleShape ciricle;
		ciricle.m_radius = 1;
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &ciricle;
		fixtureDef.density = 0.1f;
		fixtureDef.restitution = 0.5f;
		fixtureDef.friction = 2.01f;
		body()->CreateFixture(&fixtureDef);
	}
}
