#include "GameWorld.h"
#include "../Engine.h"

namespace DemoZelda
{
	GameWorld::GameWorld(Camera2D& camera) : World2D(camera, 8.f)
	{}
	void GameWorld::create(Input* inputController)
	{
		try
		{
			FileIO file("assets\\map0");
			file.start();
			serializeIn(file, inputController);
			file.end();
		}
		catch (Error& e)
		{
			e.print();

			auto* mPlayer = new Player();
			mPlayer->setInputController(inputController);
			mPlayer->setCamera(&mCam);
			&createObject(mPlayer);
		}

		mCanvas = std::unique_ptr<Canvas>(new Canvas{ &mCam, 256, 256, 2, 2 });
		clearCanvas();

		//{
		//	auto& push = createObject<Pushable>(new Pushable());
		//	push.setBodyLocation(7, 7);
		//	push.setCamera(&cam);
		//	mPushables.push_back(&push);
		//}
	}

	void GameWorld::serializeIn(FileIO& file, Input* inputController)
	{
		// Number of walls
		const auto wallsCount = file.read<us>();

		for (auto i = 0; i < wallsCount; ++i)
		{
			const auto wallShapeSize = file.read<uc>();
			Vector2Points shape;

			for (auto c = 0; c < wallShapeSize; ++c)
			{
				const auto x = file.read<float>();
				const auto y = file.read<float>();
				shape.push_back({ x, y });
			}

			//mWalls.push_back(&createStatic(WALL, 0, 0, shape);
			&createStatic(WALL, 0, 0, shape);
		}

		// Get the player spawn point
		const auto x = file.read<us>();
		const auto y = file.read<us>();
		auto& mPlayer = dynamic_cast<Player&>(createObject(new Player{}));
		mPlayer.setInputController(inputController);
		mPlayer.setCamera(&mCam);
		mPlayer.setBodyLocation(x, y);
	}

	void GameWorld::serializeOut(FileIO&)
	{
		//TODO: redfine serlize out
		// Number of walls
		//const auto wallsCount = mWalls.size();
		//file.write<us>((us)wallsCount);
		//
		//for (us i = 0; i < wallsCount; ++i)
		//{
		//	const auto& shape = *static_cast<b2PolygonShape*>(mWalls[i]->getBody().GetFixtureList()-//>GetShape());
		//
		//	const auto wallShapeSize = shape.GetVertexCount();
		//	file.write<uc>((uc)wallShapeSize);
		//
		//	for (auto c = 0; c < wallShapeSize; ++c)
		//	{
		//		const auto x = shape.GetVertex(c).x;
		//		const auto y = shape.GetVertex(c).y;
		//		file.write<float>(x);
		//		file.write<float>(y);
		//	}
		//}
	}
}
