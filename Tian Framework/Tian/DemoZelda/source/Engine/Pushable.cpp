#include "Pushable.h"
#include "../Engine.h"

namespace DemoZelda
{
	Pushable::Pushable() : GameActor(GameTypes::PUSHABLE, Engine::get->getAsset<TextureAnimated>(DemoZelda::GFXSprite2))
	{}
	Pushable::~Pushable()
	{}
	void Pushable::update()
	{	
		//Actor::update();
	}
	void Pushable::created()
	{
		body()->SetType(b2_dynamicBody);
		body()->SetLinearDamping(8.8f);
		body()->SetAngularDamping(8.8f);

		b2PolygonShape shape;
		shape.SetAsBox(1, 1);
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.8f;
		fixtureDef.friction = 0.3f;
		body()->CreateFixture(&fixtureDef);
	}
	void Pushable::attack()
	{}
}
