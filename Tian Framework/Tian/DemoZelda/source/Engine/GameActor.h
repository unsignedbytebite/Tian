#pragma once
#include "../Engine.h"

namespace DemoZelda
{
	class GameActor :
		public Actor
	{
	public:
		GameActor(const us id, const TextureAnimated sprite);
		~GameActor();

		virtual void attack();
		virtual void move(const Vector2& movementVector, const us animationSet);
		virtual void update() override;

		//virtual void contactEvent(const us index, Object2D& object) override;

		virtual void contactWithIce(Object2D& contactObject);
		virtual void contactWithTrigger(Object2D& contactObject);
		virtual void contactWithPushable(Object2D& contactObject);
		virtual void contactWithUnkown(Object2D& contactObject);
	protected:
		float mVelMod { 1.f };
		float mVel{ 1.f };
		bool isStriking{ false };
		Counter<uc> mStrikeTime;
	};
}

