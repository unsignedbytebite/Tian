#include "GameActor.h"
#include "../Engine.h"

namespace DemoZelda
{
	GameActor::GameActor(const us id, const TextureAnimated sprite) : Actor(id, sprite), mStrikeTime{ 10, 0 }
	{}
	GameActor::~GameActor()
	{}
	void GameActor::attack()
	{
#ifdef DEV_MODE
		ConBox::Log::p("Strike!");
		isStriking = true;
#endif
	}
	void GameActor::move(const Vector2& movementVector, const us animationSet)
	{
		if (isStriking)
			return;

		// Set animation state of the sprite to on
		setAnimationPaused(false);
		setCurrentFrameSet(animationSet);

		// Awake the body
		mBody->SetAwake(true);

		// Apply movment
		const auto dir = b2Vec2{ movementVector.x * mVelMod, movementVector.y * mVelMod };
		mBody->ApplyLinearImpulseToCenter(dir, false);
	}
	void GameActor::update()
	{
		if (isStriking)
		{
			if (mStrikeTime.count())
			{
				setCurrentFrameSet((us)getDirection() + 4);
				mVelMod = 0.2f;
			}
			else
			{
				mStrikeTime.setCount(0);
				isStriking = false;
				setCurrentFrameSet((us)getDirection());
				mVelMod = 1.0f;
			}
		}

		// Reset all movement based values before movement logic
		setAnimationPaused(true);
		getBody().SetLinearDamping(20);
		mVelMod = mVel;
	}
	//void GameActor::contactEvent(const us index, Object2D& object)
	//{
	//	switch (index)
	//	{
	//	case World::ObjectTypes::TRIGGER:
	//		contactWithTrigger(object);
	//		break;
	//	case DemoZelda::GameTypes::ICE:
	//		contactWithIce(object);
	//		break;
	//	case DemoZelda::GameTypes::PUSHABLE:
	//		contactWithPushable(object);
	//		break;
	//	default:
	//		contactWithUnkown(object);
	//	}
	//}
	void GameActor::contactWithIce(Object2D&)
	{
		// Apply ice effects
		getBody().SetLinearDamping(2);
		mVelMod = 0.2f;
	}
	void GameActor::contactWithTrigger(Object2D&)
	{}
	void GameActor::contactWithPushable(Object2D&)
	{}
	void GameActor::contactWithUnkown(Object2D&)
	{}
}
