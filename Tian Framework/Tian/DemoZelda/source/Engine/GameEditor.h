#pragma once
#include "../Engine.h"
#include "GameWorld.h"

namespace DemoZelda
{
	struct PiantCommand
	{
		str sheet;
		Vector2 paintLoco;
		us set;
		us frame;
	};

	class GameEditor : public Editor
	{
	public:
		GameEditor(Input& input, Camera2D& camera, World& world, AssetManager& assetManager);
		~GameEditor();

	private:
		// Load gfx
		void loadGFX(const str path);

		// Draw the objects layer
		virtual void drawObjects() override;

		// Save event
		virtual void saveEvent(FileIO& file);

		// Load data for a world
		virtual void loadEvent(FileIO& file);

		// Sub selction event
		virtual void eventMouse() override;
		virtual void eventReturn() override;
		virtual void eventChevronRight() override;
		virtual void eventChevronLeft() override;
		virtual void eventBracketLeft() override;
		virtual void eventBracketRight() override;
		virtual void eventBackspace() override;


		// The player location
		SDL_Point mPlayer;

		// The handles to loaded sheets
		vector<TextureAnimated*> mTextures;

		// The handles to loaded sheets
		vector<PiantCommand> mPaintCmds;

		// The current gfx in the hand
		short mGFXIndex{ -1 };

		// Handle to the asset manager
		AssetManager& mAssets;

		// Handle to the game world
		GameWorld& mGameWorld;

		// The current sheet set index
		ui mSheetSet{ 0 };
	};
}

