#pragma once
#include "../Engine.h"
#include "GameActor.h"

namespace DemoZelda
{
	class Pushable : public GameActor
	{
	public:
		Pushable();
		~Pushable();

		virtual void update() override;

		virtual void created() override;
	protected:
		// The actor the Pushable is in control of
		Actor* mActor;

		// Inherited via Actor
		virtual void attack() override;
	};
}

