#pragma once
#include "../../Tian/source/Tian.h"

#define DEV_MODE

using namespace Tian;

namespace DemoZelda
{
	enum GameTypes
	{
		NONE = 0,
		WALL = 2,
		PLAYER = 4,
		PUSHABLE = 8,
		ICE = 16,
		TRIGGER = 32
	};

	static const char* GFXFont = "assets\\vga850.fon";
	static const char* GFXSprite1 = "assets\\sprite1.gfx";
	static const char* GFXSprite2 = "assets\\sprite2.gfx";

	class Engine : public Loader
	{
	public:
		Engine() = default;

		// Inherited via Loader
		void registerScreens() override;
		void loadAssets() override;
		void loadParams() override;

		// Game params
		//static ui gScore;	
	};
}

