#include "ScreenGame.h"
#include "Engine.h"

namespace DemoZelda
{
	ScreenGame::ScreenGame(ScreenManager* manager, AssetManager* assets) :
		Screen2D(manager, assets), mWorld{ camera() }
	{
		camera().setScale({ 4.f, 4.f });

#ifdef DEV_MODE
		const auto pixelsPerUnit = 16.f;
		auto& cam = createCamera(new Camera2D{ camera() });
		cam.setScale({ pixelsPerUnit, pixelsPerUnit });
#endif

		mEditor = unique_ptr<GameEditor>(new GameEditor(*getInput(), camera(0), mWorld, *assets));
	}

	ScreenGame::~ScreenGame()
	{}
	void ScreenGame::preload()
	{}
	void ScreenGame::load()
	{
		mWorld.create(getInput());

#ifdef DEV_MODE
		ConBox::Log::p("Camera controls: ijkl = move, uo = zoom, nm = opacity");
		mWorld.createDebugRenderer(new Box2DebugDraw(camera(1)));
#endif
	}
	void ScreenGame::update()
	{
		mWorld.update();
		Screen2D::update();

#ifdef DEV_MODE
		auto camVel = Vector2{ 0, 0 };
		if (getInput()->isKeydown('i'))
		{
			camVel.y -= 1;
		}
		else if (getInput()->isKeydown('k'))
		{
			camVel.y += 1;
		}
		if (getInput()->isKeydown('j'))
		{
			camVel.x -= 1;
		}
		else if (getInput()->isKeydown('l'))
		{
			camVel.x += 1;
		}

		//TODO: sort out zooming for the placing items vs
		if (getInput()->isKeytap('u'))
		{
			camera(1).moveScale({ -1, -1 });
			camera(0).moveScale({ -1, -1 });
			ConBox::Log::p("Debug Cam zoom: " + camera(1).getScale().getString());
		}
		else if (getInput()->isKeytap('o'))
		{
			camera(1).moveScale({ 1, 1 });
			camera(0).moveScale({ 1, 1 });
			ConBox::Log::p("Debug Cam zoom: " + camera(1).getScale().getString());
		}

		if (getInput()->isKeydown('n'))
		{
			camera(1).moveColour(0, 0, 0, -1);
		}
		else if (getInput()->isKeydown('m'))
		{
			camera(1).moveColour(0, 0, 0, 1);
		}

		camera(1).local().move(camVel);
		camera(0).local().move(camVel);
#endif
		mEditor->update();
	}
	void ScreenGame::reset()
	{}
	void ScreenGame::render()
	{
		Screen2D::render();

		camera().expose();
		mWorld.draw();
#ifdef DEV_MODE
		mEditor->draw();
#endif
		camera().present();

#ifdef DEV_MODE
		//camera(1).expose();
		//camera(1).present();
#endif
	}
}
