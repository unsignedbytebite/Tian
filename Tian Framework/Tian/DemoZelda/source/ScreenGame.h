#pragma once
#include "Engine.h"
#include "Engine/GameWorld.h"
#include "Engine/GameEditor.h"

namespace DemoZelda
{
	class ScreenGame : public Screen2D
	{
	public:
		ScreenGame(ScreenManager* manager, AssetManager* assets = NULL);
		~ScreenGame();

		// Inherited via Screen2D
		virtual void preload() override;
		virtual void load() override;
		virtual void update() override;
		virtual void reset() override;
		virtual void render() override;

	private:
		unique_ptr<GameEditor> mEditor;
		GameWorld mWorld;
		Box2DebugDraw* mDebugDraw;
	};
}

