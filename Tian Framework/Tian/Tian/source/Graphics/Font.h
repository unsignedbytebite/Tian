///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Runtime/Asset.h"
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// A TTF font face to create SprintFont objects.
	/// </summary>
	class Font : public Asset
	{
	public:
		/// <summary>
		/// Create a font with path and text size.
		/// </summary>
		/// <param name="path">Path to font</param>
		/// <param name="size">Size of the text</param>
		Font(const str path, const us size = 16);

		/// <summary>
		/// Load a font from a file.
		/// </summary>
		/// <param name="path">Path to font</param>
		/// <param name="size">Size of the text</param>
		void loadFromFile(const str path, const us size);

		/// <summary>
		/// Get the loaded TTF font.
		/// </summary>
		/// <returns>The loaded font</returns>
		TTF_Font* get();
	protected:
		TTF_Font* mFont; ///< The SDL TTF font.
	};
}

