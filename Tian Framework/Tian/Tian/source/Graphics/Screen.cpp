#include "Screen.h"

namespace Tian
{
	Input* Screen::getInput()
	{
		return mEngine->getInput();
	}

	ControllerManager* Screen::getControllerManager()
	{
		return mEngine->getControllerManager();
	}

	const float Screen::getDelta()
	{
		return mEngine->getDelta();
	}

	const float Screen::getFPS()
	{
		return 1.f / mEngine->getDelta();
	}

	const str Screen::getFPSString()
	{
		return to_string((int)(1.f / mEngine->getDelta()));
	}
}
