#include "ScreenManager.h"

namespace Tian
{
	ScreenManager::ScreenManager(EngineGFX& engine) : mEngine(&engine)
	{}
	void ScreenManager::add(Screen* screen)
	{
		mScreens.push_back(shared_ptr<Screen>(screen));
	}
	void ScreenManager::switchScreen(const us index)
	{
		switchScreen(index, false);
	}
	void ScreenManager::switchScreen(const us index, const bool resetNext)
	{
		if (resetNext)
		{
			mScreens[index]->reset();
		}

		mScreens[index]->getEngine().execute(mScreens[index].get());
	}
	EngineGFX* ScreenManager::getEngine()
	{
		return mEngine;
	}
	void ScreenManager::close()
	{
		mEngine->setQuit(true);
	}
	void ScreenManager::remove(const us index)
	{
		mScreens.erase(mScreens.begin() + index);
	}
	void ScreenManager::remove(Screen* screen)
	{
		for (auto i = 0; i < (us)mScreens.size(); ++i)
		{
			if (mScreens[i].get() == screen)
			{
				mScreens.erase(mScreens.begin() + i);
				return;
			}
		}
	}
}
