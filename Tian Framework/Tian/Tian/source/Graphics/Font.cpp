#include "Font.h"

namespace Tian
{
	Font::Font(const str path, const us size)
	{
		loadFromFile(path, size);
	}

	void Font::loadFromFile(const str path, const us size)
	{
		mFont = TTF_OpenFont(path.c_str(), size);
		mPath = path;
	}

	TTF_Font* Font::get()
	{
		return mFont;
	}
}