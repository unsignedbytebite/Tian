///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Runtime/Asset.h"
#include "../Utils/Vector2.h"

namespace Tian
{
	/// <summary>
	/// Rendering bitmap loaded from a image file to be drawn in screen space.
	/// </summary>
	class Texture :
		public Asset
	{
	public:
		/// <summary>
		/// Default Texture constructor 
		/// </summary>
		Texture()
		{};

		/// <summary>
		/// A new texture that loads a file. Calls loadFromFile().
		/// </summary>
		/// <param name="path">File to the texture to load</param>
		/// <param name="renderer">The renderer to use</param>
		Texture(const str path, SDL_Renderer* renderer);
		~Texture();

		/// <summary>
		/// Load the texture from a file
		/// </summary>
		/// <param name="path">File to the texture to load</param>
		/// <param name="renderer">The renderer to use</param>
		virtual void loadFromFile(const str path, SDL_Renderer* renderer);

		/// <summary>
		/// Free the texture safely
		/// </summary>
		virtual void free();

		/// <summary>
		/// Render the texture.
		/// </summary>
		/// <param name="renderer">The renderer to draw the texture with</param>
		/// <param name="x">The x screen position</param>
		/// <param name="y">The y screen position</param>
		virtual void render(SDL_Renderer* renderer, const int x = 0, const int y = 0);

		/// <summary>
		/// Render the texture.
		/// </summary>
		/// <param name="renderer">The renderer to draw the texture with</param>
		/// <param name="clip">Draw the texture to the bounds of the this clip</param>
		virtual void render(SDL_Renderer* renderer, const SDL_Rect& clip);

		/// <summary>
		/// Render the texture.
		/// </summary>
		/// <param name="renderer">The renderer to draw the texture with</param>
		/// <param name="x">The x screen position</param>
		/// <param name="y">The y screen position</param>
		/// <param name="mask">A section of the texture to render</param>
		/// <param name="angle">The angle to draw the texture</param>
		/// <param name="pivot">The angle pivot point to rotate around</param>
		/// <param name="flip">Set the flip render state</param>
		virtual void render(SDL_Renderer* renderer, const int x, const int y, const SDL_Rect* mask, const float angle = 0.f, const Point& pivot = { 0, 0 }, const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);

		/// <summary>
		/// Render the texture.
		/// </summary>
		/// <param name="renderer">The renderer to draw the texture with</param>
		/// <param name="location">The screen location where to draw texture</param>
		virtual void render(SDL_Renderer* renderer, const Vector2& location);

		/// <summary>
		/// Get the width of texture.
		/// </summary>
		/// <returns>Texture width</returns>
		const int getWidth() const;

		/// <summary>
		/// Get the texture height.
		/// </summary>
		/// <returns>Texture height</returns>
		const int getHeight() const;

		/// <summary>
		/// Create and empty texture. Needed for render targets.
		/// </summary>
		/// <param name="width">Width of the texture</param>
		/// <param name="height">Height of the texture</param>
		/// <param name="renderer">The renderer to use</param>
		/// <param name="access">The texture access mode. Usually SDL_TEXTUREACCESS_TARGET</param>
		void create(int width, int height, SDL_Renderer* renderer, SDL_TextureAccess access = SDL_TextureAccess::SDL_TEXTUREACCESS_TARGET);

		/// <summary>
		/// Start the capture of the renderer on this texture for draw codes. When used as a render target.
		/// </summary>
		/// <param name="renderer">Renderer to use</param>
		void startTarget(SDL_Renderer* renderer);

		/// <summary>
		/// Stop the capture of the renderer on this texture for draw codes. When used as a render target.
		/// </summary>
		/// <param name="renderer">Renderer to use</param>
		void stopTarget(SDL_Renderer* renderer);

		/// <summary>
		/// Clears the texture with alpha.
		/// </summary>
		/// <param name="renderer">Renderer to use</param>
		void clear(SDL_Renderer* renderer);

		/// <summary>
		/// Set the colour mod of the texture.
		/// </summary>
		/// <param name="r">Red</param>
		/// <param name="g">Green</param>
		/// <param name="b">Blue</param>
		void setColour(const Uint8 r, const Uint8 g, const Uint8 b);

		/// <summary>
		/// Set the colour mod of the texture.
		/// </summary>
		/// <param name="colour">Colour</param>
		void setColour(const SDL_Color colour);

		/// <summary>
		/// Get the colour.
		/// </summary>
		/// <returns>Colour</returns>
		SDL_Color getColour() const;

		/// <summary>
		/// Move the colour by an amount.
		/// </summary>
		/// <param name="r">delta red</param>
		/// <param name="g">delta green</param>
		/// <param name="b">delta blue</param>
		/// <param name="a">delta alpha</param>
		void moveColour(const char r, const char g, const char b, const char a = 0);
	protected:
		int mWidth; ///< Texture width.
		int mHeight;  ///< Texture height.
		SDL_Color mColour; ///< The colour tint of the texture.
		SDL_Texture* mTex{ NULL }; ///< The SDL texture handle
	};
}

