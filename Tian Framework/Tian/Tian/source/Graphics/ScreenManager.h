///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Screen.h"
#include "EngineGFX.h"

namespace Tian
{
	class EngineGFX;
	class Screen;

	using Screens = std::vector<std::shared_ptr<Screen>>;

	/// <summary>
	/// ScreenManager looks after all the screens registered by the business code. 
	/// </summary>
	class ScreenManager
	{
	public:
		/// <summary>
		/// Create a new screen manager.
		/// </summary>
		/// <param name="engine">Handle to the engine</param>
		ScreenManager(EngineGFX& engine);

		/// <summary>
		/// Add a screen to the manager.
		/// </summary>
		/// <param name="screen">New screen to add</param>
		void add(Screen* screen);

		/// <summary>
		/// Remove a screen from the manager.
		/// </summary>
		/// <param name="screen">Screen to remove</param>
		void remove(Screen* screen);

		/// <summary>
		/// Remove a screen from the manager.
		/// </summary>
		/// <param name="index">Index of the screen to remove</param>
		void remove(const us index);

		/// <summary>
		/// Switch to another screen.
		/// </summary>
		/// <param name="index">Index of the screen to switch to</param>
		void switchScreen(const us index);

		/// <summary>
		/// Switch to another screen.
		/// </summary>
		/// <param name="index">Index of the screen to switch to</param>
		/// <param name="resetNext">If true reset will be called on the screen to be loaded</param>
		void switchScreen(const us index, const bool resetNext);

		/// <summary>
		/// Handle to the parent engine.
		/// </summary>
		/// <returns>Engine handle</returns>
		EngineGFX* getEngine();

		/// <summary>
		/// Tell the engine to close. The parent engine will be set to quit.
		/// </summary>
		void close();
	private:
		Screens mScreens; ///< Screens held by the manager.
		EngineGFX* mEngine; ///< Parent engine
	};
}
