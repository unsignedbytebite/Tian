///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	using Colour = SDL_Color;

	namespace Colours
	{
		const Colour TransparentBlack{ 0, 0, 0, 0 };
		const Colour TransparentWhite{ 255, 255, 255, 0 };
		const Colour AliceBlue{ 240, 248, 255, 255 };
		const Colour AntiqueWhite{ 250, 235, 215, 255 };
		const Colour Aqua{ 0, 255, 255, 255 };
		const Colour Aquamarine{ 127, 255, 212, 255 };
		const Colour Azure{ 240, 255, 255, 255 };
		const Colour Beige{ 245, 245, 220, 255 };
		const Colour Bisque{ 255, 228, 196, 255 };
		const Colour Black{ 0, 0, 0, 255 };
		const Colour BlanchedAlmond{ 255, 235, 205, 255 };
		const Colour Blue{ 0, 0, 255, 255 };
		const Colour BlueViolet{ 138, 43, 226, 255 };
		const Colour Brown{ 165, 42, 42, 255 };
		const Colour BurlyWood{ 222, 184, 135, 255 };
		const Colour CadetBlue{ 95, 158, 160, 255 };
		const Colour Chartreuse{ 127, 255, 0, 255 };
		const Colour Chocolate{ 210, 105, 30, 255 };
		const Colour Coral{ 255, 127, 80, 255 };
		const Colour CornflowerBlue{ 100, 149, 237, 255 };
		const Colour Cornsilk{ 255, 248, 220, 255 };
		const Colour Crimson{ 220, 20, 60, 255 };
		const Colour Cyan{ 0, 255, 255, 255 };
		const Colour DarkBlue{ 0, 0, 139, 255 };
		const Colour DarkCyan{ 0, 139, 139, 255 };
		const Colour DarkGoldenrod{ 184, 134, 11, 255 };
		const Colour DarkGray{ 169, 169, 169, 255 };
		const Colour DarkGreen{ 0, 100, 0, 255 };
		const Colour DarkKhaki{ 189, 183, 107, 255 };
		const Colour DarkMagenta{ 139, 0, 139, 255 };
		const Colour DarkOliveGreen{ 85, 107, 47, 255 };
		const Colour DarkOrange{ 255, 140, 0, 255 };
		const Colour DarkOrchid{ 153, 50, 204, 255 };
		const Colour DarkRed{ 139, 0, 0, 255 };
		const Colour DarkSalmon{ 233, 150, 122, 255 };
		const Colour DarkSeaGreen{ 143, 188, 139, 255 };
		const Colour DarkSlateBlue{ 72, 61, 139, 255 };
		const Colour DarkSlateGray{ 47, 79, 79, 255 };
		const Colour DarkTurquoise{ 0, 206, 209, 255 };
		const Colour DarkViolet{ 148, 0, 211, 255 };
		const Colour DeepPink{ 255, 20, 147, 255 };
		const Colour DeepSkyBlue{ 0, 191, 255, 255 };
		const Colour DimGray{ 105, 105, 105, 255 };
		const Colour DodgerBlue{ 30, 144, 255, 255 };
		const Colour Firebrick{ 178, 34, 34, 255 };
		const Colour FloralWhite{ 255, 250, 240, 255 };
		const Colour ForestGreen{ 34, 139, 34, 255 };
		const Colour Fuchsia{ 255, 0, 255, 255 };
		const Colour Gainsboro{ 220, 220, 220, 255 };
		const Colour GhostWhite{ 248, 248, 255, 255 };
		const Colour Gold{ 255, 215, 0, 255 };
		const Colour Goldenrod{ 218, 165, 32, 255 };
		const Colour Gray{ 128, 128, 128, 255 };
		const Colour Green{ 0, 128, 0, 255 };
		const Colour GreenYellow{ 173, 255, 47, 255 };
		const Colour Honeydew{ 240, 255, 240, 255 };
		const Colour HotPink{ 255, 105, 180, 255 };
		const Colour IndianRed{ 205, 92, 92, 255 };
		const Colour Indigo{ 75, 0, 130, 255 };
		const Colour Ivory{ 255, 255, 240, 255 };
		const Colour Khaki{ 240, 230, 140, 255 };
		const Colour Lavender{ 230, 230, 250, 255 };
		const Colour LavenderBlush{ 255, 240, 245, 255 };
		const Colour LawnGreen{ 124, 252, 0, 255 };
		const Colour LemonChiffon{ 255, 250, 205, 255 };
		const Colour LightBlue{ 173, 216, 230, 255 };
		const Colour LightCoral{ 240, 128, 128, 255 };
		const Colour LightCyan{ 224, 255, 255, 255 };
		const Colour LightGoldenrodYellow{ 250, 250, 210, 255 };
		const Colour LightGreen{ 144, 238, 144, 255 };
		const Colour LightGray{ 211, 211, 211, 255 };
		const Colour LightPink{ 255, 182, 193, 255 };
		const Colour LightSalmon{ 255, 160, 122, 255 };
		const Colour LightSeaGreen{ 32, 178, 170, 255 };
		const Colour LightSkyBlue{ 135, 206, 250, 255 };
		const Colour LightSlateGray{ 119, 136, 153, 255 };
		const Colour LightSteelBlue{ 176, 196, 222, 255 };
		const Colour LightYellow{ 255, 255, 224, 255 };
		const Colour Lime{ 0, 255, 0, 255 };
		const Colour LimeGreen{ 50, 205, 50, 255 };
		const Colour Linen{ 250, 240, 230, 255 };
		const Colour Magenta{ 255, 0, 255, 255 };
		const Colour Maroon{ 128, 0, 0, 255 };
		const Colour MediumAquamarine{ 102, 205, 170, 255 };
		const Colour MediumBlue{ 0, 0, 205, 255 };
		const Colour MediumOrchid{ 186, 85, 211, 255 };
		const Colour MediumPurple{ 147, 112, 219, 255 };
		const Colour MediumSeaGreen{ 60, 179, 113, 255 };
		const Colour MediumSlateBlue{ 123, 104, 238, 255 };
		const Colour MediumSpringGreen{ 0, 250, 154, 255 };
		const Colour MediumTurquoise{ 72, 209, 204, 255 };
		const Colour MediumVioletRed{ 199, 21, 133, 255 };
		const Colour MidnightBlue{ 25, 25, 112, 255 };
		const Colour MintCream{ 245, 255, 250, 255 };
		const Colour MistyRose{ 255, 228, 225, 255 };
		const Colour Moccasin{ 255, 228, 181, 255 };
		const Colour NavajoWhite{ 255, 222, 173, 255 };
		const Colour Navy{ 0, 0, 128, 255 };
		const Colour OldLace{ 253, 245, 230, 255 };
		const Colour Olive{ 128, 128, 0, 255 };
		const Colour OliveDrab{ 107, 142, 35, 255 };
		const Colour Orange{ 255, 165, 0, 255 };
		const Colour OrangeRed{ 255, 69, 0, 255 };
		const Colour Orchid{ 218, 112, 214, 255 };
		const Colour PaleGoldenrod{ 238, 232, 170, 255 };
		const Colour PaleGreen{ 152, 251, 152, 255 };
		const Colour PaleTurquoise{ 175, 238, 238, 255 };
		const Colour PaleVioletRed{ 219, 112, 147, 255 };
		const Colour PapayaWhip{ 255, 239, 213, 255 };
		const Colour PeachPuff{ 255, 218, 185, 255 };
		const Colour Peru{ 205, 133, 63, 255 };
		const Colour Pink{ 255, 192, 203, 255 };
		const Colour Plum{ 221, 160, 221, 255 };
		const Colour PowderBlue{ 176, 224, 230, 255 };
		const Colour Purple{ 128, 0, 128, 255 };
		const Colour Red{ 255, 0, 0, 255 };
		const Colour RosyBrown{ 188, 143, 143, 255 };
		const Colour RoyalBlue{ 65, 105, 225, 255 };
		const Colour SaddleBrown{ 139, 69, 19, 255 };
		const Colour Salmon{ 250, 128, 114, 255 };
		const Colour SandyBrown{ 244, 164, 96, 255 };
		const Colour SeaGreen{ 46, 139, 87, 255 };
		const Colour SeaShell{ 255, 245, 238, 255 };
		const Colour Sienna{ 160, 82, 45, 255 };
		const Colour Silver{ 192, 192, 192, 255 };
		const Colour SkyBlue{ 135, 206, 235, 255 };
		const Colour SlateBlue{ 106, 90, 205, 255 };
		const Colour SlateGray{ 112, 128, 144, 255 };
		const Colour Snow{ 255, 250, 250, 255 };
		const Colour SpringGreen{ 0, 255, 127, 255 };
		const Colour SteelBlue{ 70, 130, 180, 255 };
		const Colour Tan{ 210, 180, 140, 255 };
		const Colour Teal{ 0, 128, 128, 255 };
		const Colour Thistle{ 216, 191, 216, 255 };
		const Colour Tomato{ 255, 99, 71, 255 };
		const Colour Turquoise{ 64, 224, 208, 255 };
		const Colour Violet{ 238, 130, 238, 255 };
		const Colour Wheat{ 245, 222, 179, 255 };
		const Colour White{ 255, 255, 255, 255 };
		const Colour WhiteSmoke{ 245, 245, 245, 255 };
		const Colour Yellow{ 255, 255, 0, 255 };
		const Colour YellowGreen{ 154, 205, 50, 255 };
	}
}