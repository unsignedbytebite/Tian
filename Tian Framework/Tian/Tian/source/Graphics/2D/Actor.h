///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "Sprite.h"

namespace Tian
{
	/// <summary>
	/// An actor takes the sprite one step further to handle contact events between others Object2D. This class is ideally suited to be extended within business code.
	/// </summary>
	class Actor :
		public Sprite
	{
	public:
		/// <summary>
		/// Create a new actor with parameters.
		/// </summary>
		/// <param name="id">Object id</param>
		/// <param name="sprite">The animated texture to be used</param>
		Actor(const us id, const TextureAnimated sprite);

		/// <summary>
		/// Process all contacts on this actor.
		/// </summary>
		virtual void processContacts();

		/// <summary>
		/// Called when contact has been made. The object id can be used to process different logic in the business code.
		/// </summary>
		/// <param name="objA">Contact object A</param>
		/// <param name="objB">Contact object B</param>
		virtual void contactEvent(Object2D* objA, Object2D* objB);
	};
}

