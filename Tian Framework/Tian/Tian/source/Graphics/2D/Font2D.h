///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Font.h"
#include "../Texture.h"
#include "../../Common.h"

namespace Tian
{
	using StrUni = vector<Uint16>;

	/// <summary>
	/// Render text as a texture.
	/// </summary>
	class Font2D : 
		public Texture
	{
	public:
		/// <summary>
		/// Create a new instance with the font to use.
		/// </summary>
		/// <param name="font">Font to use at the typeface</param>
		Font2D(Font& font);

		/// <summary>
		/// Set the text.
		/// </summary>
		/// <param name="text">New text</param>
		void setText(const str text);

		/// <summary>
		/// Generates a sprite font for the texture.
		/// </summary>
		/// <param name="text">Text to render</param>
		/// <param name="renderer">Renderer to use</param>
		/// <param name="textColor">The colour of the text to generate</param>
		/// <param name="wrap_width">The width before text wrapping occurs</param> 
		void spriteFont(const str& text, SDL_Renderer* renderer, const SDL_Color& textColor, const us wrap_width = 0xffff);

		/// <summary>
		/// Generates a unicode sprite font for the texture.
		/// </summary>
		/// <param name="text">Unicode Text to render</param>
		/// <param name="renderer">Renderer to use</param>
		/// <param name="textColor">The colour of the text to generate</param>
		/// <param name="wrap_width">The width before text wrapping occurs</param> 
		void spriteFont(const StrUni& text, SDL_Renderer* renderer, const SDL_Color& textColor, const us wrap_width = 0xffff);

		/// <summary>
		/// Generates a sprite font for the texture.
		/// </summary>
		/// <param name="text">Text to render</param>
		/// <param name="renderer">Renderer to use</param>
		void spriteFont(const str& text, SDL_Renderer* renderer);

		/// <summary>
		/// Generates a unicode sprite font for the texture.
		/// </summary>
		/// <param name="text">Unicode Text to render</param>
		/// <param name="renderer">Renderer to use</param>
		void spriteFont(const StrUni& text, SDL_Renderer* renderer);
	protected:
		Font& mFont; ///< Referenced font handle.
		str mText; ///< The text string used for display.
		bool mFree; ///< A switch to flag the font is ready to be freed.
	};

	using Font2D_ = shared_ptr<Font2D>;
}

