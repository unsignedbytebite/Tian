#include "Sprite.h"

namespace Tian
{
	Sprite::Sprite(const TextureAnimated texture, const us id) :
		ObjectGFX2D(id), mTex{ texture }, mAngle{ 0.f }, mPivot{ Point{ 0, 0 } }, mFlip{ SDL_RendererFlip::SDL_FLIP_NONE },
		mColour{ Colours::White }
	{
		// Default bouding box is the texture dimensions
		const auto width = mTex.textureData()->getWidth();
		const auto height = mTex.textureData()->getHeight();
		addBounding({ 0, 0, width, height });
	}
	void Sprite::draw()
	{
		mTex.render(&mCam->renderer(), mLocal - mCam->getLocal(), mAngle, mPivot, mFlip);
	}
	void Sprite::update()
	{}
	void Sprite::setCurrentFrameSet(const us index)
	{
		mTex.setCurrentFrameSet(index);
	}
	void Sprite::setAnimationPaused(const bool pause)
	{
		mTex.setAnimationPaused(pause);
	}
	void Sprite::setFrameDelay(const us delay)
	{
		mTex.setFrameDelay(delay);
	}
	void Sprite::setAngle(const float angle)
	{
		mAngle = angle;
	}
	float Sprite::getAngle() const
	{
		return mAngle;
	}
	Point& Sprite::pivot()
	{
		return mPivot;
	}
	void Sprite::setFlip(const SDL_RendererFlip flip)
	{
		mFlip = flip;
	}
	SDL_RendererFlip Sprite::getFlip() const
	{
		return mFlip;
	}
	void Sprite::updateTextureColour()
	{
		mTex.textureData()->setColour(mColour);
	}
	void Sprite::drawInWorld(const float worldScale)
	{
		mBodyPos = Vector2{ mBody->GetPosition().x * worldScale, mBody->GetPosition().y * worldScale };
		mTex.render(&mCam->renderer(), mLocal - mCam->getLocal() + mBodyPos, mAngle, mPivot, mFlip);
	}
}
