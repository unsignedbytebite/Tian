///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "Font2D.h"
#include "ObjectGFX2D.h"

namespace Tian
{
	/// <summary>
	/// Used a Font2d  with the parent of a ObjectGFX2D. For use as drawing text as a sprite.
	/// </summary>
	class SpriteFont :
		public ObjectGFX2D
	{
	public:
		/// <summary>
		/// Create a new sprite font using a font handle.
		/// </summary>
		/// <param name="font">Font to use</param>
		SpriteFont(Font& font);

		/// <summary>
		/// Create a new sprite font using the asset manager and a index in the manager to the font.
		/// <param name="assets">The assets manager to use</param>
		/// <param name="assetIndex">The index of the font in the asset manager</param>
		SpriteFont(const AssetManager& assets, const us assetIndex);

		// Inherited via Object2D
		virtual void draw() override;

		/// <summary>
		/// Set the string to render.
		/// </summary>
		/// <param name="string">Text to render</param>
		/// <param name="location">Location to present the text</param>
		/// <param name="colour">The colour to render the text</param>
		void setString(const str string, const Vector2 location, const SDL_Color colour = Colours::White);

		/// <summary>
		/// Get the font texture handle.
		/// </summary>
		/// <returns>Font2D texture handle</returns>
		Font2D& font();
	protected:
		Font2D_ mFont; ///< Reference to the font asset
	};
}

