﻿#include "Font2D.h"

namespace Tian
{
	Font2D::Font2D(Font& font) : mFont(font), mFree(false)
	{}

	void Font2D::setText(const str text)
	{
		mText = text;
	}

	void Font2D::spriteFont(const str& text, SDL_Renderer* renderer, const SDL_Color& textColor, const us wrap_width)
	{
		// Check for new text
		if (mText == text)
		{
			return;
		}

		mText = text;

		//Get rid of preexisting texture
		if (mFree)
		{
			free();
		}

		//Render text surface
		SDL_Surface* textSurface = TTF_RenderText_Blended_Wrapped(mFont.get(), text.c_str(), textColor, wrap_width);
		if (textSurface == NULL)
		{
			throw Error("Unable to create text surface", __func__);
		}
		else
		{
			//Create texture from surface pixels
			mTex = SDL_CreateTextureFromSurface(renderer, textSurface);
			mFree = true;
			if (mTex == NULL)
			{
				throw Error("Unable to create font texture", __func__);
			}
			else
			{
				//Get image dimensions
				mWidth = textSurface->w;
				mHeight = textSurface->h;
			}

			//Get rid of old surface
			SDL_FreeSurface(textSurface);
		}
	}

	void Font2D::spriteFont(const StrUni& text, SDL_Renderer* renderer, const SDL_Color& textColor, const us wrap_width)
	{
		//Get rid of preexisting texture
		if (mFree)
		{
			free();
		}

		//Render text surface
		SDL_Surface* textSurface = TTF_RenderUNICODE_Blended_Wrapped(mFont.get(), &text[0], textColor, wrap_width);
		if (textSurface == NULL)
		{
			throw Error("Unable to create text surface", __func__);
		}
		else
		{
			//Create texture from surface pixels
			mTex = SDL_CreateTextureFromSurface(renderer, textSurface);
			mFree = true;
			if (mTex == NULL)
			{
				throw Error("Unable to create font texture", __func__);
			}
			else
			{
				//Get image dimensions
				mWidth = textSurface->w;
				mHeight = textSurface->h;
			}

			//Get rid of old surface
			SDL_FreeSurface(textSurface);
		}
	}
	void Font2D::spriteFont(const str& text, SDL_Renderer* renderer)
	{
		spriteFont(text, renderer, SDL_Color{ 255, 255, 255, 255 });
	}
	void Font2D::spriteFont(const StrUni& text, SDL_Renderer* renderer)
	{
		spriteFont(text, renderer, SDL_Color{ 255, 255, 255, 255 });
	}
}
