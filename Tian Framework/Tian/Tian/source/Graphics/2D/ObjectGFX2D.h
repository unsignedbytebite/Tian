///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../../Logic2D/Object2D.h"
#include "../../Utils/Tuple2.h"
#include "EngineGFX2D.h"
#include "../Colours.h"
#include "../../Utils/Boxes.h"
#include "../../Utils/Maths.h"
#include "Camera2D.h"

namespace Tian
{
	class Camera2D;

	/// <summary>
	/// This graphics object is for use with the Camera2D.
	/// </summary>
	class ObjectGFX2D :
		public Object2D
	{
	public:
		/// <summary>
		/// Create a new graphics object.
		/// </summary>
		/// <param name="id">Object id</param>
		/// <param name="cam">Camera pointer</param>
		ObjectGFX2D(const us id = 0xffff, Camera2D* cam = NULL) : Object2D(id), mCam(cam)
		{};
		~ObjectGFX2D()
		{};

		/// <summary>
		/// Draw the bounding boxes
		/// </summary>
		/// <param name="cam">Camera to render to</param>
		/// <param name="colour">Colour to render the box</param>
		virtual void drawBoundings(const Camera2D& cam, const SDL_Color colour = Colours::HotPink);

		/// <summary>
		/// Set the camera reference.
		/// </summary>
		/// <param name="camera">New camera to use</param>
		void setCamera(Camera2D* camera);

		/// <summary>
		/// Draw the object with a scale. For use with World. To be implemented.
		/// </summary>
		/// <param name="worldScale">Scale factor</param>
		virtual void drawInWorld(const float worldScale = 1.0f);

		// Inherited via Object
		virtual void update();
		virtual void draw();
	protected:
		Camera2D* mCam; ///< Reference to the object's camera.
	};
}
