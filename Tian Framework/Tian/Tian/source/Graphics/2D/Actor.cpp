#include "Actor.h"

namespace Tian
{
	Actor::Actor(const us id, const TextureAnimated sprite) : Sprite(sprite, id)
	{}
	void Actor::processContacts()
	{
		for (auto* ce = mBody->GetContactList(); ce; ce = ce->next)
		{
			//TODO: fix

			// Get get contacting object
			b2Contact& c = *ce->contact;

			b2Body* bodyA = c.GetFixtureA()->GetBody();
			auto* objA = (Object2D*)bodyA->GetUserData();

			b2Body* bodyB = c.GetFixtureB()->GetBody();
			auto* objB = (Object2D*)bodyB->GetUserData();

			contactEvent(objA, objB);
		}
	}
	void Actor::contactEvent(Object2D*, Object2D*)
	{}
}
