#include "Camera2D.h"

namespace Tian
{
	Camera2D::Camera2D(SDL_Renderer& renderer, const us frameWidth, const us frameHeight) :
		mRenderer(renderer)
	{
		// Create the render target texture
		mTex = shared_ptr<Texture>(new Texture());
		mTex->create(frameWidth, frameHeight, &renderer);

		// Bounds 0 is the screen dimensions of the app window
		mBounds.add({ mLocal, { 0, 0, frameWidth, frameHeight } });

		// Bounds 1 is the viewport which is the screen with camera scaling
		mBounds.add({ mLocal, { 0, 0, 0, 0 } });
		setScale(mScale);
	}
	void Camera2D::expose()
	{
		mTex->clear(&mRenderer);
		mTex->startTarget(&mRenderer);
	}
	void Camera2D::stopExpose()
	{
		mTex->stopTarget(&mRenderer);
	}
	void Camera2D::present()
	{
		mTex->stopTarget(&mRenderer);

		// Scale the texture
		const float h = mTex->getHeight() * mScale.y;
		const float w = mTex->getWidth() * mScale.x;
		const SDL_Rect clip{ 0, 0, (int)w, (int)h };
		mTex->render(&mRenderer, clip);
	}
	Vector2 Camera2D::getScale() const
	{
		return mScale;
	}
	void Camera2D::setScale(const float scale)
	{
		setScale({ scale, scale });
	}
	void Camera2D::setScale(const Vector2& scale)
	{
		mScale = scale;

		const SDL_Rect rect{
			(int)((float)mBounds.boxes().at(0).getRect().x / mScale.x),
			(int)((float)mBounds.boxes().at(0).getRect().y / mScale.y),
			(int)((float)mBounds.boxes().at(0).getRect().w / mScale.x),
			(int)((float)mBounds.boxes().at(0).getRect().h / mScale.y) };

		mBounds.boxes().at(1).setRect(rect);
	}
	void Camera2D::moveScale(const Vector2 scale)
	{
		mScale = mScale + scale;
		setScale(mScale);
	}
	Box& Camera2D::viewport()
	{
		return mBounds.boxes().at(1);
	}
	SDL_Renderer& Camera2D::renderer()
	{
		return mRenderer;
	}
	void Camera2D::moveColour(const char r, const char g, const char b, const char a)
	{
		mTex->moveColour(r, g, b, a);
	}
	void Camera2D::drawPrimitive(std::vector<Point>& points, const SDL_Color& colour) const
	{
		drawPrimitive(-(int)getLocal().getX(), -(int)getLocal().getY(), points, colour);
	}
	void Camera2D::drawPrimitive(Vector2Points& points, const SDL_Color& colour) const
	{
		drawPrimitive(-(int)getLocal().getX(), -(int)getLocal().getY(), points, colour);
	}
	void Camera2D::drawPrimitive(Point* points, const uc size, const SDL_Color& colour) const
	{
		drawPrimitive(-(int)getLocal().getX(), -(int)getLocal().getY(), points, size, colour);
	}
	void Camera2D::drawPrimitive(const Point& point, const SDL_Color& colour) const
	{
		drawPrimitive(-(int)getLocal().getX(), -(int)getLocal().getY(), point, colour);
	}
	void Camera2D::drawPrimitiveBox(const SDL_Rect & rect, const SDL_Color & colour) const
	{
		drawPrimitiveBox(-(int)getLocal().getX(), -(int)getLocal().getY(), rect, colour);
	}
	void Camera2D::drawPrimitiveBoxFill(const SDL_Rect & rect, const SDL_Color & colour) const
	{
		drawPrimitiveBoxFill(-(int)getLocal().getX(), -(int)getLocal().getY(), rect, colour);
	}
	void Camera2D::drawPrimitive(const int x, const int y, std::vector<Point>& points, const SDL_Color& colour) const
	{
		SDL_SetRenderDrawColor(&mRenderer, colour.r, colour.g, colour.b, colour.a);
		for (auto& point : points)
		{
			point.x -= -x;
			point.y -= -y;
		}

		SDL_RenderDrawLines(&mRenderer, &points[0], (int)points.size());
	}
	void Camera2D::drawPrimitive(const int x, const int y, Vector2Points& points, const SDL_Color& colour) const
	{
		SDL_SetRenderDrawColor(&mRenderer, colour.r, colour.g, colour.b, colour.a);
		std::vector<Point> pointsSDL;

		for (auto& point : points)
		{
			pointsSDL.push_back(Point{ (int)point.x + x, (int)point.y + y });
		}

		SDL_RenderDrawLines(&mRenderer, &pointsSDL[0], (int)points.size());
	}
	void Camera2D::drawPrimitive(const int x, const int y, Point* points, const uc size, const SDL_Color& colour) const
	{
		SDL_SetRenderDrawColor(&mRenderer, colour.r, colour.g, colour.b, colour.a);
		for (auto i = 0; i < size; i++)
		{
			points[i].x -= -x;
			points[i].y -= -y;
		}

		SDL_RenderDrawLines(&mRenderer, points, size);
	}
	void Camera2D::drawPrimitive(const int x, const int y, const Point& point, const SDL_Color& colour) const
	{
		SDL_SetRenderDrawColor(&mRenderer, colour.r, colour.g, colour.b, colour.a);
		SDL_RenderDrawPoint(&mRenderer, point.x + x, point.y + y);
	}
	void Camera2D::drawPrimitiveBox(const int, const int, const SDL_Rect& rect, const SDL_Color& colour) const
	{
		SDL_SetRenderDrawColor(&mRenderer, colour.r, colour.g, colour.b, colour.a);
		SDL_RenderDrawRect(&mRenderer, &rect);
	}
	void Camera2D::drawPrimitiveBoxFill(const int, const int, const SDL_Rect& rect, const SDL_Color& colour) const
	{
		SDL_SetRenderDrawColor(&mRenderer, colour.r, colour.g, colour.b, colour.a);
		SDL_RenderFillRect(&mRenderer, &rect);
	}
	void Camera2D::createBody(b2World& world, const float32 width, const float32 height)
	{
		b2BodyDef bd;
		bd.position.Set(.0f, .0f);
		bd.allowSleep = false;
		setBody(*world.CreateBody(&bd));
		body()->SetType(b2_dynamicBody);
		body()->SetActive(true);

		b2PolygonShape sd;
		sd.SetAsBox(width*.5f, height*.5f, { width*.5f, height*.5f }, 0);
		//d.filter.categoryBits = 0;
		//d.filter.groupIndex = 0;
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &sd;
		fixtureDef.density = .0f;
		fixtureDef.isSensor = true;
		body()->CreateFixture(&fixtureDef);
		body()->GetFixtureList()->SetSensor(true);

		body()->SetUserData(this);
	}
	void Camera2D::setLocalToBodyPosition(const float scale)
	{
		local().x = body()->GetPosition().x * scale;
		local().y = body()->GetPosition().y * scale;
	}
}
