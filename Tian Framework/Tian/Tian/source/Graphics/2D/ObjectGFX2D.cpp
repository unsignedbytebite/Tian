#include "ObjectGFX2D.h"

namespace Tian
{
	void ObjectGFX2D::drawBoundings(const Camera2D& cam, const SDL_Color colour)
	{
		for (Box& box : mBounds.boxes())
		{
			const SDL_Rect rect = SDL_Rect{
				box.getBounds().x - (int)cam.getLocal().getX(),
				box.getBounds().y - (int)cam.getLocal().getY(),
				box.getBounds().w,
				box.getBounds().h };

			cam.drawPrimitiveBox(rect, colour);
		}
	}
	void ObjectGFX2D::setCamera(Camera2D* newCamera)
	{
		mCam = newCamera;
	}
	void ObjectGFX2D::update()
	{}
	void ObjectGFX2D::draw()
	{}
	void ObjectGFX2D::drawInWorld(const float)
	{}
}
