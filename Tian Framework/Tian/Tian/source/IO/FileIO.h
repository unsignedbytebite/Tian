#pragma once
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// Common file IO using SDL.
	/// </summary>
	class FileIO
	{
	public:
		enum Mode { READ, WRITE }; ///< File operation mode.

		/// <summary>
		/// Create a new FileIO object path and mode parameters.
		/// </summary>
		/// <param name="filePath">File path</param>
		/// <param name="mode">Operation Mode</param>
		FileIO(const str filePath, const Mode mode = READ);
		~FileIO();

		/// <summary>
		/// Read from the file.
		/// </summary>
		/// <param name="filePath">File path</param>
		/// <param name="mode">Operation Mode</param>
		/// <returns>File contents</returns>
		static str read(const str filePath, const Mode mode = READ);

		/// <summary>
		/// Start the read process.
		/// </summary>
		void start();

		/// <summary>
		/// End the read process
		/// </summary>
		void end();

		/// <summary>
		/// Write a string to the file.
		/// </summary>
		/// <param name="strings">Text to write</param>
		void writeString(const str strings);

		/// <summary>
		/// Read a number of bytes from the file defined as type T.
		/// </summary>
		/// <returns>Data as type T</returns>
		template<class T>
		T read()
		{
			if (mFile == NULL)
			{
				throw Error("No file found: " + mPath, __func__);
			}

			// Clamp the read length to the file size
			if (mIndex >= mFile->size(mFile))
			{
				throw Error("Index overflow end of stream: " + to_string(mIndex), __func__);
			}
			else
			{
				mIndex += sizeof(T);
			}

			T r;
			SDL_RWread(mFile, &r, sizeof(T), 1);

			return r;
		}

		/// <summary>
		/// Read a string from the binary stream.
		/// </summary>
		/// <returns>Stream string</returns>
		template<>
		str read()
		{
			const us stringLength = read<us>();

			str s;
			for (us i = 0; i < stringLength; ++i)
			{
				s += read<uc>();
			}

			return s;
		}

		/// <summary>
		/// Write a number of bytes to the file that are equal to type T.
		/// </summary>
		/// <param name="data">Type data to write</param>
		template<class T>
		void write(const T data)
		{
			if (mFile == NULL)
			{
				throw Error("No file found: " + mPath, __func__);
			}

			mIndex += sizeof(T);

			SDL_RWwrite(mFile, &data, sizeof(T), 1);
		}

		/// <summary>
		/// Write a string to the file.
		/// </summary>
		/// <param name="data">String data</param>
		template<>
		void write(const str data)
		{
			write<us>((us)data.size());

			for (us i = 0; i < data.size(); ++i)
			{
				write<uc>(data[i]);
			}
		}
	protected:
		/// <summary>
		/// Get the current operation mode for SDL converted from the Mode enum.
		/// </summary>
		/// <param name="mode"> operation mode</param>
		/// <returns>SDL operation mode</returns>
		static char* getMode(const Mode mode);

		SDL_RWops* mFile; ///< SDL file handle.
		str mPath; ///< Path to the file.
		Sint64 mIndex; ///< Stream counter.
		Mode mMode; ///< Current operation mode.
	};
}

