#include "FileIO.h"

namespace Tian
{
	FileIO::FileIO(const str filePath, const Mode mode) : mPath(filePath), mFile{ NULL }, mIndex{ 0 }, mMode{ mode }
	{}

	FileIO::~FileIO()
	{
		end();
	}

	str FileIO::read(const str filePath, const Mode mode)
	{
		//Open file for reading in binary
		SDL_RWops* file = SDL_RWFromFile(filePath.c_str(), getMode(mode));

		if (file == NULL)
		{
			throw Error("No file found: " + filePath, __func__);
		}

		const Sint64 size = file->size(file);
		str line = "";
		unsigned char c;

		for (int i = 0; i < size; ++i)
		{
			SDL_RWread(file, &c, sizeof(char), 1);
			line += c;
		}

		SDL_RWclose(file);

		return line;
	}
	void FileIO::start()
	{
		//Open file for reading in binary
		mFile = SDL_RWFromFile(mPath.c_str(), getMode(mMode));

		if (mFile == NULL)
		{
			throw Error("No file found: " + mPath, __func__);
		}
	}
	void FileIO::end()
	{
		if (mFile != NULL)
		{
			SDL_RWclose(mFile);
			mFile = NULL;
		}
	}
	void FileIO::writeString(const str strings)
	{
		for (us i = 0; i < strings.size(); ++i)
		{
			write<uc>(strings[i]);
		}
	}
	char* FileIO::getMode(const Mode mode)
	{
		if (mode == WRITE)
		{
			return "w+b";
		}
		else if (mode == READ)
		{
			return "r+b";
		}

		return "null";
	}
}
