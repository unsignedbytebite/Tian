#include "Loader.h"
#include "../Utils/Maths.h"

namespace Tian
{
	Loader* Loader::get;
	Loader::Loader() : mEngine(EngineGFX2D{}), mWindowWidth(320), mWindowHeight(240), mName("Tian"), mScreenManager(mEngine)
	{
		Loader::get = this;
	}
	void Loader::run()
	{
		// Define random seed
		Maths::randomSeed();

		// Load any parameters
		loadParams();

		// Create window 
		mEngine.ini(mWindowWidth, mWindowHeight, mName);

		// Load shared assets
		loadAssets();

		// Register screens with the engine
		registerScreens();

		// Switch screen 0
		mScreenManager.switchScreen(0);
	}
	void Loader::loadAssets()
	{}
	void Loader::registerScreens()
	{}
	void Loader::loadParams()
	{}
	const us Loader::getWindowWidth() const
	{
		return mWindowWidth;
	}
	const us Loader::getWindowHeight() const
	{
		return mWindowHeight;
	}
}
