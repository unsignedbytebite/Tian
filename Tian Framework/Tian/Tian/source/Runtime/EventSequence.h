///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Utils/Timer.h"
#include "Event.h"

namespace Tian
{
	/// <summary>
	/// Use to trigger timed based events.
	/// </summary>
	class EventSequence
	{
	public:
		/// <summary>
		/// Default constructor.
		/// </summary>
		EventSequence();

		/// <summary>
		/// Start the sequence.
		/// </summary>
		virtual void start();

		/// <summary>
		/// Stop the sequence
		/// </summary>
		virtual void stop();

		/// <summary>
		/// Create a new event for the sequence cache.
		/// </summary>
		/// <param name="event">New event</param>
		virtual void createEvent(Event* event);

		/// <summary>
		/// Update the sequence
		/// </summary>
		virtual void update();

		/// <summary>
		/// Get if the sequence has complete.
		/// </summary>
		/// <returns>True of the sequence has completed</returns>
		virtual bool isFinished();
	protected:
		Timer mTimer; ///< The sequence timer
		std::vector<std::shared_ptr<Event>> mEvents; ///<  The event cache
		std::vector<Event*> mFutureEvents; ///<  The lookup list of all future events
	};
}

