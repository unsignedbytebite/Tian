///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	class Controller
	{
	public:
		Controller() = default;

		void flushHats();

		void setAxis(const SDL_JoyAxisEvent axisEvent);
		void setButton(const SDL_JoyButtonEvent buttonEvent, const bool isDown);
		void setHat(const SDL_JoyHatEvent hatEvent);

		Sint16 getAxis(const us axisIndex = 0);
		bool getHat(const us hatIndex = 0) const;
		bool getButton(const us buttonIndex = 0) const;
		str toString() const;
	private:
		const int mDeadZone{ 8000 };

		Sint16 mAxis[10]{ 0 };
		bool mHats[10]{ false };
		bool mButtons[32]{ false };
	};
}

