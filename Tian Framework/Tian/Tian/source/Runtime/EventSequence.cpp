#include "EventSequence.h"
#include <algorithm>

namespace Tian
{
	EventSequence::EventSequence()
	{}
	void EventSequence::start()
	{
		mTimer.splitTime();
	}
	void EventSequence::stop()
	{}
	void EventSequence::createEvent(Event* newEvent)
	{
		mEvents.push_back(std::shared_ptr<Event>(newEvent));
		mFutureEvents.push_back(newEvent);
	}
	void EventSequence::update()
	{
		const auto ms = mTimer.getMs();
		std::vector<Event*> mRemoveIndicies;

		// Check all future events for their logic
		for (auto& e : mFutureEvents)
		{
			if (ms > e->getEventEnd())
			{
				e->completed();
				mRemoveIndicies.push_back(e);
			}
			else if (ms > e->getEventStart())
			{
				e->update();
			}
		}

		// Remove completed future events
		for (auto& r : mRemoveIndicies)
		{
			mFutureEvents.erase(std::remove(mFutureEvents.begin(), mFutureEvents.end(), r), mFutureEvents.end());
		}
	}
	bool EventSequence::isFinished()
	{
		return mFutureEvents.size() == 0;
	}
}
