///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Controller.h"
#include <SDL.h>

namespace Tian
{
	class ControllerManager
	{
	public:
		ControllerManager(const us controllerCount);
		~ControllerManager();

		str toString() const;
		void free();
		void flushHats();
		void setAxis(const SDL_JoyAxisEvent axisEvent);
		void setButton(const SDL_JoyButtonEvent buttonEvent, const bool isDown);
		void setHat(const SDL_JoyHatEvent hatEvent);

		Sint16 getAxis(const us index = 0, const us axisIndex = 0) const;
		bool getHat(const us index = 0, const us hatIndex = 0) const;
		bool getButton(const us index = 0, const us buttonIndex = 0) const;
		Controller& getController(const us index);
		us getCount() const;
	protected:
		unique_ptr<vector<Controller>> mControllers;
		vector<SDL_Joystick*> mSDLSticks;
	};
}

