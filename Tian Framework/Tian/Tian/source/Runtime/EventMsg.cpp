#include "EventMsg.h"

namespace Tian
{
	EventMsg::EventMsg(const ui eventStart, const ui eventEnd, const str msg) :
		Event(eventStart, eventEnd), mMsg(msg)
	{}
	void EventMsg::update() const
	{}
	void EventMsg::completed() const
	{
		ConBox::Log::p(mMsg);
	}
}