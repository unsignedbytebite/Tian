///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Utils/Timer.h"

namespace Tian
{
	/// <summary>
	/// Abstract class for event types.
	/// </summary>
	class Event
	{
	public:
		/// <summary>
		/// Create an event with a start and end time in ms.
		/// </summary>
		/// <param name="eventStart">Start time ms</param>
		/// <param name="eventEnd">End time ms</param>
		Event(const ui eventStart, const ui eventEnd);

		/// <summary>
		/// Returns true if the event is in the current time window.
		/// </summary>
		/// <param name="timer">Timer to check against</param>
		/// <returns>True if the event is in the window</returns>
		virtual bool isInWindow(const Timer& timer);

		/// <summary>
		/// Get the MS time of the event start.
		/// </summary>
		/// <returns>Event start ms</returns>
		ui getEventStart() const;

		/// <summary>
		/// Get the MS time of the event end.
		/// </summary>
		/// <returns>Event end ms</returns>
		ui getEventEnd() const;

		/// <summary>
		/// Implement to update the event.
		/// </summary>
		virtual void update() const;

		/// <summary>
		/// Implement to called when the event has ended
		/// </summary>
		virtual void completed() const;
	protected:
		ui mEventStart; ///< The MS when this event is triggered
		ui mEventEnd; ///< The MS when this event is ended
	};
}

