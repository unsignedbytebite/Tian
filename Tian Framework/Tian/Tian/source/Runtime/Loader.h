///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Graphics/2D/EngineGFX2D.h"
#include "../Graphics/Font.h"
#include "../Graphics/TextureAnimated.h"

namespace Tian
{
	/// <summary>
	/// Loader is a helper class to create a EngineGFX2D engine, ScreenManager and AssetManager. It interfaces the common requirements for an engine and creating screens in the business code. Extending this class will make writing business coded engine easy.
	/// </summary>
	class Loader
	{
	public:
		static Loader* get;

		/// <summary>
		/// Default constructor creates the core aspects of engine ready for use.
		/// </summary>
		Loader();

		/// <summary>
		/// Starts the engine calls: loadParams(); 	loadAssets(); registerScreens();. Switched the current screen to 0 and initializes the engine and a random seed.
		/// </summary>
		virtual void run();

		/// <summary>
		/// Implement to load shared assets across screens.
		/// </summary>
		virtual void loadAssets();

		/// <summary>
		/// Implement to Register screens with the engine.
		/// </summary>
		virtual void registerScreens();

		/// <summary>
		/// Implement to load any parameters.
		/// </summary>
		virtual void loadParams();

		/// <summary>
		/// Load a font into the AssetManager.
		/// </summary>
		/// <param name="asset">New font asset</param>
		template<class T>
		void loadAsset(T* asset)
		{
			mSharedAssets.add<T>(asset);
		}

		/// <summary>
		/// Get an asset from the manager.
		/// </summary>
		/// <param name="name">Name of the asset</param>
		/// <returns>The handle to the asset</returns>
		template<class T>
		T& getAsset(const str name)
		{
			return *mSharedAssets.get<T>(name);
		}

		/// <summary>
		/// Load and create a screen and add it to the manager.
		/// </summary>
		template<class T>
		void addScreen()
		{
			mScreenManager.add(new T(&mScreenManager, &mSharedAssets));
		}

		/// <summary>
		/// Get the window width dimension.
		/// </summary>
		/// <returns>Window width</returns>
		virtual const us getWindowWidth() const;

		/// <summary>
		/// Get the window height dimension.
		/// </summary>
		/// <returns>Window height</returns>
		virtual const us getWindowHeight() const;
	protected:
		EngineGFX2D mEngine; ///< The engine used by this loader.
		us mWindowWidth; ///< Screen width.
		us mWindowHeight; ///< Screen height.
		str mName; ///< Name of the window.
		AssetManager mSharedAssets; ///< The shared asset manager.
		ScreenManager mScreenManager; ///< The shared screen manager.
	};
}

