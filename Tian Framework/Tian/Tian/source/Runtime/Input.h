///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// Keyboard and mouse input manager.
	/// </summary>
	class Input
	{
	public:
		/// <summary>
		/// Default constructor.
		/// </summary>
		Input();

		/// <summary>
		/// Set a key pressed state.
		/// </summary>
		/// <param name="key">Key to set</param>
		/// <param name="flag">True for on</param>
		void setKey(const SDL_Keycode key, const bool flag);

		/// <summary>
		/// Set a mouse button pressed state.
		/// </summary>
		/// <param name="key">Mouse button to set</param>
		/// <param name="flag">True for on</param>
		void setMouse(const Uint8 key, const bool flag);

		/// <summary>
		/// Syncs the mouse XY from the input device mouse state.
		/// </summary>
		void setMouseXY();

		/// <summary>
		/// Is a key down.
		/// </summary>
		/// <param name="code">Key</param>
		/// <returns>True for down</returns>
		bool isKeydown(const SDL_Keycode code);

		/// <summary>
		/// Is a key tapped. A key held down will only return true once when calling this.
		/// </summary>
		/// <param name="code">Key</param>
		/// <returns>True for down</returns>
		bool isKeytap(const SDL_Keycode code);

		/// <summary>
		/// Is a mouse button down.
		/// </summary>
		/// <param name="key">Key</param>
		/// <returns>True for down</returns>
		bool isMousedown(const Uint8 key);

		/// <summary>
		/// Is any mouse button down.
		/// </summary>
		/// <returns>True for any button down</returns>
		bool isMousedown();

		/// <summary>
		/// Is any mouse button tapped tap. A button held down will only return true once when calling this.
		/// </summary>
		/// <returns>True if tapped</returns>
		bool isMousetap();

		/// <summary>
		/// Is a mouse button tapped tap. A button held down will only return true once when calling this.
		/// </summary>
		/// <returns>True if tapped</returns>
		bool isMousetap(const Uint8 key);

		/// <summary>
		/// Get the mouse X position in screen pixels.
		/// </summary>
		/// <returns>Mouse X</returns>
		int getMouseX()
		{
			return mMouseX;
		};

		/// <summary>
		/// Get mouse Y position in screen pixels.
		/// </summary>
		/// <returns>Mouse Y</returns>
		int getMouseY()
		{
			return mMouseY;
		};

		/// <summary>
		/// Get the key currently down.
		/// </summary>
		/// <returns>Keys down</returns>
		short getKeysDown() const
		{
			return mKeysDown;
		}

		/// <summary>
		/// Set the keys currently down.
		/// </summary>
		/// <param name="val">Keys to be set down</param>
		void setKeysDown(const short val)
		{
			mKeysDown = val;
		}
	protected:
		short mKeysDown = 0; ///< The number of keys currently down
		unsigned char mKeyBuffer[127 + 255]; ///< Buffer flags for keycodes
		unsigned char mMouseBuffer[4]; ///< Buffer flags mouse codes
		int mMouseX = 0; ///< Mouse X from the mouse input device
		int mMouseY = 0; ///< Mouse y from the mouse input device
	};
}

