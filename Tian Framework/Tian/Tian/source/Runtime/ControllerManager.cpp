#include "Input.h"
#include "ControllerManager.h"

namespace Tian
{
	ControllerManager::ControllerManager(const us controllerCount)
	{
		mControllers = std::make_unique<vector<Controller>>();

		for (us i = 0; i < controllerCount; ++i)
		{
			mSDLSticks.push_back(SDL_JoystickOpen(i));
			mControllers->emplace_back();
		}
	}
	ControllerManager::~ControllerManager()
	{
		free();
	}
	Controller& ControllerManager::getController(const us index)
	{
		return mControllers.get()->at(index);
	}
	us ControllerManager::getCount() const
	{
		return mControllers.get()->size();
	}
	str ControllerManager::toString() const
	{
		str s;

		for (us i = 0; i < mControllers->size(); ++i)
		{
			s += "Pad " + std::to_string(i) + " : " + mControllers->at(i).toString() + "\n";
		}

		return s;
	}
	void ControllerManager::free()
	{
		for (auto joyPad : mSDLSticks)
		{
			SDL_JoystickClose(joyPad);
			joyPad = NULL;
		}

		mSDLSticks.clear();
	}
	void ControllerManager::flushHats()
	{
		for (auto& joyPad : *mControllers)
		{
			joyPad.flushHats();
		}
	}
	void ControllerManager::setAxis(const SDL_JoyAxisEvent axisEvent)
	{
		mControllers->at(axisEvent.which).setAxis(axisEvent);
	}
	void ControllerManager::setButton(const SDL_JoyButtonEvent buttonEvent, const bool isDown)
	{
		mControllers->at(buttonEvent.which).setButton(buttonEvent, isDown);
	}
	void ControllerManager::setHat(const SDL_JoyHatEvent hatEvent)
	{
		mControllers->at(hatEvent.which).setHat(hatEvent);
	}
	Sint16 ControllerManager::getAxis(const us index, const us axisIndex) const
	{
		return mControllers->at(index).getAxis(axisIndex);
	}
	bool ControllerManager::getHat(const us index, const us hatIndex) const
	{
		return mControllers->at(index).getHat(hatIndex);
	}
	bool ControllerManager::getButton(const us index, const us buttonIndex) const
	{
		return mControllers->at(index).getButton(buttonIndex);
	}
}