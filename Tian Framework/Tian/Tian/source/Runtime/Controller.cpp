#include "Input.h"
#include "Controller.h"

namespace Tian
{
	void Controller::flushHats()
	{
		for (auto& hat : mHats)
		{
			hat = false;
		}
	}
	void Controller::setAxis(const SDL_JoyAxisEvent axisEvent)
	{
		// Check deadzone
		if (axisEvent.value >= -mDeadZone && axisEvent.value <= mDeadZone)
		{
			mAxis[axisEvent.axis] = 0;
			return;
		}

		mAxis[axisEvent.axis] = axisEvent.value;
	}
	void Controller::setButton(const SDL_JoyButtonEvent buttonEvent, const bool isDown)
	{
		mButtons[buttonEvent.button] = isDown;
	}
	void Controller::setHat(const SDL_JoyHatEvent hatEvent)
	{
		mHats[hatEvent.value] = true;
	}
	Sint16 Controller::getAxis(const us axisIndex)
	{
		return mAxis[axisIndex];
	}
	bool Controller::getHat(const us hatIndex) const
	{
		return mHats[hatIndex];
	}
	bool Controller::getButton(const us buttonIndex) const
	{
		return mButtons[buttonIndex];
	}
	str Controller::toString() const
	{
		str s;

		// Axis
		for (const auto axis : mAxis)
		{
			s += std::to_string(axis) + ",";
		}

		s += "|";

		// Hats
		for (const auto hat : mHats)
		{
			s += std::to_string(hat) + ",";
		}

		s += "|";

		// Buttons
		for (const auto button : mButtons)
		{
			s += std::to_string(button) + ",";
		}

		return s;
	}
}