///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// A abstract class for all engine object types
	/// </summary>
	class Object
	{
	public:
		/// <summary>
		/// Create a new object with a id code.
		/// </summary>
		/// <param name="id">Id code</param>
		Object(const ui id = 0) : mID(id) {};
		~Object() {};

		/// <summary>
		/// Test if this object is the same as another.
		/// </summary>
		/// <param name="obj">Object to compare against</param>
		/// <returns>True if the objects are the same</returns>
		virtual const bool operator==(const Object& obj) const;

		/// <summary>
		/// Set a new id of the object.
		/// </summary>
		/// <param name="id">New id</param>
		void setID(const ui id);

		/// <summary>
		/// Get the object id code.
		/// </summary>
		/// <returns>Object's id code</returns>
		const ui getID() const;
	protected:
		ui mID; ///< The object's id code
	};
}

