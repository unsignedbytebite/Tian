#include "AudioCue.h"

namespace Tian
{
	AudioCue::AudioCue(const str& path)
	{
		loadFromFile(path);
	}

	AudioCue::~AudioCue()
	{
		free();
	}

	void AudioCue::loadFromFile(const str& path)
	{
		mCue = Mix_LoadWAV(path.c_str());
		if (mCue == NULL)
		{
			throw Error("Audio cue cannot be loaded: " + path, __func__);
		}
	}

	void AudioCue::free()
	{
		Mix_FreeChunk(mCue);
		mCue = NULL;
	}
	void AudioCue::play(const int channel, const int loops)
	{
		Mix_PlayChannel(channel, mCue, loops);
	}

	void AudioCue::playOnce(const int channel, const int loops)
	{
		if (!Mix_Playing(channel))
		{
			Mix_PlayChannel(channel, mCue, loops);
		}
	}
}
