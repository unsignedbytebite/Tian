///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "AudioCue.h"

namespace Tian
{
	/// <summary>
	/// Looks after all audio cues. To extended for custom audio logic in the business code.
	/// </summary>
	class AudioEngine
	{
	public:
		/// <summary>
		/// Default constructor initializes the core audio engine.
		/// </summary>
		AudioEngine();

		/// <summary>
		/// Calls free for the core audio engine.
		/// </summary>
		~AudioEngine();

		/// <summary>
		/// Load and create an audio cue from a path.
		/// </summary>
		/// <param name="filePath">Path to audio file</param>
		virtual void load(const str& filePath);

		/// <summary>
		/// Cue a sound event. Can be extended for custom playback.
		/// </summary>
		/// <param name="index">Index of the cue to be played</param>
		/// <param name="channel">Channel to play the cue on</param>
		/// <param name="loops">Number of the loops to play the cue with</param>
		virtual void cue(const us index, const int channel = -1, const int loops = 0);

		/// <summary>
		/// Cue a sound event only if the channel is not used. Can be extended for custom playback.
		/// </summary>
		/// <param name="index">Index of the cue to be played</param>
		/// <param name="channel">Channel to play the cue on</param>
		/// <param name="loops">Number of the loops to play the cue with</param>
		virtual void cueOnce(const us index, const int channel = -1, const int loops = 0);
	protected:
		vector<shared_ptr<AudioCue>> mCues; ///< The cache of audio cues.
	};
}

