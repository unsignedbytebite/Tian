///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Runtime/Asset.h"
#include "../Utils/Vector2.h"

namespace Tian
{
	/// <summary>
	/// Audio data that can be loaded from a file and played on a channel.
	/// </summary>
	class AudioCue :
		public Asset
	{
	public:
		/// <summary>
		/// Create the audio cue with a wav file. Calls loadFromFile().
		/// </summary>
		/// <param name="path">Wav path</param>
		AudioCue(const str& path);

		/// <summary>
		/// Calls free on destruction.
		/// </summary>
		~AudioCue();

		/// <summary>
		/// Load the AudioCue from a file
		/// </summary>
		/// <param name="path">Path to the wav file</param>
		virtual void loadFromFile(const str& path);

		/// <summary>
		/// Free the AudioCue
		/// </summary>
		virtual void free();

		/// <summary>
		/// Play the cue.
		/// </summary>
		/// <param name="channel">Channel to play the cue on</param>
		/// <param name="loops">The number of loops to play the cue</param>
		virtual void play(const int channel = -1, const int loops = 0);

		/// <summary>
		/// Play the cue only if the channel is free.
		/// </summary>
		/// <param name="channel">Channel to play on</param>
		/// <param name="loops">The number of loops to play the cue</param>
		virtual void playOnce(const int channel = -1, const int loops = 0);
	protected:
		Mix_Chunk* mCue; ///< The loaded audio data.
	};
}

