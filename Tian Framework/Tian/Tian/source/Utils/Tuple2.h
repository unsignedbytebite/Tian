///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	template<class T>
	class Tuple2
	{
	public:
		T x{ 0 };
		T y{ 0 };

		Tuple2(const T setX = 0, const T setY = 0)
		{
			set(setX, setY);
		};
		Tuple2(const Tuple2& copyVal)
		{
			set(copyVal);
		}
		~Tuple2()
		{};

		// Operators
		Tuple2 operator+(const Tuple2& a) const
		{
			return Tuple2{ x + a.x, y + a.y };
		}

		Tuple2 operator-(const Tuple2& a) const
		{
			return Tuple2{ x - a.x, y - a.y };
		}

		Tuple2 operator*(const Tuple2& a) const
		{
			return Tuple2{ x * a.x, y * a.y };
		}

		Tuple2 operator*(const T& a) const
		{
			return Tuple2{ x * a, y * a };
		}

		Tuple2 operator=(const T& a) const
		{
			set(a);
			return this;
		}

		bool operator==(const T& a) const
		{
			return equals(a);
		}

		// Set the new values
		void set(const T setX, const T setY)
		{
			x = setX;
			y = setY;
		}
		void set(const Tuple2& val)
		{
			x = val.x;
			y = val.y;
		}

		// Get the tuple values
		T getX() const
		{
			return x;
		}
		T getY() const
		{
			return y;
		}

		// Move the current values
		void move(const T moveX, const T moveY)
		{
			x += moveX;
			y += moveY;
		}
		void move(const Tuple2& val)
		{
			x += val.x;
			y += val.y;
		}

		// Does val equal this point
		bool equals(const Tuple2& val) const
		{
			return (val.x == x && val.y == y);
		}

		// Round a Tuple2
		static Tuple2 doRound(const Tuple2& val)
		{
			return{ (T)round(val.x), (T)round(val.y) };
		}

		// Floor a Tuple2
		static Tuple2 doFloor(const Tuple2& val)
		{
			return{ (T)floor(val.x), (T)floor(val.y) };
		}

		// Return the sum of two values
		static Tuple2 add(const Tuple2& valA, const Tuple2& valB)
		{
			return Tuple2{ valA.x + valB.x, valA.y + valB.y };
		}

		// Is this val contained in these points
		bool isBounded(const T xBoundUpper, const T yBoundUpper, const T xBoundLower = 0, const T yBoundLower = 0) const
		{
			return (xBoundLower <= x && x < xBoundUpper &&  yBoundLower <= y && y < yBoundUpper);
		}

		const str getString() const
		{
			return std::to_string(x) + "," + std::to_string(y);
		}
	};

	// Typedefines
	using Float2 = Tuple2<float>;
	using Us2 = Tuple2<us>;
	using Ui2 = Tuple2<ui>;
	using Int2 = Tuple2<int>;
	using Short2 = Tuple2<short>;
	using Double2 = Tuple2<double>;
}

