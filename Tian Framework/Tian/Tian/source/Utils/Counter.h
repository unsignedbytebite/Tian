///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// A counter of defined type that can be used for counter based trigger events. Example of use is for a blinking light.
	/// </summary>
	template<class T>
	class Counter
	{
	public:
		/// <summary>
		/// Create a new counter with parameters.
		/// </summary>
		/// <param name="upperBound">The upper number where trigger event will be returned False</param>
		/// <param name="lowerBound">The lower number where trigger event will be returned True</param>
		/// <param name="step">The number to step with in the counter</param>
		/// <param name="startingCount">The starting count number</param>
		/// <param name="loopOut">The number where the counter will loop back</param>
		/// <param name="loopIn">The number where the counter loops into after reaching the loop out number</param>
		Counter(const T upperBound = 10,
			const T lowerBound = 5,
			const T step = 1,
			const T startingCount = 0,
			const T loopOut = 20,
			const T loopIn = 0) :
			mUpperBound{ upperBound },
			mLowerBound{ lowerBound },
			mStep{ step },
			mCounter{ startingCount },
			mLoopOut{ loopOut },
			mLoopIn{ loopIn }
		{}

		/// <summary>
		/// Count a step and returns if in bounds.
		/// </summary>
		/// <returns>True if in bounds</returns>
		bool count()
		{
			return count(mStep);
		}

		/// <summary>
		/// Count a step and returns if in bounds.
		/// </summary>
		/// <param name="step">Step to count on</param>
		/// <returns>True if in bounds</returns>
		bool count(const T step)
		{
			mCounter += step;

			// Check for loops
			mCounter = (mCounter > mLoopOut) ? mLoopIn + (mCounter - mLoopOut) : mCounter;
			mCounter = (mCounter < mLoopIn) ? mLoopOut + (mCounter - mLoopIn) : mCounter;

			// Check if in trigger range
			isTriggered = (mCounter < mUpperBound) && (mCounter > mLowerBound);

			return isTriggered;
		}

		/// <summary>
		/// Returns if the counter is triggered by crossing into its bounds.
		/// </summary>
		/// <returns>True if in bounds</returns>
		bool isTriggerd()
		{
			return isTriggered;
		}

		/// <summary>
		/// Set the current counter value.
		/// </summary>
		/// <param name="count">New counter value</param>
		void setCount(const T count)
		{
			mCounter = count;
		}
		
		/// <summary>
		/// Get the current counter value.
		/// </summary>
		/// <returns>Current count</returns>
		T getCount()
		{
			return mCounter;
		}

		/// <summary>
		/// Get the counter string for it parameters.
		/// </summary>
		/// <returns>String format of the Counter's parameters</returns>
		const str getString() const
		{
			return to_string(mUpperBound) + "," +
				to_string(mLowerBound) + "," +
				to_string(mStep) + "," +
				to_string(mCounter) + "," +
				to_string(mLoopOut) + "," +
				to_string(mLoopIn);
		}
	private:
		T mCounter{ 0 }; ///< The current counter.
		T mStep{ 0 }; ///< The amount to step in the counter.
		T mUpperBound{ 0 }; ///< The upperbound to trigger the counter.
		T mLowerBound{ 0 }; ///< The lowerbound to trigger the counter.
		T mLoopOut{ 0 }; ///< The loop out.
		T mLoopIn{ 0 }; ///< The loop in.
		bool isTriggered; ///< Is the counter been triggered.
	};
}


