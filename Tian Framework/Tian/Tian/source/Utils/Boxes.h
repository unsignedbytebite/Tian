///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Vector2.h"
#include "Box.h"

namespace Tian
{
	/// <summary>
	/// A manager for a collection of boxes.
	/// </summary>
	class Boxes
	{
	public:
		/// <summary>
		/// Create a new collection of Box objects.
		/// </summary>
		/// <param name="boxes">Box list to use</param>
		Boxes(const BoxList boxes);

		/// <summary>
		/// Default constructor.
		/// </summary>
		Boxes();
		~Boxes();

		/// <summary>
		/// Add a box into the list.
		/// </summary>
		/// <param name="box">New box to add</param>
		void add(const Box box);

		/// <summary>
		/// Clear the list of boxes.
		/// </summary>
		void clear();

		/// <summary>
		/// Set the reference location of all the boxes with this location.
		/// </summary>
		/// <param name="location">New reference location</param>
		void updateLocation(const Vector2 location);

		/// <summary>
		/// Sets the new list of boxes.
		/// </summary>
		/// <param name="boxes">New box list</param>
		void setBoxes(const BoxList boxes);

		/// <summary>
		/// Reference to the box collection.
		/// </summary>
		/// <returns>BoxList handle</returns>
		BoxList& boxes();

		/// <summary>
		/// Check if a point is contained in any of the boxes in the collection.
		/// </summary>
		/// <param name="targetX">X position</param>
		/// <param name="targetY">Y position</param>
		/// <returns>True if contained</returns>
		bool contains(const int targetX, const int targetY) const;

		/// <summary>
		/// Check if a point is contained in any of the boxes in the collection.
		/// </summary>
		/// <param name="point">XY position</param>
		/// <returns>True if contained</returns>
		bool contains(const Vector2& point) const;

		/// <summary>
		/// Check if a box is contained in any of the boxes in the collection.
		/// </summary>
		/// <param name="box">Other Box</param>
		/// <returns>True if contained</returns>
		bool contains(const Box& box) const;

		/// <summary>
		/// Check if a collection of Box objects are contained in any of the boxes in the collection.
		/// </summary>
		/// <param name="boxes">Box collection</param>
		/// <returns>True if contained</returns>
		bool contains(const Boxes& boxes) const;

		/// <summary>
		/// Check if a collection of Box objects intersect any of the boxes in the collection.
		/// </summary>
		/// <param name="boxes">Other Box collection</param>
		/// <returns>True if intersects</returns>
		bool intersects(const Boxes& boxes) const;

		/// <summary>
		/// Check if a Box object intersect any of the boxes in the collection.
		/// </summary>
		/// <param name="boxes">Other Box</param>
		/// <returns>True if intersects</returns>
		bool intersects(const Box& box) const;
	private:
		BoxList mBoxes; ///< Box collection
	};
}

