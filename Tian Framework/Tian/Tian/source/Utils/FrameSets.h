///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "FrameSet.h"
#include "../IO/FileIO.h"

namespace Tian
{
	class FrameSets
	{
	public:
		// Serialize in this object
		void serializeIn(FileIO& file);

		// Serialize out this object
		void serializeOut(FileIO& file) const;

		// Add a set to the collection
		void add(const FrameSet set);

		// Get the list
		std::vector<FrameSet>& get();

		// Get the number of sets in this collection
		const us size() const;

		// Get the set at an index
		FrameSet& at(const us index);
	protected:
		// Set list
		std::vector<FrameSet> mSets;
	};
}
