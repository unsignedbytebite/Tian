#include "Timer.h"

namespace Tian
{
	Timer::Timer() : mTimeOffset(0)
	{}

	void Timer::splitTime()
	{
		mTimeOffset = SDL_GetTicks();
	}

	const ui Timer::getMs() const
	{
		return SDL_GetTicks() - mTimeOffset;
	}
}
