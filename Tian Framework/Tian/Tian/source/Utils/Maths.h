///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "Vector2.h"
#include "Tuple2.h"
#include "Tuple4.h"

namespace Tian
{
	const static float pi = 3.14159265358979323846f;

	/// <summary>
	/// Common math functions
	/// </summary>
	class Maths
	{	
	public:

		/// <summary>
		/// Set a random seed
		/// </summary>
		static void randomSeed()
		{
			std::srand((unsigned int)time(NULL));
		}

		/// <summary>
		/// Get a random number 
		/// </summary>
		/// <returns>std::rand()</returns>
		static int random()
		{
			return std::rand();
		}

		/// <summary>
		/// Get a random number 
		/// </summary>
		/// <param name="mod">Modulus value</param>
		/// <returns>Random number by mod</returns>
		template<class T>
		static T random(const ui mod)
		{
			return (T)(std::rand() % mod);
		}

		/// <summary>
		/// A scaled cosine wave 0 to 1
		/// </summary>
		/// <param name="time">Time amount</param>
		/// <param name="freq">Frequency amount</param>
		/// <param name="phase">Phase amount</param>
		/// <returns>Value of the wave at the time</returns>
		static double cosine(const double time, const double freq = 1.0, const double phase = 0.0)
		{
			return ((cos((time + phase) * pi * freq) * 0.5) + 0.5);
		}

		/// <summary>
		/// Fast square root.
		/// </summary>
		/// <param name="x">Value to square</param>
		/// <returns>Square root of x</returns>
		static float fastSq(const float x)
		{
			const float xhalf = 0.5f * x;
			int i = *(int*)&x;            // store floating-point bits in integer
			i = 0x5f3759df - (i >> 1);    // initial guess for Newton's method
			float r = *(float*)&i;        // convert new bits into float
			r = x*(1.5f - xhalf*x*x);     // One round of Newton's method
			return x;
		}

		/// <summary>
		/// Returns what quadrant a point is in from another
		/// </summary>
		/// <param name="x1">Point 1 X</param>
		/// <param name="y1">Point 2 Y</param>
		/// <param name="x2">Point 1 X</param>
		/// <param name="y2">Point 2 Y</param>
		/// <returns>1-4 quadrant value</returns>
		static us getCircleQuadrant(const float x1, const float y1, const float x2, const float y2)
		{
			if (x1 < x2 && y1 > y2)
			{
				return 1;
			}
			else if (x1 < x2 && y1 < y2)
			{
				return 2;
			}
			else if (x1 > x2 && y1 < y2)
			{
				return 3;
			}
			else if (x1 > x2 && y1 > y2)
			{
				return 4;
			}

			return 0;
		}

		/// <summary>
		/// Get an angle between two points.
		/// </summary>
		/// <param name="x1">Point 1 X</param>
		/// <param name="y1">Point 2 Y</param>
		/// <param name="x2">Point 1 X</param>
		/// <param name="y2">Point 2 Y</param>
		/// <returns>Angle</returns>
		static double getAngle(const float x1, const float y1, const float x2, const float y2)
		{
			const double adj = x2 - x1;
			const double opp = y2 - y1;
			const double angle = atan(opp / adj);

			switch (getCircleQuadrant(x1, y1, x2, y2))
			{
			case 1: // TOP RIGHT
				return angle;
			case 2: // BOTTOM RIGHT
				return angle;
			case 3:
				return (-((1.5f * pi) - angle)) - (1.5f * pi);
			case 4: // TOP LEFT
				return (-((1.5f * pi) - angle)) - (1.5f * pi);
			}

			return 0;
		}

		/// <summary>
		/// Get an angle between two points.
		/// </summary>
		/// <param name="a">Point A</param>
		/// <param name="b">Point B</param>
		/// <returns>Angle</returns>
		static double getAngle(const Vector2 a, const Vector2 b)
		{
			return getAngle(a.x, a.y, b.x, b.y);
		}

		/// <summary>
		/// Degrees to radians.
		/// </summary>
		/// <param name="degrees">Degrees</param>
		/// <returns>Radians</returns>
		static double degreesToRadians(const float degrees)
		{
			return (degrees * (pi / 180));
		}

		/// <summary>
		/// Radians to degrees
		/// </summary>
		/// <param name="radians">Radians</param>
		/// <returns>Degrees</returns>
		static double radiansToDegrees(const float radians)
		{
			return (radians * (180 / pi));
		}

		/// <summary>
		/// Get the distance between two points.
		/// </summary>
		/// <param name="x1">Point 1 X</param>
		/// <param name="y1">Point 2 Y</param>
		/// <param name="x2">Point 1 X</param>
		/// <param name="y2">Point 2 Y</param>
		/// <returns>Distance</returns>
		static double distance(const float x1, const float y1, const float x2, const float y2)
		{
			return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
		}

		/// <summary>
		/// Get the distance between two points.
		/// </summary>
		/// <param name="start">Start point </param>
		/// <param name="end">End point</param>
		/// <returns>Distance</returns>
		static double distance(const Us2& start, const Us2& end)
		{
			return sqrt(
				(end.x - start.x) * (end.x - start.x) +
				(end.y - start.y) * (end.y - start.y));
		}

		/// <summary>
		/// Get the distance between two points.
		/// </summary>
		/// <param name="start">Start point </param>
		/// <param name="end">End point</param>
		/// <returns>Distance</returns>
		static double distance(const Vector2& start, const Vector2& end)
		{
			return sqrt(
				(end.x - start.x) * (end.x - start.x) +
				(end.y - start.y) * (end.y - start.y));
		}


		/// <summary>
		/// Fast floor
		/// </summary>
		/// <param name="x">Value to floor</param>
		/// <returns>Floored x</returns>
		static int fastfloor(const double x)
		{
			int xi = (int)x;
			return x < xi ? xi - 1 : xi;
		}

		/// <summary>
		/// Get the gradient factor over a box.
		/// </summary>
		/// <param name="x">X box position</param>
		/// <param name="y">y box position</param>
		/// <param name="amplitude">Amplitude if the gradient</param>
		/// <returns>Gradient amount</returns>
		static double gradientBox(const double x, const double y, const double amplitude)
		{
			double r = sin(x*3.14) * sin(y * 3.14);
			r *= amplitude;
			r /= 2;

			// Clamp
			r = (r > 1) ? r = 1 : r;
			r = (r < 0) ? r = 0 : r;

			return r;
		}

		/// <summary>
		/// Interpolate across a line.
		/// </summary>
		/// <param name="a">Point A</param>
		/// <param name="b">Point B</param>
		/// <param name="t">Time</param>
		/// <returns>Interpolated value</returns>
		static float interpolate(const float a, const float b, const float t)
		{
			return (a + t * (b - a));
		}

		/// <summary>
		/// Calculate the dot product between two double4.
		/// </summary>
		/// <param name="a">Point A</param>
		/// <param name="b">Point B</param>
		/// <returns>Dot product</returns>
		static double dot(const Double4* a, const Double4* b)
		{
			return a->x*b->x + a->y*b->y + a->w*b->w + a->h*b->h;
		}

		/// <summary>
		/// Clamps a value between two bounds
		/// </summary>
		/// <param name="value">Value</param>
		/// <param name="value">Lower bound</param>
		/// <param name="value">Upper bound</param>
		/// <returns>Clamped value</returns>
		template<class T>
		static void clamp(T& value, const T lowerBound, const T upperBound)
		{
			if (value > upperBound)
			{
				value = upperBound;
			}
			else if (value < lowerBound)
			{
				value = lowerBound;
			}
		}

		/// <summary>
		/// Convert radians to a vector rotation.
		/// </summary>
		/// <param name="rads">Radians</param>
		/// <returns>Vector rotation</returns>
		static Vector2 radiansToVector2(const float rads)
		{
			return{ cos(rads), sin(rads) };
		}
	};
}

