#include "FrameSet.h"

namespace Tian
{
	FrameSet::FrameSet(const us delay) : mDelay(delay), isPaused(false), mFrameHead(0)
	{}

	void FrameSet::add(const int x, const int y, const int width, const int height)
	{
		mFrames.push_back(SDL_Rect{ x, y, width, height });
	}

	void FrameSet::add(const SDL_Rect frame)
	{
		mFrames.push_back(frame);
	}

	void FrameSet::step()
	{
		++mFrameCount;

		if (mFrameCount >= mDelay && !isPaused)
		{
			mFrameCount = 0;

			//Go to next frame
			++mFrameHead;

			// If frame head is greater than the set size
			if (mFrameHead > mFrames.size() - 1)
			{
				mFrameHead = 0;
			}
		}
	}

	SDL_Rect* FrameSet::current()
	{
		return &mFrames.at(mFrameHead);
	}

	Frames FrameSet::getFrames() const
	{
		return mFrames;
	}
	void FrameSet::setPause(const bool pause)
	{
		isPaused = pause;
	}
	void FrameSet::setFrameDelay(const us delay)
	{
		mDelay = delay;
	}
	const us FrameSet::getFrameDelay() const
	{
		return mDelay;
	}
	const us FrameSet::getFrameHead() const
	{
		return mFrameHead;
	}
	void FrameSet::setFrameHead(const us indexHead)
	{
		mFrameHead = indexHead;
	}
}
