///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include <SDL.h>
#include "../Common.h"

namespace Tian
{
	/// <summary>
	/// A SDL tick stopwatch timer.
	/// </summary>
	class Timer
	{
	public:
		Timer();

		/// <summary>
		/// Split the current time. The timer will rest to 0.
		/// </summary>
		void splitTime();

		/// <summary>
		/// Get the MS since the last time that spliTime() was called.
		/// </summary>
		/// <returns>MS past since last split time</returns>
		const ui getMs() const;
	private:
		ui mTimeOffset; ///< The time offset vs total ticks
	};
}

