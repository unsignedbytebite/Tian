///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"
#include "../Box2d/Common/b2Math.h"

namespace Tian
{
	/// <summary>
	/// Extends Box2d vector 2.
	/// </summary>
	class Vector2 : public b2Vec2
	{
	public:
		Vector2() = default;

		/// <summary>
		/// Create a new vector at a location
		/// </summary>
		/// <param name="x">X position</param>
		/// <param name="y">y position</param>
		Vector2(const float x, const float y) : b2Vec2(x, y) {}

		/// <summary>
		/// Check if this Vector2 equals another.
		/// </summary>
		/// <param name="a">Other vector to test against</param>
		/// <returns>True if the values are the same</returns>
		bool operator==(const Vector2& a) const
		{
			return equals(a);
		}

		Vector2 operator+(const Vector2& a) const
		{
			return Vector2{ x + a.x, y + a.y };
		}

		Vector2 operator-(const Vector2& a) const
		{
			return Vector2{ x - a.x, y - a.y };
		}

		Vector2 operator*(const Vector2& a) const
		{
			return Vector2{ x * a.x, y * a.y };
		}

		Vector2 operator*(const float& a) const
		{
			return Vector2{ x * a, y * a };
		}

		Vector2 operator=(const Vector2& a)
		{
			set(a);
			return *this;
		}

		/// <summary>
		/// Set new values.
		/// </summary>
		/// <param name="setX">X position</param>
		/// <param name="setY">y position</param>
		void set(const float setX, const float setY)
		{
			x = setX;
			y = setY;
		}

		/// <summary>
		/// Set new values.
		/// </summary>
		/// <param name="val">XY position</param>
		void set(const Vector2& val)
		{
			x = val.x;
			y = val.y;
		}

		/// <summary>
		/// Get x position.
		/// </summary>
		/// <returns>X position</returns>
		float getX() const
		{
			return x;
		}

		/// <summary>
		/// Get y position.
		/// </summary>
		/// <returns>Y position</returns>
		float getY() const
		{
			return y;
		}

		/// <summary>
		/// Move the current values by an amount.
		/// </summary>
		/// <param name="moveX">Delta x</param>
		/// <param name="moveY">Delta y</param>
		void move(const float moveX, const float moveY)
		{
			x += moveX;
			y += moveY;
		}

		/// <summary>
		/// Move the current values by an amount.
		/// </summary>
		/// <param name="val">Delta XY</param>
		void move(const Vector2& val)
		{
			x += val.x;
			y += val.y;
		}

		/// <summary>
		/// Check if another vector equals this vector.
		/// </summary>
		/// <param name="val">Other vector</param>
		/// <returns>True is equal</returns>
		bool equals(const Vector2& val) const
		{
			return (val.x == x && val.y == y);
		}

		/// <summary>
		/// Round the values of a vector.
		/// </summary>
		/// <param name="val">Vector to round</param>
		/// <returns>New vector that is rounded</returns>
		static Vector2 doRound(const Vector2& val)
		{
			return{ round(val.x), round(val.y) };
		}

		/// <summary>
		/// Floor the values of a vector.
		/// </summary>
		/// <param name="val">Vector to floor</param>
		/// <returns>New vector that is floor</returns>
		static Vector2 doFloor(const Vector2& val)
		{
			return{ floor(val.x), floor(val.y) };
		}

		/// <summary>
		/// Add two vectors together. 
		/// </summary>
		/// <param name="valA">Vector A</param>
		/// <param name="valB">Vector B</param>
		/// <returns>New Vector2 the sum of A and B</returns>
		static Vector2 add(const Vector2& valA, const Vector2& valB)
		{
			return Vector2{ valA.x + valB.x, valA.y + valB.y };
		}

		/// <summary>
		/// Test if this vector is contained in points.
		/// </summary>
		/// <param name="xBoundUpper">X upper bound</param>
		/// <param name="yBoundUpper">y upper bound</param>
		/// <param name="xBoundLower">x lower bound</param>
		/// <param name="yBoundLower">y lower bound</param>
		/// <returns></returns>
		bool isBounded(const float xBoundUpper, const float yBoundUpper, const float xBoundLower = 0, const float yBoundLower = 0) const
		{
			return (xBoundLower <= x && x < xBoundUpper &&  yBoundLower <= y && y < yBoundUpper);
		}

		/// <summary>
		/// Get a string representation of the vector.
		/// </summary>
		/// <returns>Formatted string</returns>
		const str getString() const
		{
			return "[" + std::to_string(x) + "," + std::to_string(y) + "]";
		}
	};

	using Vector2Points = std::vector<Vector2>;
}

