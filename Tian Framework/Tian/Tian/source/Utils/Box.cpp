#include "Box.h"

namespace Tian
{
	Vector2 Box::mDefault = { 0, 0 };

	Box::Box(Vector2& location, const SDL_Rect rect) : mLoco(location), mRect(rect)
	{}
	Box::Box(const SDL_Rect rect) : mLoco(mDefault), mRect(rect)
	{}
	Box::Box() : mLoco(mDefault)
	{}
	const Box Box::operator=(const Box & box)
	{
		return{ box.mLoco, box.mRect };
	}
	Box Box::operator/(const Vector2& Vector2)
	{
		return{ mLoco, { mRect.x / (int)Vector2.x,
			mRect.y / (int)Vector2.y,
			mRect.w / (int)Vector2.x,
			mRect.h / (int)Vector2.y } };
	}
	Box Box::operator+(const Vector2& Vector2)
	{
		return{ mLoco, { mRect.x + (int)Vector2.x,
			mRect.y + (int)Vector2.y,
			mRect.w + (int)Vector2.x,
			mRect.h + (int)Vector2.y } };
	}
	void Box::setRect(const SDL_Rect rect)
	{
		mRect = rect;
	}
	SDL_Rect Box::getRect() const
	{
		return mRect;
	}

	SDL_Rect Box::getBounds() const
	{
		return SDL_Rect{ (int)mLoco.x + mRect.x, (int)mLoco.y + mRect.y, mRect.w, mRect.h };
	}
	void Box::setLocation(const Vector2& location)
	{
		mLoco = location;
	}
	Vector2& Box::getLocal() const
	{
		return mLoco;
	}
	bool Box::contains(const int targetX, const int targetY) const
	{
		const int left = this->left();
		const int right = this->right();
		const int bottom = this->bottom();
		const int top = this->top();

		return (targetX >= left
			&& targetX <= right
			&& targetY >= bottom
			&& targetY <= top);
	}
	bool Box::contains(const Vector2& point) const
	{
		return contains((const int)point.x, (const int)point.y);
	}
	bool Box::contains(const Box& box) const
	{
		const bool boxContain = (box.contains(topLeft()) &&
			box.contains(topRight()) &&
			box.contains(bottomLeft()) &&
			box.contains(bottomRight()));

		const bool thisContain = (contains(box.topLeft()) &&
			contains(box.topRight()) &&
			contains(box.bottomLeft()) &&
			contains(box.bottomRight()));

		return (boxContain || thisContain);
	}
	bool Box::intersects(const Box& box) const
	{
		const bool boxContain = (box.contains(topLeft()) ||
			box.contains(topRight()) ||
			box.contains(bottomLeft()) ||
			box.contains(bottomRight()));

		const bool thisContain = (contains(box.topLeft()) ||
			contains(box.topRight()) ||
			contains(box.bottomLeft()) ||
			contains(box.bottomRight()));

		return (boxContain || thisContain);
	}
	const int Box::left() const
	{
		return (const int)mLoco.x + mRect.x;
	}
	const int Box::right() const
	{
		return (const int)mLoco.x + mRect.x + mRect.w;
	}
	const int Box::bottom() const
	{
		return (const int)mLoco.y + mRect.y;
	}
	const int Box::centreX() const
	{
		return (const int)mLoco.x + (mRect.x + (mRect.w / 2));
	}
	const int Box::centreY() const
	{
		return (const int)mLoco.y + (mRect.y + (mRect.h / 2));
	}
	const int Box::top() const
	{
		return (const int)mLoco.y + mRect.y + mRect.h;
	}
	const Vector2 Box::topLeft() const
	{
		return mLoco + Vector2{ (float)mRect.x, (float)mRect.y } +Vector2{ 0.0f, (float)mRect.h };
	}
	const Vector2 Box::topRight() const
	{
		return mLoco + Vector2{ (float)mRect.x, (float)mRect.y } +Vector2{ (float)mRect.w, (float)mRect.h };
	}
	const Vector2 Box::bottomLeft() const
	{
		return mLoco + Vector2{ (float)mRect.x, (float)mRect.y };
	}
	const Vector2 Box::bottomRight() const
	{
		return mLoco + Vector2{ (float)mRect.x, (float)mRect.y } +Vector2{ (float)mRect.w, 0.0f };
	}
}