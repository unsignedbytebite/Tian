///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once
#include "../Common.h"

namespace Tian
{
	using Frames = std::vector<SDL_Rect>;

	/// <summary>
	/// A frame set is used by the TextureAnimated to displaying animation frames on a texture.
	/// </summary>
	class FrameSet
	{
	public:
		/// <summary>
		/// Create a default frameset
		/// </summary>
		/// <param name="delay">The counter delay between progressing to the next frame</param>
		FrameSet(const us delay = 4);

		/// <summary>
		/// Add a frame to the set.
		/// </summary>
		/// <param name="frame">New frame to push onto the collection</param>
		void add(const SDL_Rect frame);

		/// <summary>
		/// Add a frame to the set.
		/// </summary>
		/// <param name="x">X position</param>
		/// <param name="y">Y position</param>
		/// <param name="width">Width of the frame</param>
		/// <param name="height">Height of the frame</param>
		void add(const int x, const int y, const int width, const int height);

		/// <summary>
		/// Process an update step
		/// </summary>
		void step();

		/// <summary>
		/// Get the current frame
		/// </summary>
		/// <returns>Handle to the current frame</returns>
		SDL_Rect* current();

		/// <summary>
		/// Get the frame collection.
		/// </summary>
		/// <returns>Frame sets</returns>
		Frames getFrames() const;

		/// <summary>
		/// Set the frame pause state.
		/// </summary>
		/// <param name="pause">True to pause playback</param>
		void setPause(const bool pause);

		/// <summary>
		/// Set the counter delay between frames.
		/// </summary>
		/// <param name="delay">Count frame delay</param>
		void setFrameDelay(const us delay);

		/// <summary>
		/// Get the delay between frames.
		/// </summary>
		/// <returns>Frame delay</returns>
		const us getFrameDelay() const;

		/// <summary>
		/// Get the current frame head in the playback frame set.
		/// </summary>
		/// <returns>Frame index head</returns>
		const us getFrameHead() const;

		/// <summary>
		/// Set the current frame head of the playback.
		/// </summary>
		/// <param name="indexHead">Frame set index head</param>
		void setFrameHead(const us indexHead);
	protected:
		Frames mFrames; ///< Frame collection.
		us mFrameCount; ///< Number of frames in the set.
		us mFrameHead; ///< Current frame head.
		us mDelay; ///< Count delay between frames.
		bool isPaused; ///< Playback pause state.
	};
}
