#include "Object2D.h"

namespace Tian
{
	const Vector2 Object2D::getLocal() const
	{
		return mLocal;
	}
	void Object2D::setLocal(const Vector2 val)
	{
		mLocal = val;
		moved();
	}
	Vector2& Object2D::local()
	{
		return mLocal;
	}
	const float Object2D::getDirection() const
	{
		return mDir;
	}
	void Object2D::setDirection(const float dir)
	{
		mDir = dir;
	}
	float& Object2D::direction()
	{
		return mDir;
	};

	Box& Object2D::getBounds(const us index)
	{
		return mBounds.boxes().at(index);
	}

	void Object2D::setBounds(const Boxes val)
	{
		mBounds = val;
	};

	Boxes& Object2D::bounds()
	{
		return mBounds;
	}
	void Object2D::created()
	{}
	void Object2D::move(const Vector2& velocity)
	{
		mLocal.move(velocity);
		moved();
	}
	void Object2D::move(const float direction, const float velocity)
	{
		mLocal.x += cos(direction * pi) * velocity;
		mLocal.y += sin(direction * pi) * velocity;
		moved();
	}
	void Object2D::move(const float velocity)
	{
		move(mDir, velocity);
	}
	void Object2D::moved()
	{}
	void Object2D::addBounding(const SDL_Rect rect)
	{
		mBounds.add({ mLocal, rect });
	}
	void Object2D::setBodyLocation(const float x, const float y, const float rot)
	{
		setBodyLocation({ x, y }, rot);
	}
	void Object2D::setBodyLocation(const Vector2& location, const float rot)
	{
		mBody->SetTransform(location, rot);
	}
	b2Body* Object2D::body()
	{
		return mBody;
	}
	b2Body& Object2D::getBody() const
	{
		return *mBody;
	}
	void Object2D::setBody(b2Body& body)
	{
		mBody = &body;
	}
	void Object2D::serializeIn(FileIO&)
	{}
	void Object2D::serializeOut(FileIO& )
	{}
	void Object2D::setFilter(const us category, const us mask, const us groupIndex)
	{
		b2Filter filter;
		filter.categoryBits = category;
		filter.maskBits = mask;
		filter.groupIndex = groupIndex;
		body()->GetFixtureList()->SetFilterData(filter);
	}

}
