///////////////////////////////////////// 
/// Tian Framework
/// 2017
/////////////////////////////////////////

#pragma once

#include "ConBox\ConBox.h"

// STD
#include <string>
#include <vector>
#include <sstream>
#include <math.h>
#include <iosfwd>
#include <iomanip>
#include <memory>
#include <random>
#include <iostream>
#include <fstream>

// SDL
#include <SDL.h>
#include <SDL_stdinc.h>
#include <SDL_keycode.h>
#include <SDL_mouse.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_render.h>
#include <SDL_mixer.h>

namespace Tian
{
	using ui = unsigned int;
	using str = std::string;
	using uc = unsigned char;
	using us = unsigned short;
	using ul = unsigned long long;
	using Point = SDL_Point;

	using Log = ConBox::Log;
	using Error = ConBox::ConBoxError;

	using namespace std;

	// Directions
	enum Direction
	{
		NONE = 0, LEFT, UP, RIGHT, DOWN, COUNT
	};

	/// <summary>
	/// Get the string representation of hex value.
	/// </summary>
	/// <param name="i">Value to get as a HEX string</param>
	template<typename T>
	str toHex(const T i)
	{
		strstream stream;
		stream << "0x"
			<< std::setfill('0') << std::setw(sizeof(T) * 2)
			<< std::hex << i;
		return stream.str();
	}

	/// <summary>
	/// Splits a string using characters the delimiter and ignore any characters in ignore
	/// </summary>
	/// <param name="text">The string to split</param>
	/// <param name="dilmeter">The characters to split on</param>
	/// <param name="ignore">The characters to ignore</param>
	/// <param name="openIgnore">The character to open an ignore nest</param>
	inline static std::vector<str> split(const str text, const str dilmeter, const str ignore = "", const str openIgnore = "")
	{
		std::vector<str> r;
		str builder = "";
		bool ignoreNest = false;
		for (auto c : text)
		{
			bool doIgnore = false;

			if (!ignoreNest)
			{
				for (auto i : ignore)
				{
					if (i == c)
					{
						doIgnore = true;
						break;
					}
				}
			}

			for (auto i : openIgnore)
			{
				if (i == c)
				{
					ignoreNest = !ignoreNest;
					doIgnore = !ignoreNest;
					break;
				}
			}

			if (!doIgnore && !ignoreNest)
			{
				for (auto d : dilmeter)
				{
					if (d == c)
					{
						r.push_back(builder);
						builder = "";
						doIgnore = true;
						break;
					}
				}
			}

			if (!doIgnore && !ignoreNest)
			{
				builder += c;
			}
		}

		r.push_back(builder);

		return r;
	}
}