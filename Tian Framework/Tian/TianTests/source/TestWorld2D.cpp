#include "TestWorld2D.h"

void TestWorld2D::preload()
{
	const float y = 64;
	const float x = 64;
	const int size = 48;

	// Player
	//mPlayer = &mWorld.addObject(new Object2D{});
	//mPlayer->addBounding({ -2, -2, 5, 5 });
	//mPlayer->setLocal({ 8, 8 });

	// Set up portals
	//Portal& p1 = mWorld.addObject(new Portal{});
	//p1.addBounding({ 0, 0, 7, 3 });
	//p1.setLocal({ 8, 0 });
	//Portal& p2 = mWorld.addObject(new Portal{});
	//p2.addBounding({ 0, 0, 7, 3 });
	//p2.setLocal({ 8, (float)size - 4 });
	//
	//// Connect portals
	//p1.setConnectedPortal(&p2);
	//p2.setConnectedPortal(&p1);
	//
	//// Sektors
	//mParentSektor = &mWorld.addSektor(new Sektor{ mWorld.local(), size, size, x, y });
	//mParentSektor->addObject(*mPlayer);
	//mParentSektor->spaces().add({ mWorld.local(), { 20, -8, 12, 14 } });
	//mParentSektor->spaces().add({ mWorld.local(), { 25, 40, 80, 52 } });
	////mParentSektor->areas().add({ mWorld.local(), { 0, 16, 8, 16 } });
	//mParentSektor->addPortal(&p1);
	//
	//{
	//	Sektor& sektor = mWorld.addSektor(new Sektor{ mWorld.local(), size, size, x, y - y });
	//	sektor.addPortal(&p2);
	//}

	// Place objects in sektors
	//mPlayer->setParentSektor(mParentSektor);
}

void TestWorld2D::load()
{}

void TestWorld2D::postload()
{}

void TestWorld2D::update()
{
	const auto step = 0.1f;
	const auto maxVel = 2.2f;
	auto dir = 0.f;
	auto vel = 0.f;

	if (getInput()->isKeydown('w'))
	{
		dir = 1.5f;
		vel = maxVel;
	}
	else if (getInput()->isKeydown('s'))
	{
		dir = 0.5f;
		vel = maxVel;
	}
	else if (getInput()->isKeydown('a'))
	{
		dir = 1.0f;
		vel = maxVel;
	}
	else if (getInput()->isKeydown('d'))
	{
		vel = maxVel;
	}

	// Move only inside the sektor
	for (float doStep = 0; doStep < vel; doStep += step)
	{
		//if (mPlayer->bounds().contains(mParentSektor->spaces()))
		//{
		//	mPlayer->move(dir, step);
		//}
		//else
		//{
		//	mPlayer->move(dir, -(step));
		//	break;
		//}
	}	//

	// Check for portal
	//Portal* portal = mParentSektor->intersectsPortal(*mPlayer);
	//if (portal != NULL && portal->isEnabled())
	//{
	//	mPlayer->setLocal(portal->getConnectedPortal()->getLocal());
	//	mParentSektor->removeObject(*mPlayer);
	//	Sektor* locoSektor = mWorld.findParent(*portal->getConnectedPortal());
	//	mParentSektor = locoSektor;
	//	mParentSektor->addObject(*mPlayer);
	//	portal->setEnabled(false);
	//	portal->getConnectedPortal()->setEnabled(false);
	//}

}

void TestWorld2D::render()
{
	//// Draw the world
	//{
	//	EngineGFX2D::setRenderColour(&camera().renderer(), Colours::BlueViolet);
	//	const SDL_Rect rect = SDL_Rect{
	//		mWorld.getSpace().getBounds().x - (int)camera().getLocal().getX(),
	//		mWorld.getSpace().getBounds().y - (int)camera().getLocal().getY(),
	//		mWorld.getSpace().getBounds().w,
	//		mWorld.getSpace().getBounds().h };
	//
	//	SDL_RenderDrawRect(&camera().renderer(), &rect);
	//}
	//
	//// Draw sketors
	//{
	//	for (auto& sektor : mWorld.sektors())
	//	{
	//		// Draw spaces
	//		{
	//			EngineGFX2D::setRenderColour(&camera().renderer(), Colours::DeepPink);
	//			auto& boxes = sektor->spaces();
	//			for (auto& box : boxes.boxes())
	//			{
	//				const SDL_Rect rect = SDL_Rect{
	//					box.getBounds().x - (int)(camera().getLocal().getX() - sektor->local().x),
	//					box.getBounds().y - (int)(camera().getLocal().getY() - sektor->local().y),
	//					box.getBounds().w,
	//					box.getBounds().h };
	//
	//				SDL_RenderDrawRect(&camera().renderer(), &rect);
	//			}
	//		}
	//
	//		// Draw Areas
	//		{
	//			EngineGFX2D::setRenderColour(&camera().renderer(), Colours::DarkBlue);
	//			auto& areas = sektor->areas();
	//			for (auto& area : areas)
	//			{
	//				for (auto& box : area->boxes())
	//				{
	//					const SDL_Rect rect = SDL_Rect{
	//						box.getBounds().x - (int)(camera().getLocal().getX() - sektor->local().x),
	//						box.getBounds().y - (int)(camera().getLocal().getY() - sektor->local().y),
	//						box.getBounds().w,
	//						box.getBounds().h };
	//
	//					SDL_RenderFillRect(&camera().renderer(), &rect);
	//				}
	//			}
	//		}
	//
	//		// Draw Portals
	//		{
	//			for (auto& obj : sektor->portals())
	//			{
	//				EngineGFX2D::setRenderColour(&camera().renderer(), Colours::Orange);
	//				for (auto& box : obj->bounds().boxes())
	//				{
	//					const SDL_Rect rect = SDL_Rect{
	//						box.getBounds().x - (int)(camera().getLocal().getX() - sektor->local().x),
	//						box.getBounds().y - (int)(camera().getLocal().getY() - sektor->local().y),
	//						box.getBounds().w,
	//						box.getBounds().h };
	//
	//					SDL_RenderDrawRect(&camera().renderer(), &rect);
	//				}
	//
	//				const SDL_Rect rect = SDL_Rect{
	//					(int)obj->local().x - (int)(camera().getLocal().getX() - sektor->local().x),
	//					(int)obj->local().y - (int)(camera().getLocal().getY() - sektor->local().y),
	//					1,
	//					1 };
	//				EngineGFX2D::setRenderColour(&camera().renderer(), Colours::Wheat);
	//				SDL_RenderFillRect(&camera().renderer(), &rect);
	//			}
	//		}
	//
	//		// Draw all the objects in the sektor
	//		{
	//			for (auto& obj : sektor->objects())
	//			{
	//				EngineGFX2D::setRenderColour(&camera().renderer(), Colours::GreenYellow);
	//				for (auto& box : obj->bounds().boxes())
	//				{
	//					const SDL_Rect rect = SDL_Rect{
	//						box.getBounds().x - (int)(camera().getLocal().getX() - sektor->local().x),
	//						box.getBounds().y - (int)(camera().getLocal().getY() - sektor->local().y),
	//						box.getBounds().w,
	//						box.getBounds().h };
	//
	//					SDL_RenderDrawRect(&camera().renderer(), &rect);
	//				}
	//
	//				const SDL_Rect rect = SDL_Rect{
	//					(int)obj->local().x - (int)(camera().getLocal().getX() - sektor->local().x),
	//					(int)obj->local().y - (int)(camera().getLocal().getY() - sektor->local().y),
	//					1,
	//					1 };
	//				EngineGFX2D::setRenderColour(&camera().renderer(), Colours::Wheat);
	//				SDL_RenderFillRect(&camera().renderer(), &rect);
	//			}
	//		}
	//	}
	//}
}

void TestWorld2D::free()
{}

void TestWorld2D::reset()
{}
