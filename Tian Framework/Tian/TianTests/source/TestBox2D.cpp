#include "TestBox2D.h"

void TestBox2D::preload()
{
	camera().setScale({ 4.0f, 4.0f });

	// Define the gravity vector.
	b2Vec2 gravity(0.0f, 0.0f);

	// Construct a world object, which will hold and simulate the rigid bodies.
	mPhysWorld = new b2World(gravity);
	mDebugDraw = new Box2DebugDraw(camera());


	mPhysWorld->SetDebugDraw(mDebugDraw);

	// Walls
	{
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(34.0f, 10.0f);

		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		b2Body* groundBody = mPhysWorld->CreateBody(&groundBodyDef);
		mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 0 }));
		groundBody->SetUserData(mPhysObjs.back().get());

		// Define the ground box shape.
		b2PolygonShape groundBox;
		groundBox.SetAsBox(32, 1);
		groundBody->CreateFixture(&groundBox, 10.0f);
	}

	{
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(34.0f + 32, 32 + 10);

		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		b2Body* groundBody = mPhysWorld->CreateBody(&groundBodyDef);
		mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 0 }));
		groundBody->SetUserData(mPhysObjs.back().get());

		// Define the ground box shape.
		b2PolygonShape groundBox;
		groundBox.SetAsBox(1, 32);
		groundBody->CreateFixture(&groundBox, 10.0f);
	}

	{
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(34.0f - 32, 32 + 10);

		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		b2Body* groundBody = mPhysWorld->CreateBody(&groundBodyDef);
		mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 0 }));
		groundBody->SetUserData(mPhysObjs.back().get());


		// Define the ground box shape.
		b2PolygonShape groundBox;
		groundBox.SetAsBox(1, 32);
		groundBody->CreateFixture(&groundBox, 10.0f);
	}

	{
		// Define the ground body.
		b2BodyDef groundBodyDef;
		groundBodyDef.position.Set(34.0f - 10, 32 + 10.0f);

		// Call the body factory which allocates memory for the ground body
		// from a pool and creates the ground box shape (also from a pool).
		// The body is also added to the world.
		b2Body* groundBody = mPhysWorld->CreateBody(&groundBodyDef);
		mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 0 }));
		groundBody->SetUserData(mPhysObjs.back().get());

		// Define the ground box shape.
		b2PolygonShape groundBox;
		groundBox.SetAsBox(32 - 10, 1);
		groundBody->CreateFixture(&groundBox, 10.0f);
	}

	// Boxes
	{
		for (int i = 0; i < 4; i++)
		{
			// Define the dynamic body. We set its position and call the body factory.
			b2BodyDef bodyDef;
			bodyDef.type = b2_dynamicBody;
			bodyDef.position.Set(48.f + (i * 3.f), 32.f + 10.f);
			bodyDef.linearDamping = 8.8f;
			bodyDef.angularDamping = 8.8f;

			auto* body = mPhysWorld->CreateBody(&bodyDef);
			mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 1 }));
			body->SetUserData(mPhysObjs.back().get());

			// Define another box shape for our dynamic body.
			b2PolygonShape dynamicBox;
			dynamicBox.SetAsBox(1, 1);

			// Define the dynamic body fixture.
			b2FixtureDef fixtureDef;
			fixtureDef.shape = &dynamicBox;
			fixtureDef.density = 0.8f;
			fixtureDef.friction = 1.9f;


			//fixtureDef.restitution = 20.0000f;

			// Add the shape to the body.
			body->CreateFixture(&fixtureDef);
		}
	}

	// Player
	{
		// Define the dynamic body. We set its position and call the body factory.
		b2BodyDef bodyDef;
		bodyDef.type = b2_dynamicBody;
		bodyDef.position.Set(55.0f, 35.f);
		bodyDef.linearDamping = 20.8f;
		bodyDef.angularDamping = 20.8f;
		mBody = mPhysWorld->CreateBody(&bodyDef);
		mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 2 }));
		mBody->SetUserData(mPhysObjs.back().get());

		// Define another box shape for our dynamic body.
		b2CircleShape ciricle;
		ciricle.m_radius = 1;

		// Define the dynamic body fixture.
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &ciricle;
		fixtureDef.density = 0.1f;
		fixtureDef.friction = 2.01f;

		// Add the shape to the body.
		mBody->CreateFixture(&fixtureDef);
	}

	// Sensor
	{
		// Define the ground body.
		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		bodyDef.position.Set(15, 15);
		mTrigger = mPhysWorld->CreateBody(&bodyDef);
		mPhysObjs.push_back(shared_ptr<PhysObj>(new PhysObj{ 3 }));
		mTrigger->SetUserData(mPhysObjs.back().get());


		b2PolygonShape shape;
		vector<b2Vec2> points{ { 1, 1 }, { 8, 3 }, { 3, 5 } };

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.isSensor = true;
		fixtureDef.userData = &thingy;

		shape.Set(&points[0], (int32)points.size());
		mTrigger->CreateFixture(&fixtureDef);
	}
}

void TestBox2D::load()
{}

void TestBox2D::postload()
{}

void TestBox2D::update()
{
	const auto vel = 1.5f;
	auto dir = b2Vec2{ 0, 0 };
	if (getInput()->isKeydown('s'))
	{
		dir = b2Vec2{ 0, vel };
	}
	else if (getInput()->isKeydown('w'))
	{
		dir = b2Vec2{ 0, -vel };
	}
	if (getInput()->isKeydown('a'))
	{
		dir += b2Vec2{ -vel, 0 };
	}
	else if (getInput()->isKeydown('d'))
	{
		dir += b2Vec2{ vel, 0 };
	}
	mBody->ApplyLinearImpulseToCenter(dir, true);

	for (auto* ce = mBody->GetContactList(); ce; ce = ce->next)
	{
		b2Contact& c = *ce->contact;

		b2Body* body = c.GetFixtureA()->GetBody();

		auto* obj = (PhysObj*)body->GetUserData();

		if (obj->type == 3)
		{
			ConBox::Log::p("Contact event");
			body->SetActive(false);
			break;
		}
		else
		{
			if (getInput()->isKeydown('e'))
			{
				body->ApplyForce({ 100.f * mBody->GetLinearVelocity().x, 100.f * mBody->GetLinearVelocity().y }, mBody->GetPosition(), true);
			}
		}
	}

	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	float32 timeStep = 1.0f / 60.0f;
	int32 velocityIterations = 6;
	int32 positionIterations = 2;

	// Instruct the world to perform a single step of simulation.
	// It is generally best to keep the time step and iterations fixed.
	mPhysWorld->Step(timeStep, velocityIterations, positionIterations);
}

void TestBox2D::render()
{
	camera().expose();
	mPhysWorld->DrawDebugData();
	camera().present();
}

void TestBox2D::free()
{
	delete mDebugDraw;
	delete mPhysWorld;
}

void TestBox2D::reset()
{}
