#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestCanvas2D : public Screen2D
{
public:
	enum Assets
	{
		SHEET = 0, WALK_RIGHT, SIZE
	};

	TestCanvas2D(ScreenManager* manager, AssetManager* assets = NULL) :
		Screen2D(manager, assets)
	{

	}
	~TestCanvas2D()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	Canvas* mCanvas;

	// Inherited via Screen2D
	virtual void reset() override;
};

