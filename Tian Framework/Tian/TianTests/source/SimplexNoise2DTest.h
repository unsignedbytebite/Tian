#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class SimplexNoise2DTest : public Screen2D
{
public:
	SimplexNoise2DTest(EngineGFX2D* engine) : Screen2D(engine)
	{};
	~SimplexNoise2DTest()
	{};

	virtual void preload() override;

	virtual void load() override;

	virtual void postload() override;

	virtual void update() override;

	virtual void render() override;

	virtual void free() override;

private:
	us mResolution;
	double mZ;
	Vector2 mLoco;
	SimplexNoise noise;
	double mThreshold;

	// Inherited via Screen2D
	virtual void reset() override;
};
