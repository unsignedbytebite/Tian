#include "TestCanvas2D.h"

void TestCanvas2D::preload()
{}

void TestCanvas2D::load()
{
	camera().setScale(Vector2{ 4, 4 });

	// Create blank canvas
	const us size = 128;

	mCanvas = &addObject(new Canvas{ &camera(), size, size, 2 });
	mCanvas->fillWithCanvasTexture();

	// Get the texture
	auto& tex = *mAssets->get<Texture>(0);

	Vector2 a{ 60, 60 };
	Vector2 b{ 60, 60 };
	mCanvas->drawTexture(tex, a);
	mCanvas->drawTexture(tex, b);

	mCanvas->startExposure(0,0);
	tex.render(&camera().renderer(), 0, 0);
	mCanvas->stopExposure(0,0);

	mCanvas->startExposure(1, 0);
	tex.render(&camera().renderer(), -5, 0);
	mCanvas->stopExposure(1, 0);
}

void TestCanvas2D::postload()
{}

void TestCanvas2D::update()
{
	const float speed = 1.0f;

	if (getInput()->isKeydown('w'))
	{
		camera().local().move(0, -1 * speed);
	}
	else if (getInput()->isKeydown('s'))
	{
		camera().local().move(0, 1 * speed);
	}

	if (getInput()->isKeydown('a'))
	{
		camera().local().move(-1 * speed, 0);
	}
	else if (getInput()->isKeydown('d'))
	{
		camera().local().move(1 * speed, 0);
	}

	if (getInput()->isKeydown('q'))
	{
		camera().moveScale({ 0.1f, 0.1f });
	}
	else if (getInput()->isKeydown('z'))
	{
		camera().moveScale({ -0.1f, -0.1f });
	}
}

void TestCanvas2D::render()
{
	camera().expose();
	mCanvas->draw();
	camera().present();
}

void TestCanvas2D::free()
{}

void TestCanvas2D::reset()
{}
