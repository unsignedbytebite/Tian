#include "TestCamera.h"

void TestCamera::preload()
{}

void TestCamera::load()
{
	Texture* tex = mAssets->get<Texture>(SHEET);

	const int cellW = tex->getWidth() / 4;
	const int cellH = tex->getHeight() / 1;

	FrameSets sets;
	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 0, cellW, cellH);
		set.add(cellW * 1, cellH * 0, cellW, cellH);
		set.add(cellW * 2, cellH * 0, cellW, cellH);
		set.add(cellW * 3, cellH * 0, cellW, cellH);
		sets.add(set);
	}

	mCam.setScale(Vector2{ 2, 2 });
	TextureAnimated texAnimted{ tex, sets };
	mPlayer = new Sprite(texAnimted);
	mPlayer->setCamera(&camera());
}

void TestCamera::postload()
{}

void TestCamera::update()
{
	const float speed = 1.0f;

	mPlayer->setCurrentFrameSet(0);

	if (getInput()->isKeydown('w'))
	{
		mCam.local().move(0, -1 * speed);
	}
	else if (getInput()->isKeydown('s'))
	{
		mCam.local().move(0, 1 * speed);
	}

	if (getInput()->isKeydown('a'))
	{
		mCam.local().move(-1 * speed, 0);
	}
	else if (getInput()->isKeydown('d'))
	{
		mCam.local().move(1 * speed, 0);
	}

	if (getInput()->isKeydown('q'))
	{
		mCam.moveScale({ 0.1f, 0.1f });
	}
	else if (getInput()->isKeydown('z'))
	{
		mCam.moveScale({ -0.1f, -0.1f });
	}
}

void TestCamera::render()
{
	mCam.expose();
	SDL_SetRenderDrawColor(&camera().renderer(), 0x24, 0x1c, 0x24, 255);
	const SDL_Rect rect = { 0 + (const int)mCam.getLocal().getX(), 0 + (const int)mCam.getLocal().getY(), 256, 256 };
	SDL_RenderFillRect(&camera().renderer(), &rect);
	mPlayer->draw();
	mCam.present();
}

void TestCamera::free()
{
	delete mPlayer;
}

void TestCamera::reset()
{}
