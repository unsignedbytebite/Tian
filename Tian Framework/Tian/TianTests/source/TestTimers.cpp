﻿#include "TestTimers.h"

void TestTimers::timer()
{
	Timer timer;
	timer.splitTime();
	SDL_Delay(1000);
	ui time = timer.getMs();
	Log::p(time);
}

void TestTimers::reset()
{}

void TestTimers::preload()
{}

void TestTimers::load()
{
	mFont = new Font2D{ *mAssets->get<Font>(0) };
}

void TestTimers::postload()
{
	Log::p("Testing stopwatches");
	timer();
	x = 0;
	mTimer.splitTime();
	mTimeStretch = 60;
}

void TestTimers::update()
{
	x += getDelta();
}

void TestTimers::render()
{
	mTimeStretch -= 0.5f;

	if (mTimeStretch <= 1)
	{
		this->getEngine().setQuit(true);
	}

	mFont->spriteFont(getFPSString(), &camera().renderer(), SDL_Color{ 255, 255, 255, 255 });
	mFont->render(&camera().renderer(), 0, 0);

	const SDL_Rect r{ 0, 50, (int)x, 50 };
	SDL_SetRenderDrawColor(&camera().renderer(), 0, 0, 0, 255);
	SDL_RenderFillRect(&camera().renderer(), &r);

	SDL_Delay((ui)(1000 / mTimeStretch));
}

void TestTimers::free()
{
}
