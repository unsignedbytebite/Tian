#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestTimers : public Screen2D
{
private:
	Font2D* mFont;
	Timer mTimer;
	float x;
	float mTimeStretch;

public:
	TestTimers(EngineGFX2D* engine, AssetManager* assets = NULL) : Screen2D(engine, assets)
	{}
	~TestTimers()
	{
		delete mFont;
	};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

	// Test a stop watch
	void timer();

	// Inherited via Screen2D
	virtual void reset() override;

};


