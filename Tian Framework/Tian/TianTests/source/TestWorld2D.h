#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestWorld2D : public Screen2D
{
public:
	TestWorld2D(ScreenManager* manager, AssetManager* assets = NULL) :
		Screen2D(manager, assets)
	{}
	~TestWorld2D()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	World mWorld;
	Object2D* mPlayer;

	// Inherited via Screen2D
	virtual void reset() override;
};

