﻿#include "TestFont.h"

void TestFont::preload()
{
}

void TestFont::load()
{
}

void TestFont::postload()
{
}

void TestFont::update()
{
}

void TestFont::render()
{
	us space = 0;
	{
		Font2D font{ *mAssets->get<Font>(0) };
		font.spriteFont("The quick brown Fox jumped over the lazy Dog!", &camera().renderer(), SDL_Color{ 255,255,255,255 });
		font.render(&camera().renderer(), 0, space);
	}
	space += 40;
	{
		Font2D font{ *mAssets->get<Font>(1) };
		font.spriteFont("The quick brown Fox jumped over the lazy Dog!", &camera().renderer(), SDL_Color{ 255,0,255,255 }, 400);
		font.render(&camera().renderer(), 0, space);
	}
	space += 80;
	{
		Font2D font{ *mAssets->get<Font>(2) };
		font.spriteFont("The quick brown Fox jumped over the lazy Dog!", &camera().renderer(), SDL_Color{ 255,0,0,255 });
		font.render(&camera().renderer(), 0, space);
	}
	space += 40;
	{
		Font2D font{ *mAssets->get<Font>(3) };
		font.spriteFont(StrUni{ L'你', L'好', L'电', L'脑', L'。', '\0' }, &camera().renderer(), SDL_Color{ 255,200,150,255 });
		font.render(&camera().renderer(), 0, space);
	}
	space += 40;
	{
		Font2D font{ *mAssets->get<Font>(4) };

		font.spriteFont(StrUni{ L'你', L'是', L'我', L'的', L'中', L'文', L'老', L'师', L'吗', L'。', '\0' }, &camera().renderer(), SDL_Color{ 150,200,255,255 });
		font.render(&camera().renderer(), 0, space);
	}
}

void TestFont::free()
{
}

void TestFont::reset()
{}
