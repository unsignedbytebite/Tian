#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

struct PhysObj
{
	PhysObj(uc type) : type(type) {}
	uc type;
};

class TestBox2D : public Screen2D
{
public:
	TestBox2D(ScreenManager* manager, AssetManager* assets = NULL) :
		Screen2D(manager, assets)
	{}
	~TestBox2D()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	// Inherited via Screen2D
	virtual void reset() override;

	b2World* mPhysWorld;
	Box2DebugDraw* mDebugDraw;
	b2Body* mBody;
	b2Body* mTrigger;

	std::vector<std::shared_ptr<PhysObj>> mPhysObjs;

	int thingy{ 13 };
};

