#include "TestTexAnims.h"

void TestTexAnims::preload()
{}

void TestTexAnims::load()
{
	mPlayer = mAssets->get<TextureAnimated>(1);
}

void TestTexAnims::postload()
{}

void TestTexAnims::update()
{
	const float speed = 2.5f;

	if (getInput()->isKeydown('w'))
	{
		mPlayer->setCurrentFrameSet(0);
	}
	else if (getInput()->isKeydown('s'))
	{
		mPlayer->setCurrentFrameSet(1);
	}

	if (getInput()->isKeydown('a'))
	{
		mPlayer->setCurrentFrameSet(2);
	}
	else if (getInput()->isKeydown('d'))
	{
		mPlayer->setCurrentFrameSet(3);
	}
}

void TestTexAnims::render()
{
	mPlayer->render(&camera().renderer(), { 0, 0 });
}

void TestTexAnims::free()
{
}

void TestTexAnims::reset()
{}
