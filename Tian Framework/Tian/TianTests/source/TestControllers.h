#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestControllers : public Screen2D
{
public:
	TestControllers(ScreenManager* manager, AssetManager* assets = NULL) :
		Screen2D(manager, assets)
	{}
	~TestControllers()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	// Inherited via Screen2D
	virtual void reset() override;
};

