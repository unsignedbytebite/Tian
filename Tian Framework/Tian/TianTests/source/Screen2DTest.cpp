#include "Screen2DTest.h"

void Screen2DTest::preload()
{
}

void Screen2DTest::load()
{
	mTime = 0;
}

void Screen2DTest::postload()
{
}	

void Screen2DTest::update()
{
	mTime++;
}

void Screen2DTest::render()
{
	const us rSize = 256;
	SDL_Rect r = { 0 * rSize, 0 * rSize, rSize, rSize };
	SDL_SetRenderDrawColor(&camera().renderer(),
		255 * (uc)Maths::cosine(mTime, 0.02),
		255 * (uc)Maths::cosine(mTime, 0.01),
		255 * (uc)Maths::cosine(mTime, 0.005),
		255);
	SDL_RenderFillRect(&camera().renderer(), &r);
}

void Screen2DTest::free()
{
}

void Screen2DTest::reset()
{}
