#include "SimplexNoise2DTest.h"

using namespace std;

void SimplexNoise2DTest::preload()
{
}

void SimplexNoise2DTest::load()
{
	noise.generate();
	mResolution = 1;
	mZ = 0.013;
	mThreshold = 0.75;

	ConBox::Log::p("Controls", ConBox::Colours::LABEL);
	ConBox::Log::p("z,x = resolution");
	ConBox::Log::p("e,r = z");
	ConBox::Log::p("w,a,s,d = pan");
	ConBox::Log::p("c, v = threshold");
}

void SimplexNoise2DTest::postload()
{
}

void SimplexNoise2DTest::update()
{
	mResolution += (getInput()->isKeydown('x') * 1) + (getInput()->isKeydown('z') * -1);
	mResolution = (mResolution < 1) ? 1 : mResolution;

	mZ += ((getInput()->isKeydown('r') * 1) + (getInput()->isKeydown('e') * -1)) * 0.001;

	const float speed = 50.0;
	mLoco.move(
		getInput()->isKeydown('a') * speed - getInput()->isKeydown('d') * speed,
		getInput()->isKeydown('w') * speed - getInput()->isKeydown('s') * speed);

	const double threshDelta = 0.01;
	mThreshold += (getInput()->isKeydown('v') * threshDelta + getInput()->isKeydown('c') * -threshDelta);

	if (getInput()->isKeydown('p'))
	{
		getInput()->isKeydown('o');
	}

	if (getInput()->getKeysDown() > 0)
	{
		const string s =
			"RESO: " + to_string(mResolution) +
			"  Z: " + to_string(mZ) +
			"  THRESH: " + to_string(mThreshold);
		ConBox::Log::p(s);
	}

}

void SimplexNoise2DTest::render()
{
	const us ssize = 128 * 2;
	const us rSize = mResolution;

	for (us y = 0; y < ssize; y++)
	{
		for (us x = 0; x < ssize; x++)
		{
			double noiseLevl = noise.get(
				(mLoco.x + x) * mZ,
				(mLoco.y + y) * mZ);

			noiseLevl = (noiseLevl + 1) * 0.5; // scale 0 to 1

			//ConBox::p(toString(noiseLevl));

			SDL_Rect r = { x * rSize, y * rSize, rSize, rSize };
			SDL_SetRenderDrawColor(&camera().renderer(), 255 * 0, 255 * (uc)(noiseLevl > mThreshold), 255 * (uc)noiseLevl, 255);
			SDL_RenderFillRect(&camera().renderer(), &r);
		}
	}
}

void SimplexNoise2DTest::free()
{
}

void SimplexNoise2DTest::reset()
{}
