#include "TestSprites.h"

void TestSprites::preload()
{}

void TestSprites::load()
{
	Texture* tex = mAssets->get<Texture>(SHEET);


	const int cellW = tex->getWidth() / 4;
	const int cellH = tex->getHeight() / 4;

	FrameSets sets;
	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 0, cellW, cellH);
		set.add(cellW * 1, cellH * 0, cellW, cellH);
		set.add(cellW * 2, cellH * 0, cellW, cellH);
		set.add(cellW * 3, cellH * 0, cellW, cellH);
		sets.add(set);
	}

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 1, cellW, cellH);
		set.add(cellW * 1, cellH * 1, cellW, cellH);
		set.add(cellW * 2, cellH * 1, cellW, cellH);
		set.add(cellW * 3, cellH * 1, cellW, cellH);
		sets.add(set);
	}

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 2, cellW, cellH);
		set.add(cellW * 1, cellH * 2, cellW, cellH);
		set.add(cellW * 2, cellH * 2, cellW, cellH);
		set.add(cellW * 3, cellH * 2, cellW, cellH);
		sets.add(set);
	}

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 3, cellW, cellH);
		set.add(cellW * 1, cellH * 3, cellW, cellH);
		set.add(cellW * 2, cellH * 3, cellW, cellH);
		set.add(cellW * 3, cellH * 3, cellW, cellH);
		sets.add(set);
	}


	TextureAnimated animTex{ tex, sets };
	mPlayer = new Sprite(animTex);
	mPlayer->setCamera(&camera());
}

void TestSprites::postload()
{}

void TestSprites::update()
{
	const float speed = 2.5f;

	if (getInput()->isKeydown('w'))
	{
		mPlayer->setCurrentFrameSet(0);
		mPlayer->local().move(0, -1 * speed);
	}
	else if (getInput()->isKeydown('s'))
	{
		mPlayer->setCurrentFrameSet(1);
		mPlayer->local().move(0, 1 * speed);
	}

	if (getInput()->isKeydown('a'))
	{
		mPlayer->setCurrentFrameSet(2);
		mPlayer->local().move(-1 * speed, 0);
	}
	else if (getInput()->isKeydown('d'))
	{
		mPlayer->setCurrentFrameSet(3);
		mPlayer->local().move(1 * speed, 0);
	}
}

void TestSprites::render()
{
	mPlayer->draw();
}

void TestSprites::free()
{
	delete mPlayer;
}

void TestSprites::reset()
{}
