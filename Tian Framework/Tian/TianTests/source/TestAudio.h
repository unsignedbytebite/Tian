#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestAudioEngine 
	: public AudioEngine
{

public:
	enum AudioCues
	{
		SNAP = 0
	};

	TestAudioEngine()
	{
		load("assets/sfx/high.wav");
	}

	void cueSnap()
	{
		cue(SNAP, 0);
	}
};

class TestAudio : public Screen2D
{
public:
	TestAudio(ScreenManager* manager, AssetManager* assets = NULL) :
		Screen2D(manager, assets)
	{};
	~TestAudio()
	{	//Free the sound effects
		Mix_FreeChunk(gScratch);
		Mix_FreeChunk(gHigh);
		Mix_FreeChunk(gMedium);
		Mix_FreeChunk(gLow);
		gScratch = NULL;
		gHigh = NULL;
		gMedium = NULL;
		gLow = NULL;

		//Free the music
		Mix_FreeMusic(gMusic);
		gMusic = NULL;
	};

	virtual void preload() override;

	virtual void load() override;

	virtual void postload() override;

	virtual void update() override;

	virtual void render() override;

	virtual void free() override;

private:
	// Inherited via Screen2D
	virtual void reset() override;

	//The music that will be played
	Mix_Music *gMusic = NULL;

	TestAudioEngine mAudio;

	//The sound effects that will be used
	Mix_Chunk *gScratch = NULL;
	Mix_Chunk *gHigh = NULL;
	Mix_Chunk *gMedium = NULL;
	Mix_Chunk *gLow = NULL;
};

