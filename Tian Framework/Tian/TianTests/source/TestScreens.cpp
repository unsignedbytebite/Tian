﻿#include "TestScreens.h"

// A
void TestScreenA::preload()
{}

void TestScreenA::load()
{}

void TestScreenA::postload()
{}

void TestScreenA::update()
{}

void TestScreenA::render()
{
	Font2D font{ *mAssets->get<Font>(0) };
	font.spriteFont("Screen A", &camera().renderer(), SDL_Color{ 255, 255, 255, 255 });
	font.render(&camera().renderer(), 10, 10);

	mManager->switchScreen(1);

	if (getInput()->isKeytap('a'))
	{
		mManager->switchScreen(1);
	}
}

void TestScreenA::free()
{}

void TestScreenA::reset()
{}

// B
void TestScreenB::preload()
{}

void TestScreenB::load()
{}

void TestScreenB::postload()
{}

void TestScreenB::update()
{}

void TestScreenB::render()
{
	Font2D font{ *mAssets->get<Font>(0) };
	font.spriteFont("Screen B", &camera().renderer(), SDL_Color{ 255, 255, 255, 255 });
	font.render(&camera().renderer(), 10, 10);

	mManager->switchScreen(2);

	if (getInput()->isKeytap('a'))
	{
		mManager->switchScreen(2);
	}
}

void TestScreenB::free()
{}

void TestScreenB::reset()
{}

// C
void TestScreenC::preload()
{}

void TestScreenC::load()
{}

void TestScreenC::postload()
{}

void TestScreenC::update()
{}

void TestScreenC::render()
{
	Font2D font{ *mAssets->get<Font>(0) };
	font.spriteFont("Screen C", &camera().renderer(), SDL_Color{ 255, 255, 255, 255 });
	font.render(&camera().renderer(), 10, 10);

	mManager->close();

	if (getInput()->isKeytap('a'))
	{
		mManager->switchScreen(0);
	}
}

void TestScreenC::free()
{}

void TestScreenC::reset()
{}
