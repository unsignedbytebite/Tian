#pragma once

#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestFont : public Screen2D
{
public:
	TestFont(EngineGFX2D* engine, AssetManager* assets = NULL) : Screen2D(engine, assets)
	{}
	~TestFont()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	Texture* tex;
	FrameSets sets;

	// Inherited via Screen2D
	virtual void reset() override;
};



