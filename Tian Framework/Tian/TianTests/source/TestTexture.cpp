#include "TestTexture.h"

void TestTexture::preload()
{
}

void TestTexture::load()
{
	tex = mAssets->get<Texture>(0);

	const int cellW = tex->getWidth() / 4;
	const int cellH = tex->getHeight() / 1;

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, 0, cellW, cellH);
		set.add(cellW * 1, 0, cellW, cellH);
		set.add(cellW * 2, 0, cellW, cellH);
		set.add(cellW * 3, 0, cellW, cellH);
		sets.add(set);
	}

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 1, cellW, cellH);
		set.add(cellW * 1, cellH * 1, cellW, cellH);
		set.add(cellW * 2, cellH * 1, cellW, cellH);
		set.add(cellW * 3, cellH * 1, cellW, cellH);
		sets.add(set);
	}

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 2, cellW, cellH);
		set.add(cellW * 1, cellH * 2, cellW, cellH);
		set.add(cellW * 2, cellH * 2, cellW, cellH);
		set.add(cellW * 3, cellH * 2, cellW, cellH);
		sets.add(set);
	}

	{
		FrameSet set{ 8 };
		set.add(cellW * 0, cellH * 3, cellW, cellH);
		set.add(cellW * 1, cellH * 3, cellW, cellH);
		set.add(cellW * 2, cellH * 3, cellW, cellH);
		set.add(cellW * 3, cellH * 3, cellW, cellH);
		sets.add(set);
	}

	mRenderTarget = new Texture();
	mRenderTarget->create(128, 128, &camera().renderer());

}

void TestTexture::postload()
{
}

void TestTexture::update()
{
}

void TestTexture::render()
{
	// Reference
	tex->render(&camera().renderer(), 0, 0);

	// Animated
	for (us i = 0; i < sets.size(); ++i)
	{
		sets.at(i).step();
		tex->render(&camera().renderer(), tex->getWidth() + 8, i * sets.at(i).current()->h, sets.at(i).current());
	}

	// Renderer texture
	mRenderTarget->clear(&camera().renderer());
	mRenderTarget->startTarget(&camera().renderer());
	tex->render(&camera().renderer(), 0, 0); // Capture
	mRenderTarget->stopTarget(&camera().renderer());
	mRenderTarget->render(&camera().renderer(), SDL_Rect{ 300,0,256,256 });

}

void TestTexture::free()
{
}

void TestTexture::reset()
{}
