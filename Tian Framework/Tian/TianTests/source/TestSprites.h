#pragma once

#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestSprites : public Screen2D
{
public:
	enum Assets
	{
		SHEET = 0, WALK_UP, WALK_DOWN, WALK_LEFT, WALK_RIGHT, SIZE
	};

	TestSprites(EngineGFX2D* engine, AssetManager* assets = NULL) : Screen2D(engine, assets)
	{}
	~TestSprites()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	Sprite* mPlayer;

	// Inherited via Screen2D
	virtual void reset() override;
};

