#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class Screen2DTest : public Screen2D
{
public:
	Screen2DTest(EngineGFX2D* engine) : Screen2D(engine) {};
	~Screen2DTest() {};

	virtual void preload() override;

	virtual void load() override;

	virtual void postload() override;

	virtual void update() override;

	virtual void render() override;

	virtual void free() override;

private:
	us mTime;

	// Inherited via Screen2D
	virtual void reset() override;
};

