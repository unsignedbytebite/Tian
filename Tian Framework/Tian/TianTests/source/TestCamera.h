#pragma once

#include "../../Tian/source/Tian.h"

using namespace Tian;

class TestCamera : public Screen2D
{
public:
	enum Assets
	{
		SHEET = 0, WALK_RIGHT, SIZE
	};

	TestCamera(EngineGFX2D* engine, AssetManager* assets = NULL) : Screen2D(engine, assets), mCam{ *engine->getRenderer(), 256, 256 }
	{

	}
	~TestCamera()
	{};

	// Inherited via Screen2D
	virtual void preload() override;
	virtual void load() override;
	virtual void postload() override;
	virtual void update() override;
	virtual void render() override;
	virtual void free() override;

private:
	Sprite* mPlayer;
	Camera2D mCam;

	// Inherited via Screen2D
	virtual void reset() override;
};

