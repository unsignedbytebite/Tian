#pragma once
#include "Tests.h"

#include "source/Screen2DTest.h"
#include "source/SimplexNoise2DTest.h"
#include "source/TestWorld2D.h"
#include "source/TestTimers.h"
#include "source/TestTexture.h"
#include "source/TestFont.h"
#include "source/TestSprites.h"
#include "source/TestCamera.h"
#include "source/TestScreens.h"
#include "source/TestSprites.h"
#include "source/TestTexAnims.h"
#include "source/TestBox2D.h"
#include "source/TestCanvas2D.h"
#include "source/TestTimers.h"
#include "source/TestAudio.h"
#include "source/TestControllers.h"

using namespace Tian;
using namespace ConBox;

void Test_Screen(const bool kill)
{
	const char* name = __func__;
	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);
	engine->execute(new Screen2DTest(engine));
	delete engine;
}

void Test_SimplexNoise(const bool kill)
{
	const char* name = __func__;
	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);
	engine->execute(new SimplexNoise2DTest(engine));
	delete engine;
}

void Test_Timers(const bool kill)
{
	const char* name = __func__;
	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);
	AssetManager assets1;
	assets1.add(new Font{ "assets\\comic.ttf", 10 });
	engine->execute(new TestTimers(engine, &assets1));
	delete engine;
}

void Test_Textures(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1;
	assets1.add(new Texture{ "assets\\DoomCharMarine.png", engine->getRenderer() });

	engine->execute(new TestTexture(engine, &assets1));
	delete engine;
}
void Test_Fonts(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1;
	assets1.add(new Font{ "assets\\Copy0955.ttf", 24 });
	assets1.add(new Font{ "assets\\comic.ttf", 24 });
	assets1.add(new Font{ "assets\\PARCHM.TTF", 24 });
	assets1.add(new Font{ "assets\\Fang zheng Xi yuan Font-Traditional Chinese.ttf", 24 });
	assets1.add(new Font{ "assets\\simhei.ttf", 24 });

	engine->execute(new TestFont(engine, &assets1));
	delete engine;
}
void Test_Sprites(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1{ TestSprites::Assets::SIZE };
	assets1.add(new Texture{ "assets\\DoomCharMarine.png", engine->getRenderer() }, TestSprites::Assets::SHEET);

	engine->execute(new TestSprites(engine, &assets1));
	delete engine;
}
void Test_Camera(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1{ TestSprites::Assets::SIZE };
	Texture* tex = new Texture{ "assets\\hero.png", engine->getRenderer() };
	assets1.add(tex, TestSprites::Assets::SHEET);

	engine->execute(new TestCamera(engine, &assets1));
	delete engine;
}
void Test_Screens(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1;
	assets1.add(new Font{ "assets\\comic.ttf", 24 });

	ScreenManager sm{ *engine };
	sm.add(new TestScreenA(&sm, &assets1));
	sm.add(new TestScreenB(&sm, &assets1));
	sm.add(new TestScreenC(&sm, &assets1));
	sm.remove(1);
	sm.switchScreen(0);
	delete engine;
}
void Test_Buffer()
{
	const char* name = __func__;

	Buffer<int> buffer{ { 13, 99, -20 } };
	int r[5]{ 0 };
	r[0] = buffer.get();
	r[1] = ++buffer;
	r[2] = ++buffer;
	r[3] = ++buffer;
	r[4] = --buffer;

	const bool correct = (r[0] + r[1] + r[2] + r[3] + r[4]) == (13 + 99 + -20 + 13 + -20);

	if (!correct)
	{
		throw ConBoxError(name);
	}
}
void Test_Box()
{
	const char* name = __func__;

	// Test for intersects
	{
		bool correct = true;
		Vector2 posA{ -4, -4 };
		Box boxA{ posA, { 0, 0, 5, 5 } };
		Box boxB{ { 0, 0, 10, 10 } };

		for (uc x = 0; x < 15; x++)
		{
			for (uc y = 0; y < 15; y++)
			{
				const bool intersects = boxB.intersects(boxA);
				correct &= intersects;

				if (!intersects)
				{
					throw ConBoxError("boxA fails to intersect: ", name);
				}

				posA.move(0, 1);
			}

			posA.move(0, -15);
			posA.move(1, 0);
		}
	}
}
void Test_FileIO()
{
	const char* name = __func__;
	// Get string
	{
		string line = FileIO::read("assets\\test01.txt");
		if (line != "test\r\nline 2\r\nline \"three\"")
		{
			throw ConBoxError("Read file incorrect", name);
		}
	}

	// Save file
	{
		FileIO file{ "assets\\test03.bin", FileIO::WRITE };

		file.start();
		file.write<us>(320);
		file.write<us>(240);
		file.write<ui>(40056);
		file.write<int>(-666);
		file.write<float>(9000.56543f);
		file.write<string>("abcdefgh");
		file.end();
	}

	// Load binary
	{
		FileIO file{ "assets\\test03.bin" };

		file.start();
		const us a = file.read<us>();
		const us b = file.read<us>();
		const ui c = file.read<ui>();
		const int d = file.read<int>();
		const float e = file.read<float>();
		const string s = file.read<string>();
		file.end();

		if ((double)(a + b + c + d + e) != (double)(320 + 240 + 40056 + -666 + 9000.56543f) ||
			s != "abcdefgh")
		{
			throw ConBoxError("Binary data incorrect", name);
		}
	}
}
void Test_AnimatedTexture(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	{
		// This texture will be deleted by the TexAnim 
		auto* tex = new Texture{ "assets\\DoomCharMarine.png", engine->getRenderer() };
		const auto cellW = tex->getWidth() / 4;
		const auto cellH = tex->getHeight() / 4;

		FrameSets sets;
		{
			FrameSet set{ 8 };
			set.add(cellW * 0, cellH * 0, cellW, cellH);
			set.add(cellW * 1, cellH * 0, cellW, cellH);
			set.add(cellW * 2, cellH * 0, cellW, cellH);
			set.add(cellW * 3, cellH * 0, cellW, cellH);
			sets.add(set);
		}

		{
			FrameSet set{ 8 };
			set.add(cellW * 0, cellH * 1, cellW, cellH);
			set.add(cellW * 1, cellH * 1, cellW, cellH);
			set.add(cellW * 2, cellH * 1, cellW, cellH);
			set.add(cellW * 3, cellH * 1, cellW, cellH);
			sets.add(set);
		}

		{
			FrameSet set{ 8 };
			set.add(cellW * 0, cellH * 2, cellW, cellH);
			set.add(cellW * 1, cellH * 2, cellW, cellH);
			set.add(cellW * 2, cellH * 2, cellW, cellH);
			set.add(cellW * 3, cellH * 2, cellW, cellH);
			sets.add(set);
		}

		{
			FrameSet set{ 8 };
			set.add(cellW * 0, cellH * 3, cellW, cellH);
			set.add(cellW * 1, cellH * 3, cellW, cellH);
			set.add(cellW * 2, cellH * 3, cellW, cellH);
			set.add(cellW * 3, cellH * 3, cellW, cellH);
			sets.add(set);
		}

		// Save out the set
		{
			FileIO file{ "assets\\marine.frameset", FileIO::WRITE };
			file.start();
			sets.serializeOut(file);
			file.end();
		}

		// Read in the set
		{
			FileIO file{ "assets\\marine.frameset", FileIO::READ };
			file.start();
			FrameSets loadedSets;
			loadedSets.serializeIn(file);
			file.end();
		}

		// Save out the animated texture
		{
			// Tex will now be handled by the texAnim and released when texAnim is destroyed
			TextureAnimated texAnim{ tex, sets };

			FileIO file{ "assets\\marine.texanim", FileIO::WRITE };
			file.start();
			texAnim.serializeOut(file);
			file.end();
		}
	}

	AssetManager assets{ TestSprites::Assets::SIZE };
	assets.add(new TextureAnimated{ "assets\\marine.texanim", engine->getRenderer() }, 1);

	ScreenManager sm{ *engine };
	sm.add(new TestTexAnims(&sm, &assets));
	sm.switchScreen(0);
	delete engine;
}
void Test_World2D(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1;
	//assets1.add(new Font{ "assets\\comic.ttf", 24 });

	ScreenManager sm{ *engine };
	sm.add(new TestWorld2D(&sm, &assets1));
	sm.switchScreen(0);
	delete engine;
}
void Test_Box2D(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 480, name);
	engine->setQuit(kill);

	AssetManager assets1;
	//assets1.add(new Font{ "assets\\comic.ttf", 24 });

	ScreenManager sm{ *engine };
	sm.add(new TestBox2D(&sm, &assets1));
	sm.switchScreen(0);
	delete engine;
}
void Test_Canvas2D(const bool kill)
{
	const char* name = __func__;

	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 480, name);
	engine->setQuit(kill);

	AssetManager assets1;
	assets1.add(new Texture{ "assets\\tile.png", engine->getRenderer() });

	ScreenManager sm{ *engine };
	sm.add(new TestCanvas2D(&sm, &assets1));
	sm.switchScreen(0);
	delete engine;
}

void Test_EventSequence()
{
	EventSequence seq;
	seq.createEvent(new EventMsg(2000, 2000, "msg1"));
	seq.createEvent(new EventMsg(3000, 3000, "msg2"));
	seq.createEvent(new EventMsg(1000, 1000, "msg3"));
	seq.createEvent(new EventMsg(1000, 5000, "complete"));

	while (!seq.isFinished())
	{
		seq.update();
	}

	int j = 0;
	j++;
}

void Test_Audio(const bool kill)
{
	const char* name = __func__;
	EngineGFX2D* engine = new EngineGFX2D();
	engine->ini(640, 320, name);
	engine->setQuit(kill);

	AssetManager assets1;
	ScreenManager sm{ *engine };
	sm.add(new TestAudio(&sm, &assets1));
	sm.switchScreen(0);
	delete engine;
}

void Test_TearDown()
{
	for (auto i = 0u; i < 50; ++i)
	{
		Test_AnimatedTexture(true);
	}
}

void Test_Controller(const bool kill)
{
	auto engine{ std::make_unique<EngineGFX2D>() };
	engine->ini(640, 480, "Test_Controller");
	engine->setQuit(kill);

	ScreenManager sm{ *engine };
	sm.add(new TestControllers(&sm));
	sm.switchScreen(0);
}

void Tests::run()
{
	Test_Screen(true);
	Test_Screens(true);
	Test_Camera(true);
	Test_FileIO();
	Test_Timers(true);
	Test_Buffer();
	Test_EventSequence();
	Test_Box();
	Test_SimplexNoise(true);
	Test_Textures(true);
	Test_Sprites(true);
	Test_Fonts(true);
	Test_AnimatedTexture(true);
	Test_World2D(true);
	Test_Audio(true);
	Test_TearDown();
	Test_Controller(true);
}