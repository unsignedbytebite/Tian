-- Premake for Tian

-- Create Solution
workspace "Tian_Framework"
	filename "Tian"
	configurations { "Debug", "Release" }
	platforms { "x86", "x64"}
	startproject "TianTests"
   
filter { "platforms:Win32" }
    system "Windows"
    architecture "x32"

filter { "platforms:x64" }
    system "Windows"
    architecture "x64"
 
-- Create Tian Framework project 
local minzgi = "Tian"
project (minzgi)
	location (minzgi)
	kind "StaticLib"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	warnings "Extra"
	includedirs { "libs/Include",  (minzgi).."/source"}
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp" }

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"  
		
-- Create Tian Packer project  
local minzgi = "TianPacker"
project (minzgi)
	location (minzgi)
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	debugdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	includedirs { "libs/Include" }
	links { "Tian" }
	warnings "Extra"
	links { "SDL2main.lib", "SDL2_image.lib", "SDL2_mixer.lib", "SDL2_ttf.lib", "SDL2.lib" }
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp" }
	postbuildcommands {
		"{COPY} ../libs/dlls/%{cfg.platform} %{cfg.targetdir}"
	}	

	filter { "platforms:x64" }
		libdirs  { "libs/x64" }
		
	filter { "platforms:x86" }
		libdirs  { "libs/x86" }
   
	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"
   
-- Create tests project   
local minzgi = "TianTests"
project (minzgi)
	location (minzgi)
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
   	debugdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	includedirs { "libs/Include" }
	links { "Tian" }
	warnings "Extra"
	links { "SDL2main.lib", "SDL2_image.lib", "SDL2_mixer.lib", "SDL2_ttf.lib", "SDL2.lib" }
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp" }
	postbuildcommands {
		"{COPY} ../libs/dlls/%{cfg.platform} %{cfg.targetdir}",
		"{COPY} assets %{cfg.targetdir}/assets"
	}	

	filter { "platforms:x64" }
		libdirs  { "libs/x64" }
		
	filter { "platforms:x86" }
		libdirs  { "libs/x86" }
   
	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

   filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"
	  
-- Create Mandlebrot demo 
local minzgi = "DemoMandlebrot"
project (minzgi)
	location (minzgi)
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	debugdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	includedirs { "libs/Include" }
	links { "Tian" }
	warnings "Extra"
	links { "SDL2main.lib", "SDL2_image.lib", "SDL2_mixer.lib", "SDL2_ttf.lib", "SDL2.lib" }
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp" }
	postbuildcommands {
		"{COPY} ../libs/dlls/%{cfg.platform} %{cfg.targetdir}"
	}	

	filter { "platforms:x64" }
		libdirs  { "libs/x64" }
		
	filter { "platforms:x86" }
		libdirs  { "libs/x86" }
   
	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"
	  
-- Project Template
local minzgi = "ProjectTemplate"
project (minzgi)
	location (minzgi)
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	debugdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	includedirs { "libs/Include" }
	links { "Tian", "TianPacker" }
	warnings "Extra"
	links { "SDL2main.lib", "SDL2_image.lib", "SDL2_mixer.lib", "SDL2_ttf.lib", "SDL2.lib" }
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp", (minzgi) .. "/**.structure", (minzgi) .. "/**.values",  (minzgi) .. "/**.bat" }
	postbuildcommands {
		"{COPY} ../libs/dlls/%{cfg.platform} %{cfg.targetdir}",
		"{COPY} assets %{cfg.targetdir}/assets",
		"{COPY} scripts %{cfg.targetdir}/scripts",
	}	

	filter { "platforms:x64" }
		libdirs  { "libs/x64" }
		
	filter { "platforms:x86" }
		libdirs  { "libs/x86" }
   
	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"		  
	  
-- Create Shooter demo  
local minzgi = "DemoShooter"
project (minzgi)
	location (minzgi)
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	debugdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	includedirs { "libs/Include" }
	links { "Tian", "TianPacker" }
	warnings "Extra"
	links { "SDL2main.lib", "SDL2_image.lib", "SDL2_mixer.lib", "SDL2_ttf.lib", "SDL2.lib" }
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp", (minzgi) .. "/**.structure", (minzgi) .. "/**.values",  (minzgi) .. "/**.bat" }
	postbuildcommands {
		"{COPY} ../libs/dlls/%{cfg.platform} %{cfg.targetdir}",
		"{COPY} assets %{cfg.targetdir}/assets",
		"{COPY} scripts %{cfg.targetdir}/scripts",
	}	

	filter { "platforms:x64" }
		libdirs  { "libs/x64" }
		
	filter { "platforms:x86" }
		libdirs  { "libs/x86" }
   
	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"	

-- Create Zelda demo  
local minzgi = "DemoZelda"
project (minzgi)
	location (minzgi)
	kind "ConsoleApp"
	language "C++"
	targetdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	debugdir "build/%{cfg.platform}/%{cfg.buildcfg}"
	includedirs { "libs/Include" }
	links { "Tian", "TianPacker" }
	warnings "Extra"
	links { "SDL2main.lib", "SDL2_image.lib", "SDL2_mixer.lib", "SDL2_ttf.lib", "SDL2.lib" }
	files { (minzgi) .. "/**.h", (minzgi) .. "/**.cpp", (minzgi) .. "/**.structure", (minzgi) .. "/**.values",  (minzgi) .. "/**.bat" }
	postbuildcommands {
		"{COPY} ../libs/dlls/%{cfg.platform} %{cfg.targetdir}",
		"{COPY} assets %{cfg.targetdir}/assets",
		"{COPY} scripts %{cfg.targetdir}/scripts",
	}	

	filter { "platforms:x64" }
		libdirs  { "libs/x64" }
		
	filter { "platforms:x86" }
		libdirs  { "libs/x86" }
   
	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"	  		
	  
