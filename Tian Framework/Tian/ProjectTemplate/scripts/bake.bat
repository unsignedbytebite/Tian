@echo off

echo [ TianPacker ]
TianPacker.exe -p scripts\engine.structure scripts\engine.values assets\engine
TianPacker.exe -p scripts\spriteSheet.structure scripts\spriteSheet.values assets\spriteSheet.gfx
echo [ Bake Complete! ]

