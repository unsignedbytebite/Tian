#include "ScreenGame.h"
#include "Engine.h"

namespace ProjectTemplate
{
	ScreenGame::ScreenGame(ScreenManager* manager, AssetManager* assets) : 
		Screen2D(manager, assets)
	{}
	void ScreenGame::load()
	{
		// Create an additonal camera cloned on the current camera
		createCamera(new Camera2D{ camera() });

		// This main camera is for the world graphics and set to be double size
		camera(WORLD).setScale(2);

		// Reset the scores to zero
		Engine::gPlayerAScore = Engine::gPlayerBScore = 0;

		// Create the world
		mWorld = shared_ptr<GameWorld>(new GameWorld{ camera(WORLD), *getInput() });

		// Create the sprite font for the score
		mFontScore = &addObject(new SpriteFont{ *mAssets, 0 });

		// Assign the camera to the spritefont
		mFontScore->setCamera(&camera(WORLD));

#ifdef DEV_MODE
		// Set the debug renderer
		mWorld->createDebugRenderer(new DebugDraw(camera(DEBUGCAM)));
#endif
	}
	void ScreenGame::update()
	{
		// Escape to the main menu
		if (getInput()->isKeytap(SDLK_ESCAPE))
		{
			// Switch to main menu at index 0
			mManager->switchScreen(0, true);
		}

		// Update the world
		mWorld->update();
	}
	void ScreenGame::reset()
	{
		camera().setLocal({ 0, 0 });
		mObjs.clear();
	}
	void ScreenGame::render()
	{
		// Use the world camera to capture the drawing
		camera(WORLD).expose();

		// Draw the world
		mWorld->draw();

		// Draw the score labels
		const float margin = 16.f;
		mFontScore->setString(to_string(Engine::gPlayerBScore), { margin, (float)Engine::gGameBounds.centreY() - 2 - 12}, Colours::OrangeRed);
		mFontScore->draw();

		mFontScore->setString(to_string(Engine::gPlayerAScore), { margin, (float)Engine::gGameBounds.centreY() + 2}, Colours::CornflowerBlue);
		mFontScore->draw();

		// End the exposure of the camera and render the results
		camera(WORLD).present();

#ifdef DEV_MODE
		// Draw debug graphics
		camera(DEBUGCAM).expose();
		mWorld->drawDebug();
		camera(DEBUGCAM).present();
#endif
	}
	void ScreenGame::free()
	{}
}
