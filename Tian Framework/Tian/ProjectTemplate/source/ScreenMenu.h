#pragma once
#include "Engine.h"

namespace ProjectTemplate
{
	class ScreenMenu : public Screen2D
	{
	public:
		ScreenMenu(ScreenManager* manager, AssetManager* assets = NULL);

		// Inherited via Screen2D
		virtual void preload() override;
		virtual void load() override;
		virtual void postload() override;
		virtual void update() override;
		virtual void free() override;
		virtual void reset() override;
	private:
		// The types of the labels
		enum Labels { OPTION1 = 0, OPTION2, OPTION3, PICKER, MENU , COUNT };

		// The selection buffer
		Buffer<uc> mSelection;

		// List of pointers to spritefonts in the cache
		vector<SpriteFont*> mFonts;

		// Font layout parameters
		const float mTop = 10;
		const float mLeft = 10;
		const float mGap = 25;
		const float mMargin = 10;

		// Run navigation logic for screen menu selection
		void navigationControl();

		// Colour picker logic
		void pickerColour();
	};
}

