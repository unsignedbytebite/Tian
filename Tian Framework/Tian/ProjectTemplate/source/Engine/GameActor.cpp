#include "GameActor.h"

namespace ProjectTemplate
{
	GameActor::GameActor(const Engine::GameTypes gameType, const Engine::ActorPosition position, Camera2D& cam, const TextureAnimated texture) :
		Actor((us)gameType, texture)
	{
		mCam = &cam; // TODO: pass cam up

		// Set the actor's location and colour based on its position
		switch (position)
		{
		case Engine::ActorPosition::TOP:
			// Set the starting location
			mPos = { (float)Engine::gGameBounds.centreX(), (float)(Engine::gGameBounds.bottom() + 8) };

			// Rotate the sprite
			mAngle = 180;

			// The sprite's rotation pivot point
			mPivot = { 19 / 2, 19 / 2 };

			// Set the sprite's colour
			mColour = Colours::OrangeRed;
			break;
		case Engine::ActorPosition::BOTTOM:
			// Set the starting location
			mPos = { (float)(Engine::gGameBounds.centreX()), (float)(Engine::gGameBounds.top() - 8) };

			// Set the sprite's colour
			mColour = Colours::CornflowerBlue;
			break;
		case Engine::ActorPosition::MIDDLE:
			// Set the starting location
			mPos = { (float)(Engine::gGameBounds.centreX()), (float)(Engine::gGameBounds.centreY()) };

			// Set the sprite's colour
			mColour = Colours::White;
			break;
		}
	}
	void GameActor::drawInWorld(const float worldScale)
	{
		// Set the texture colour
		Sprite::updateTextureColour();

		// Draw the textured sprite in the world
		Sprite::drawInWorld(worldScale);
	}
	void GameActor::move(const Vector2& dir)
	{
		// Awake the body
		mBody->SetAwake(true);

		// Apply movment
		mBody->ApplyLinearImpulseToCenter({ dir.x, dir.y }, false);
	}

	void GameActor::update()
	{
		Actor::processContacts();
	}
}
