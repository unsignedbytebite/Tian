#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

namespace ProjectTemplate
{
	class GameAudio :
		public AudioEngine
	{
	public:
		GameAudio();

		// Cue the sound of the wall being hit
		void cueWallHit();

		// Cue the sound of the ball hitting the paddle
		void cuePaddleHit();

	protected:
		// The types of cues
		enum CueTypes
		{
			CUE_WALL = 0, CUE_PADDLE
		};

		// The channles for the cues
		enum CueChannels
		{
			CHANNEL_WALL = 0, CHANNEL_PADDLE
		};
	};
}

