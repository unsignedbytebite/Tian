#pragma once
#include "../Engine.h"
#include "Ball.h"

namespace ProjectTemplate
{
	class GameWorld :
		public World2D
	{
	public:
		GameWorld(Camera2D& camera, Input& inputController);

		// Update the game world logic
		virtual void update();
	protected:
		// Reference to the ball to be used by paddles
		Ball& mBall;
	};
}

