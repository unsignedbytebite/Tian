#include "Paddle.h"

namespace ProjectTemplate
{
	Paddle::Paddle(const Engine::ActorPosition position, Camera2D& cam) :
		GameActor(Engine::GameTypes::TYPE_PADDLE, position, cam, Engine::get->getAsset<TextureAnimated>(ProjectTemplate::GFXSheet))
	{
		mTex.setCurrentFrameSet(Engine::SpriteSheetSet::PADDLE);
	}
	void Paddle::created()
	{
		// Set the body physics parameters
		body()->SetType(b2_dynamicBody);
		body()->SetTransform(mPos, 0);
		body()->SetLinearDamping(0.9f);
		body()->SetFixedRotation(true);

		// Create the body of the paddle
		b2CircleShape shape;
		shape.m_radius = 10.0f;

		// Create the fixture definition for the body
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.05f;
		fixtureDef.restitution = 1.0f;
		fixtureDef.friction = 0.01f;
		body()->CreateFixture(&fixtureDef);

		// Set the texture offset
		setLocal({ -9, -9 });
	}
	void Paddle::update()
	{
		// Clamp the paddle to the Y pos so the ball cannot knock it away
		body()->SetTransform({ body()->GetPosition().x, mPos.y }, 0);
	}
	void Paddle::move(const MoveDirection direction)
	{
		// Move the paddle by a velocity
		static const auto vel = 50.5f;
		GameActor::move({ (direction == LEFT) ? -vel : vel, 0 });
	}
}
