#include "Ball.h"
#include "Paddle.h"

namespace ProjectTemplate
{
	Ball::Ball(Camera2D& cam) : GameActor(Engine::GameTypes::TYPE_BALL, Engine::ActorPosition::MIDDLE, cam, Engine::get->getAsset<TextureAnimated>(ProjectTemplate::GFXSheet))
	{
		mTex.setCurrentFrameSet(Engine::SpriteSheetSet::BALL);
	}
	void Ball::created()
	{
		body()->SetType(b2_dynamicBody);

		// Set the ball body shape
		b2CircleShape shape;
		shape.m_radius = 1.0f;

		// Set the ball's physics parameters
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.01f;
		fixtureDef.restitution = 0.8f;
		fixtureDef.friction = 0.0001f;
		body()->CreateFixture(&fixtureDef);

		// Set the ball in motion
		reset();
	}

	void Ball::reset(const bool down)
	{
		// Reset the ball and apply impluse
		const float vel = 3.5;
		body()->SetLinearVelocity({ 0, 0 });
		body()->SetTransform(mPos, 0);
		body()->ApplyLinearImpulseToCenter({ 0, (down) ? -vel : vel }, true);
	}

	void Ball::contactEvent(Object2D* objA, Object2D* objB)
	{
		// Check for collided object type
		if (objB->getID() == Engine::GameTypes::TYPE_PADDLE)
		{
			Engine::gAudio.cuePaddleHit();
		}
		else if (objA->getID() == Engine::GameTypes::TYPE_WALL)
		{
			Engine::gAudio.cueWallHit();
		}
	}
}
