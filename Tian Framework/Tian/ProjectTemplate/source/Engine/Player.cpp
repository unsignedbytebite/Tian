#include "Player.h"
#include "../Engine.h"

namespace ProjectTemplate
{
	Player::Player(const Engine::ActorPosition  position, Input& input, Camera2D& cam) :
		Paddle(position, cam), mInput(input)
	{}
	void Player::update()
	{
		// Check for the keys to move the paddle
		if (mInput.isKeydown(KEY_LEFT))
		{
			move(LEFT);
		}
		else if (mInput.isKeydown(KEY_RIGHT))
		{
			move(RIGHT);
		}

		Paddle::update();
	}
}
