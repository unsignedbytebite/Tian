#include "CPU.h"
#include "../Engine.h"

namespace ProjectTemplate
{
	CPU::CPU(Ball& ball, const Engine::ActorPosition position, Camera2D& cam) :
		Paddle(position, cam),
		mBall{ ball }
	{}

	void CPU::update()
	{
		// If the ball is left then move left
		if (mBall.getBody().GetPosition().x <= body()->GetPosition().x - 10)
		{
			Paddle::move(LEFT);
		}
		// If the ball is right then move left
		else if (mBall.getBody().GetPosition().x >= body()->GetPosition().x + 10)
		{
			Paddle::move(RIGHT);
		}

		Paddle::update();
	}
}
