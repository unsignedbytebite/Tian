#pragma once
#include "Paddle.h"
#include "Ball.h"

namespace ProjectTemplate
{
	class CPU :
		public Paddle
	{
	public:
		CPU(Ball& ball, const Engine::ActorPosition position, Camera2D& cam);

		// Inherited functions
		void update() override;
	private:
		// Reference to the game's ball so AI can track
		Ball& mBall;
	};
}

