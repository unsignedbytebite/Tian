#pragma once
#include "../Engine.h"

namespace ProjectTemplate
{
	class GameActor :
		public Actor
	{
	public:
		GameActor(const Engine::GameTypes gameType, const Engine::ActorPosition  position, Camera2D& cam, const TextureAnimated texture);

		// Draw the actor in the world
		virtual void drawInWorld(const float worldScale) override;

		// Move the actor on vector
		virtual void move(const Vector2& dir) override;

		virtual void update() override;
	protected:
		// The starting postion of the actor
		Vector2 mPos;
	};
}

