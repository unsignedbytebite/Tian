#pragma once
#include "Paddle.h"

namespace ProjectTemplate
{
	class Player :
		public Paddle
	{
	public:
		Player(const Engine::ActorPosition position, Input& input, Camera2D& cam);

		// Inherited functions
		void update() override;
	private:
		// Input controller
		Input& mInput;
	};
}

