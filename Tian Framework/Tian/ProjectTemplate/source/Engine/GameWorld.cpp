#include "GameWorld.h"
#include "../Engine.h"
#include "Player.h"
#include "CPU.h"

namespace ProjectTemplate
{
	GameWorld::GameWorld(Camera2D& camera, Input& inputController) :
		World2D(camera),
		// Create and assign the ball
		mBall{ (Ball&)createObject(new Ball{ mCam }) }
	{
		const float margin = 8;

		// Create paddles
		createObject(new Player{ Engine::ActorPosition::BOTTOM, inputController, mCam });
		createObject(new CPU{ mBall, Engine::ActorPosition::TOP, mCam });

		// Create side walls
		createStatic(Engine::GameTypes::TYPE_WALL, 0.f, 0.f, margin, (float)Engine::gGameBounds.top());
		createStatic(Engine::GameTypes::TYPE_WALL, (float)Engine::gGameBounds.right(), 0.f, margin, (float)Engine::gGameBounds.top());

		// Paint canvas textures
		mCanvas = std::unique_ptr<Canvas>(new Canvas{ &mCam, (us)Engine::gGameBounds.right(), (us)Engine::gGameBounds.top(), 2, 2 });
		mCanvas->fillWithCanvasTexture();

		auto& wallTexture = Engine::get->getAsset<TextureAnimated>(ProjectTemplate::GFXSheet);
		wallTexture.setCurrentFrameSet(Engine::SpriteSheetSet::WALL);
		wallTexture.textureData()->setColour(Colours::DarkGray);

		// Left wall
		Vector2 loco;
		for (us y = 0; y < Engine::gGameBounds.top(); y += 8)
		{
			loco = Vector2{ 0, (float)y };
			mCanvas->drawTexture(wallTexture, loco);
		}

		// Right wall
		for (us y = 0; y < Engine::gGameBounds.top(); y += 8)
		{
			loco = Vector2{ (float)Engine::gGameBounds.right() - 8, (float)y };
			mCanvas->drawTexture(wallTexture, loco);
		}
	}

	void GameWorld::update()
	{
		const Vector2& ballPositon = {
			mBall.body()->GetPosition().x,
			mBall.body()->GetPosition().y
		};

		// Check for ball out of bounds
		if (ballPositon.getY() >= Engine::gGameBounds.top())
		{
			++Engine::gPlayerBScore;
			mBall.reset(false);
		}
		else if (ballPositon.getY() <= 0)
		{
			++Engine::gPlayerAScore;
			mBall.reset();
		}

		World2D::update();
	}
}
