#include "GameAudio.h"

namespace ProjectTemplate
{
	GameAudio::GameAudio() 
		: AudioEngine()
	{
		// Load cues
		load("assets/high.wav");
		load("assets/low.wav");
	}
	void GameAudio::cueWallHit()
	{
		cueOnce(CueTypes::CUE_WALL, CueChannels::CHANNEL_WALL);
	}
	void GameAudio::cuePaddleHit()
	{
		cueOnce(CueTypes::CUE_PADDLE, CueChannels::CHANNEL_PADDLE);
	}
}
