#pragma once
#include "../Engine.h"
#include "GameActor.h"

namespace ProjectTemplate
{
	class Ball : public GameActor
	{
	public:
		Ball(Camera2D& cam);

		// Called when created
		virtual void created() override;

		// Rest the ball with direction of tavel
		void reset(const bool down = true);

		// A contact event between two objects
		virtual void contactEvent(Object2D* objA, Object2D* objB);
	};
}

