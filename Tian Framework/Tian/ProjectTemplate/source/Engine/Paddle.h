#pragma once
#include "../Engine.h"
#include "GameActor.h"

namespace ProjectTemplate
{
	class Paddle :
		public GameActor
	{
	protected:
		// The direction to move the paddle
		enum MoveDirection
		{
			LEFT, RIGHT
		};

	public:
		Paddle(const Engine::ActorPosition position, Camera2D& cam);

		// Called when the paddle has been created
		void created() override;
		
		// Update the paddle
		void virtual update();

		// Move the paddle in a direction
		void move(const MoveDirection direction);
	};
}

