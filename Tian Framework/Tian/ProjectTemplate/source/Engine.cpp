#include "Engine.h"
#include "ScreenMenu.h"
#include "ScreenGame.h"

namespace ProjectTemplate
{
	// Game params
	ui Engine::gPlayerAScore{ 0 };
	ui Engine::gPlayerBScore{ 0 };
	Box Engine::gGameBounds{ { 0, 0, 320, 240 } };
	GameAudio Engine::gAudio{};

	void Engine::loadAssets()
	{
		// Load the assts into the shared manager used by the engine loader
		loadAsset(new Font{ GFXFont, 12 });
		loadAsset(new TextureAnimated{ GFXSheet, mEngine.getRenderer() });
	}
	void Engine::registerScreens()
	{
		// Register screens with the screen manager used by the loader. First in first present.
		addScreen<ScreenMenu>();
		addScreen<ScreenGame>();
	}
	void Engine::loadParams()
	{
		// Load any properties 
		try
		{
			FileIO file{ "assets\\engine" };
			file.start();
			mWindowWidth = file.read<us>();
			mWindowHeight = file.read<us>();
			mName = file.read<str>();
			file.end();
		}
		catch (Error& e)
		{
			e.print();
		}
	}
}
