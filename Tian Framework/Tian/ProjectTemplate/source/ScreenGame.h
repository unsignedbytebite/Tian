#pragma once
#include "Engine/GameWorld.h"

namespace ProjectTemplate
{
	class ScreenGame : public Screen2D
	{
	public:
		ScreenGame(ScreenManager* manager, AssetManager* assets = NULL);

		// Inherited via Screen2D
		virtual void load() override;
		virtual void update() override;
		virtual void reset() override;
		virtual void render() override;
		virtual void free() override;

	private:
		// The camera types
		enum Cameras
		{
			WORLD = 0, DEBUGCAM
		};

		// The game world
		shared_ptr<GameWorld> mWorld;

		// Spritefont label for the score
		SpriteFont* mFontScore;
	};
}

