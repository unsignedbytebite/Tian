#pragma once
#include "../../Tian/source/Tian.h"
#include "Engine/GameAudio.h"

// Define a dev mode allows for toggle of debug messages etc
#define DEV_MODE

using namespace Tian;

namespace ProjectTemplate
{
	// Paths to shared assets
	static const char* GFXFont = "assets\\vga850.fon";
	static const char* GFXSheet = "assets\\spriteSheet.gfx";

	// Player keybindings
	const char KEY_LEFT = 'a';
	const char KEY_RIGHT = 'd';

	class Engine : public Loader
	{
	public:
		Engine() = default;

		// Inherited via Loader
		void registerScreens() override;
		void loadAssets() override;
		void loadParams() override;

		// The positon of the actors
		enum ActorPosition
		{
			TOP, BOTTOM, MIDDLE
		};

		// The sprite sheet sets
		enum SpriteSheetSet
		{
			PADDLE = 0, WALL, BALL
		};

		// The game types
		enum GameTypes
		{
			TYPE_PADDLE = 0, TYPE_WALL, TYPE_BALL
		};

		// Game params
		static ui gPlayerAScore, gPlayerBScore;

		// The game bounds
		static Box gGameBounds;

		// The game audio engine
		static GameAudio gAudio;
	};
}

