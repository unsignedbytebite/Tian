#include "ScreenMenu.h"
#include "Engine.h"

namespace ProjectTemplate
{
	ScreenMenu::ScreenMenu(ScreenManager* manager, AssetManager* assets) : Screen2D(manager, assets)
	{}
	void ScreenMenu::preload()
	{}
	void ScreenMenu::load()
	{
		// Set the camera scale to be 4 times to make things big
		camera().setScale({ 4, 4 });

		// Add the selection values to the buffer
		mSelection.add({ OPTION1, OPTION2, OPTION3 });

		// Create new labels 
		for (uc i = 0; i < COUNT; i++)
		{
			// Create a new spritefont using the loaded font and store in cache
			auto& spriteFont = addObject(new SpriteFont{ Engine::get->getAsset<Font>(ProjectTemplate::GFXFont) });
			mFonts.push_back(&spriteFont);
		}

		// Setup the labels
		auto line = 0;
		mFonts[MENU]->setString("MENU", { mLeft, mTop }, Colours::CornflowerBlue);
		mFonts[OPTION1]->setString("Play", { mLeft + mMargin, mTop + mGap * ++line });
		mFonts[OPTION2]->setString("Option", { mLeft + mMargin, mTop + mGap * ++line });
		mFonts[OPTION3]->setString("Exit", { mLeft + mMargin, mTop + mGap * ++line });
		mFonts[PICKER]->setString(">", { mLeft + mMargin - 10, mTop + mGap }, Colours::OrangeRed);

		pickerColour();
	}
	void ScreenMenu::postload()
	{}
	void ScreenMenu::update()
	{
		// Control the navigation selection
		if (getInput()->isKeytap('w'))
		{
			--mSelection;
			pickerColour();
		}
		else if (getInput()->isKeytap('s'))
		{
			++mSelection;
			pickerColour();
		}
		else if (getInput()->isKeytap(SDLK_SPACE))
		{
			// Process navigation selection
			navigationControl();
		}
	}
	void ScreenMenu::free()
	{
		// On screen change the previous screen is freed
		mFonts.clear();
		mSelection.clear();
		mObjs.clear();
	}
	void ScreenMenu::reset()
	{}
	void ScreenMenu::navigationControl()
	{
		// Navigation option slection
		switch (mSelection.get())
		{
		case OPTION1:
			mManager->switchScreen(1, true);
			break;
		case OPTION2:
#ifdef DEV_MODE
			Log::p("This is an option...");
#endif
			break;
		case OPTION3:
			mManager->close();
			break;
		}
	}
	void ScreenMenu::pickerColour()
	{
		// Set colours
		mFonts[OPTION1]->font().setColour(Colours::Gray);
		mFonts[OPTION2]->font().setColour(Colours::Gray);
		mFonts[OPTION3]->font().setColour(Colours::Gray);

		// Set the selction picker colour
		mFonts[PICKER]->local().y = mFonts[mSelection.get()]->getLocal().y;
		mFonts[mSelection.get()]->font().setColour(mFonts[PICKER]->font().getColour());
	}
}
