#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

class TianPacker
{
public:

	// Pack a binary file
	virtual void pack(const string structure, const string inPath, const string outPath);

	// Unpack a binary file
	virtual void unpack(const string structure, const string inPath, const string outPath);
};

