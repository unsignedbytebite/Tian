#include "TianPacker.h"

void TianPacker::pack(const string structure, const string inPath, const string outPath)
{
	FileIO outFile{ outPath, FileIO::WRITE };

	string read = "";
	string inValues = FileIO::read(inPath);

	const auto structureSplit = split(structure, ":", "\n\r \t", "#");
	const auto valuesSplit = split(inValues, ":", "\n\r\t", "#");

	// Begin ouptut
	outFile.start();
	for (unsigned long i = 0; i < structureSplit.size() && i < valuesSplit.size(); i++)
	{
		// Check each value type
		const auto& element = structureSplit[i];
		const auto& value = valuesSplit[i];

		if (element.size() == 0 || value.size() == 0)
		{
			continue;
		}
		else if (element == "uc")
		{
			const uc out = (uc)stoi(value);
			outFile.write(out);
		}
		else if (element == "us")
		{
			const us out = (us)stoi(value);
			outFile.write(out);
		}
		else if (element == "ui")
		{
			const ui out = stoi(value);
			outFile.write(out);
		}
		else if (element == "ul")
		{
			const ul out = stoi(value);
			outFile.write(out);
		}
		else if (element == "c")
		{
			const char out = (char)stoi(value);
			outFile.write(out);
		}
		else if (element == "s")
		{
			const short out = (short)stoi(value);
			outFile.write(out);
		}
		else if (element == "i")
		{
			const int out = stoi(value);
			outFile.write(out);
		}
		else if (element == "l")
		{
			const long out = stoi(value);
			outFile.write(out);
		}
		else if (element == "f")
		{
			const float out = stof(value);
			outFile.write(out);
		}
		else if (element == "d")
		{
			const double out = stod(value);
			outFile.write(out);
		}
		else if (element == "str")
		{
			const str out = value;
			outFile.write(out);
		}
		else if (element == "rect")
		{
			const auto valuesSplitSplit = split(value, ",", "\n\r \t", "#");
			const SDL_Rect out{ stoi(valuesSplitSplit[0]), stoi(valuesSplitSplit[1]), stoi(valuesSplitSplit[2]), stoi(valuesSplitSplit[3]) };
			outFile.write(out);
		}
		else if (element == "Vector2")
		{
			const auto valuesSplitSplit = split(value, ",", "\n\r \t", "#");
			const Vector2 out{ stof(valuesSplitSplit[0]), stof(valuesSplitSplit[1]) };
			outFile.write(out);
		}
		else if (element == "counter<uc>")
		{
			const auto valuesSplitSplit = split(value, ",", "\n\r \t", "#");
			const Counter<uc> out{ 
				(uc)stoi(valuesSplitSplit[0]),
				(uc)stoi(valuesSplitSplit[1]), 
				(uc)stoi(valuesSplitSplit[2]),
				(uc)stoi(valuesSplitSplit[3]),
				(uc)stoi(valuesSplitSplit[4]),
				(uc)stoi(valuesSplitSplit[5])
			};
			outFile.write(out);
		}
	}

	outFile.end();
}

void TianPacker::unpack(const string structure, const string inPath, const string outPath)
{
	FileIO outFile{ outPath, FileIO::WRITE };
	FileIO inFile(inPath);

	string read = "";

	const auto structureSplit = split(structure, ":", "\n\r \t", "#");

	// Begin ouptut
	outFile.start();
	inFile.start();
	for (unsigned long i = 0; i < structureSplit.size(); i++)
	{
		// Check each value type
		const auto& element = structureSplit[i];

		if (element.size() == 0)
		{
			continue;
		}
		else if (element == "uc")
		{
			const auto in = inFile.read<uc>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "us")
		{
			const auto in = inFile.read<us>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "ui")
		{
			const auto in = inFile.read<ui>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "ul")
		{
			const auto in = inFile.read<ul>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "c")
		{
			const auto in = inFile.read<uc>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "s")
		{
			const auto in = inFile.read<short>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "i")
		{
			const auto in = inFile.read<int>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "l")
		{
			const auto in = inFile.read<long>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "f")
		{
			const auto in = inFile.read<float>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "d")
		{
			const auto in = inFile.read<double>();
			const str stringOut = to_string(in) + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "str")
		{
			const auto in = inFile.read<str>();
			const str stringOut = in + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "rect")
		{
			const auto in = inFile.read<SDL_Rect>();
			const str stringOut(
				to_string(in.x) + "," +
				to_string(in.y) + "," +
				to_string(in.w) + "," +
				to_string(in.h) + "," +
				":");
			outFile.writeString(stringOut);
		}
		else if (element == "Vector2")
		{
			const auto in = inFile.read<Vector2>();
			const str stringOut = in.getString() + ":";
			outFile.writeString(stringOut);
		}
		else if (element == "counter<uc>")
		{
			const auto in = inFile.read<Counter<uc>>();
			const str stringOut = in.getString() + ":";
			outFile.writeString(stringOut);
		}
	}

	outFile.end();
	inFile.end();
}
