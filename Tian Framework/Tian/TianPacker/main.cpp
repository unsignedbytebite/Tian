﻿#include "TianPacker.h"

//-p testStructure.txt packIn.txt data.params
//-u testStructure.txt data.params packOut.txt
int main(int argc, char *args[])
{
	// Test correct input
	if (argc < 4)
	{
		cout << "Not enough arguments" << endl;
		cout << "[-u OR -p] [format style path] [export path]" << endl;
		return 1;
	}

	// Get the arguments
	const string command = args[1];
	const string pathStructure = args[2];
	const string pathIn = args[3];
	const string pathOut = args[4];

	// Get the structure
	string structure = FileIO::read(pathStructure);

	if (structure.size() == 0)
	{
		cout << "No structure to use" << endl;
		cout << "[-u OR -p] [format style path] [export path]" << endl;
		return 1;
	}

	TianPacker packer;

	// Check for valid flag
	if (command == "-u")
	{
		packer.unpack(structure, pathIn, pathOut);
	}
	else if (command == "-p")
	{
		packer.pack(structure, pathIn, pathOut);
	}
	else
	{
		cout << "Incorrect flags" << endl;
		cout << "[-u OR -p] [format style path] [export path]" << endl;
	}

	cout << "Done " << pathOut << endl;

	return 0;
}