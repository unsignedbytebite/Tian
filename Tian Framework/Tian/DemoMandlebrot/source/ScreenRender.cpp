#include "ScreenRender.h"
#include "Engine.h"

namespace Mandlebrot
{
	ScreenRender::ScreenRender(ScreenManager* manager, AssetManager* assets) : Screen2D(manager, assets), mOffset{ 0, 0 }, mScale{ 1.0f }
	{}
	void ScreenRender::load()
	{
		// Create a preview camera
		this->createCamera(new Camera2D(camera().renderer(), (us)mFramePrevDimension.x, (us)mFramePrevDimension.y));
		camera(1).setScale({ 8.f, 8.f });

		// Setup the render targets
		mCanvas = unique_ptr<Canvas>(new Canvas{ &camera(), (us)mFrameDimesion.x, (us)mFrameDimesion.y });
		mCanvasPrev = unique_ptr<Canvas>(new Canvas{ &camera(1), (us)mFramePrevDimension.x, (us)mFramePrevDimension.y });

		// Intial render
		renderToTarget(*mCanvasPrev, mFramePrevDimension, (us)mPrevIterations, (us)mFramePrevDimension.y, 0);
	}
	void ScreenRender::update()
	{
		// Update navigation
		const auto scaleVel = 0.01f * mScale;
		const auto moveVel = 0.1f * mScale;

		if (getInput()->isKeytap(SDLK_ESCAPE))
		{
			mManager->close();
		}
		else if (getInput()->isKeydown('w'))
		{
			move({ 0, -moveVel });
		}
		else if (getInput()->isKeydown('s'))
		{
			move({ 0, moveVel });
		}
		else if (getInput()->isKeydown('d'))
		{
			move({ moveVel, 0 });
		}
		else if (getInput()->isKeydown('a'))
		{
			move({ -moveVel, 0 });
		}
		else if (getInput()->isKeydown('q'))
		{
			moveScale(-scaleVel);
		}
		else if (getInput()->isKeydown('e'))
		{
			moveScale(scaleVel);
		}
		// Bookmarks
		else if (getInput()->isKeydown('1'))
		{
			mScale = 0.000434611196f;
			mOffset = { -0.201113626f, 0.684592187f };
			mMode = PREVIEW;
			renderToTarget(*mCanvasPrev, mFramePrevDimension, mPrevIterations, (us)mFramePrevDimension.y, 0);
		}
		else if (getInput()->isKeydown('2'))
		{
			mScale = 7.38393064e-05f;
			mOffset = { 0.331459969f, -0.510328054f };
			mMode = PREVIEW;
			renderToTarget(*mCanvasPrev, mFramePrevDimension, mPrevIterations, (us)mFramePrevDimension.y, 0);
		}
		// Reset view on space
		else if (getInput()->isKeytap(SDLK_SPACE))
		{
			mScale = 1;
			mOffset = { 0, 0 };
			mMode = PREVIEW;
			renderToTarget(*mCanvasPrev, mFramePrevDimension, mPrevIterations, (us)mFramePrevDimension.y, 0);
		}
		// If not moving and in preview mode then switch to master mode
		else if (mMode == PREVIEW)
		{
			mMode = MASTER_PROCESS;

			// Flush the master buffer
			mScanlineY = 0;
			mCanvas->clear();
		}
	}
	void ScreenRender::render()
	{
		// Scanline render
		if (mMode == MASTER_PROCESS)
		{
			// Render a chunk of the image
			const us renderSteps = 32;
			renderToTarget(*mCanvas, mFrameDimesion, mIterations, renderSteps, mScanlineY);

			// Advance the scanline
			mScanlineY += renderSteps;

			// Check for scanline complete
			mMode = (mScanlineY >= mFrameDimesion.y) ? MASTER_COMPLETE : mMode;
		}

		// Only render preview in preview mode
		if (mMode == PREVIEW)
		{
			camera(1).expose();
			mCanvasPrev->draw();
			camera(1).present();
		}
		// Render the master image over preview
		else
		{
			camera(1).expose();
			mCanvasPrev->draw();
			camera(1).present();

			camera().expose();
			mCanvas->draw();
			camera().present();
		}
	}
	void ScreenRender::move(const Vector2& vel)
	{
		mOffset += vel;
		mMode = PREVIEW;
		renderToTarget(*mCanvasPrev, mFramePrevDimension, mPrevIterations, (us)mFramePrevDimension.y, 0);
	}
	void ScreenRender::moveScale(const float vel)
	{
		mScale += vel;
		mMode = PREVIEW;
		renderToTarget(*mCanvasPrev, mFramePrevDimension, mPrevIterations, (us)mFramePrevDimension.y, 0);
	}
	Vector2 ScreenRender::mapWorldSpace(const Point& pixelPoint, const Point& frameDimension, const Complex<float>& min, const Complex<float>& max)
	{
		const auto realRange = (max.real() - min.real());
		const auto imaginaryRange = (max.imaginary() - min.imaginary());

		return {
			pixelPoint.x * (realRange / frameDimension.x) + min.real(),
			pixelPoint.y * (imaginaryRange / frameDimension.y) + min.imaginary()
		};
	}
	us ScreenRender::findMandlebrot(const Vector2& point, const us maxInteration)
	{
		// Check for escape iteration
		Complex<float> z;
		us i = 0;
		for (i = 0; i < maxInteration && z.realSq() + z.imaginarySq() < 4.0f; ++i)
		{
			z = {
				z.realSq() - z.imaginarySq() + point.x,
				2.0f * z.real() * z.imaginary() + point.y
			};
		}

		return i;
	}
	void ScreenRender::renderToTarget(Canvas& canvas, const Point& frame, const us iterations, const us lineProcess, const us renderYOffset)
	{
		// Bounds of the fractal
		const Complex<float> max{ 0.7f, 1.0f }, min{ -1.5f, -1.0f };

		// Expose the renderering to a canvas
		canvas.startExposure();
		Point pixel;
		for (pixel.y = renderYOffset; pixel.y < lineProcess + renderYOffset; ++pixel.y)
		{
			for (pixel.x = 0; pixel.x < frame.x; ++pixel.x)
			{
				// Get the pixel at a location in the set
				const auto calcultedWorldSpace = mapWorldSpace(pixel, frame, min * mScale, max * mScale) + mOffset;
				const auto n = findMandlebrot(calcultedWorldSpace, iterations);

				// Draw pixel with colour from gradient
				camera().drawPrimitive(pixel, mGrad[n % mGradSize]);
			}
		}
		canvas.stopExposure();
	}
}
