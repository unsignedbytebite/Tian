#include "Engine.h"
#include "ScreenRender.h"

namespace Mandlebrot
{
	void Engine::loadAssets()
	{}
	void Engine::registerScreens()
	{
		// Register screens with the screen manager used by the loader
		addScreen<ScreenRender>();
	}
	void Engine::loadParams()
	{
		mWindowWidth = mWindowHeight = 1024;
		mName = "DemoMandlebrot";
	}
}
