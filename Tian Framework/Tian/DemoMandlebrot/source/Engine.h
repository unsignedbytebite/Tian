#pragma once
#include "../../Tian/source/Tian.h"

using namespace Tian;

namespace Mandlebrot
{
	class Engine : public Loader 
	{
	public:
		Engine() = default;

		// Inherited via Loader
		void registerScreens() override;
		void loadAssets() override;
		void loadParams() override;
	};
}

