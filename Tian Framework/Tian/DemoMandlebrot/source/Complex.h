#pragma once

namespace Mandlebrot
{
	template<typename T>
	class Complex
	{
	public:
		Complex(const T real = 0, const T imaginary = 0) : mR{ real }, mI{ imaginary }
		{}

		Complex<T> operator*(const T val) const
		{
			return { mR * val, mI * val };
		}

		T realSq() const
		{
			return mR * mR;
		}

		T imaginarySq() const
		{
			return  mI * mI;
		}

		T real() const
		{
			return mR;
		}

		T imaginary() const
		{
			return  mI;
		}
	private:
		T mR;
		T mI;
	};
}
