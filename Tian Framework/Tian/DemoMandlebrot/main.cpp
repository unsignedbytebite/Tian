﻿#include "source/Engine.h"

int main(int, char*[])
{
	// Create and run the engine
	Mandlebrot::Engine engine;
	try
	{
		engine.run();
	}
	catch (Error& e)
	{
		e.print();
		Log::pause();
	}
	return 0;
} 
