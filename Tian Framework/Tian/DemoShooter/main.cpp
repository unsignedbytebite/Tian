﻿#include "source/Engine.h"

int main(int, char *[])
{
#ifdef DEV_MODE
	// Run the baking script for assets and params
	system("scripts\\bake.bat");
#endif

	// Create and run the engine
	DemoShooter::Engine engine;
	try
	{
		engine.run();
	}
	catch (Error& e)
	{
		e.print();
		Log::pause();
	}
	return 0;
} 
