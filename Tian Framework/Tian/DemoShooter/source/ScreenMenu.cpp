#include "ScreenMenu.h"
#include "Engine.h"

namespace DemoShooter
{
	ScreenMenu::ScreenMenu(ScreenManager* manager, AssetManager* assets) : Screen2D(manager, assets)
	{}
	ScreenMenu::~ScreenMenu()
	{}
	void ScreenMenu::preload()
	{}
	void ScreenMenu::load()
	{
		// Set the camera scale to be 4 times larger
		camera().setScale({ 4, 4 });

		// Add the selction values to the buffer
		mSelection.add({ OPTION1, OPTION2, OPTION3 });

		// Create new labels with font[0] from the assets
		for (auto i = 0; i < COUNT; i++)
		{
			// Create a new spritefont using the loaded font and store in cache
			auto& spriteFont = addObject(new SpriteFont{ Engine::get->getAsset<Font>(DemoShooter::GFXFont) });
			mFonts.push_back(&spriteFont);
		}

		// Setup the labels
		auto line = 0;
		mFonts[MENU]->setString("MENU", { mLeft, mTop }, Colours::Tomato);
		mFonts[OPTION1]->setString("Play", { mLeft + mMargin, mTop + mGap * ++line });
		mFonts[OPTION2]->setString("Empty", { mLeft + mMargin, mTop + mGap * ++line });
		mFonts[OPTION3]->setString("Exit", { mLeft + mMargin, mTop + mGap * ++line });
		mFonts[PICKER]->setString(">", { mLeft + mMargin - 10, mTop + mGap }, Colours::HotPink);

		pickerColour();
	}
	void ScreenMenu::postload()
	{}
	void ScreenMenu::update()
	{
		// Control the navigation selection
		if (getInput()->isKeytap('w'))
		{
			--mSelection;
			pickerColour();
		}
		else if (getInput()->isKeytap('s'))
		{
			++mSelection;
			pickerColour();
		}
		else if (getInput()->isKeytap(SDLK_SPACE))
		{
			navigationControl();
		}
	}
	void ScreenMenu::free()
	{
		// On screen change the previous screen is freed
		mFonts.clear();
		mSelection.clear();
		mObjs.clear();
	}
	void ScreenMenu::reset()
	{}
	void ScreenMenu::navigationControl()
	{
		// Selection action logic
		switch (mSelection.get())
		{
		case OPTION1:
			mManager->switchScreen(1, true);
			break;
		case OPTION2:
#ifdef DEV_MODE
			Log::p("This is an option...");
#endif
			break;
		case OPTION3:
			mManager->close();
			break;
		}
	}
	void ScreenMenu::pickerColour()
	{
		// Set colours
		mFonts[OPTION1]->font().setColour(Colours::Gray);
		mFonts[OPTION2]->font().setColour(Colours::Gray);
		mFonts[OPTION3]->font().setColour(Colours::Gray);

		// Set the selction picker colour
		mFonts[PICKER]->local().y = mFonts[mSelection.get()]->getLocal().y;
		mFonts[mSelection.get()]->font().setColour(mFonts[PICKER]->font().getColour());
	}
}
