#pragma once
#include "../../Tian/source/Tian.h"

// Define a dev mode allows for toggle of debug messages etc
#define DEV_MODE

using namespace Tian;

namespace DemoShooter
{
	// Paths to shared assets
	static const char* GFXFont = "assets\\vga850.fon";
	static const char* GFXPlayer = "assets\\player.gfx";
	static const char* GFXBullet = "assets\\bullet.gfx";
	static const char* GFXCharger = "assets\\bad_runner.gfx";
	static const char* GFXShotgun = "assets\\bad_shotgun.gfx";
	static const char* GFXBomber = "assets\\bad_bomber.gfx";

	// The bit codes for game types used
	enum GameTypes
	{
		NONE = 0,
		PLAYER = 1,
		WALL = 2,
		PLAYER_BULLET = 4,
		ENEMY_BULLET = 8,
		CAMERA = 16,
		TRIGGER = 32,
		ENEMY = 64,
		ALL = 127
	};

	class Engine : public Loader
	{
	public:
		Engine() = default;

		// Inherited via Loader
		void registerScreens() override;
		void loadAssets() override;
		void loadParams() override;

		// Game params
		static ui gScore;
	};
}

