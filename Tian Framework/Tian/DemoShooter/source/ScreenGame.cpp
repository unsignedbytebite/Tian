#include "ScreenGame.h"
#include "Engine.h"

namespace DemoShooter
{
	ScreenGame::ScreenGame(ScreenManager* manager, AssetManager* assets) : Screen2D(manager, assets)
	{}
	ScreenGame::~ScreenGame()
	{}
	void ScreenGame::load()
	{
		// Create an additonal camera for the HUD
		createCamera(new Camera2D{ camera() });

		// This main camera is for the world graphics
		camera(WORLD).setScale({ 2.0f, 2.0f });

		// Reset the score to zero
		Engine::gScore = 0;

		// Create the world
		mWorld = shared_ptr<GameWorld>(new GameWorld());
		mWorld->create(getInput(), camera(WORLD));

#ifdef DEV_MODE
		// Set the debug renderer
		ConBox::Log::p("IJKL = Move camera");
		mWorld->createDebugRenderer(new Box2DebugDraw(camera(HUD)));
#endif

		// Set the spritefont for the score
		mFontScore = &addObject(new SpriteFont{ *mAssets, 0 });
		mFontScore->setCamera(&camera(HUD));
	}
	void ScreenGame::update()
	{
#ifdef DEV_MODE
		// Update the debug camera controls
		updateDebugCam();
#endif
		// Escape to the main menu
		if (getInput()->isKeytap(SDLK_ESCAPE))
		{
			mManager->switchScreen(0, true);
		}

		// Update the world
		mWorld->update();
	}
	void ScreenGame::reset()
	{
		//TODO: proper reset
		camera().setLocal({ 0, 0 });
		mObjs.clear();
	}
	void ScreenGame::render()
	{
		// Use the world camera to capture the world draw
		camera(WORLD).expose();
		mWorld->draw();
		camera(WORLD).present();

		// Use the hud camera to campture the text
		camera(HUD).expose();
		mFontScore->setString("Score: " + to_string(Engine::gScore), { 2, 224 });
#ifdef DEV_MODE
		mWorld->drawDebug();
#endif
		camera(HUD).present();

		Screen2D::render();
	}
	void ScreenGame::free()
	{}
	void ScreenGame::updateDebugCam()
	{
		// Debug camera controls
		auto camVel = Vector2{ 0, 0 };
		const auto camSpeed = 0.25f;
		if (getInput()->isKeydown('i'))
		{
			camVel.y -= camSpeed;
		}
		else if (getInput()->isKeydown('k'))
		{
			camVel.y += camSpeed;
		}
		if (getInput()->isKeydown('j'))
		{
			camVel.x -= camSpeed;
		}
		else if (getInput()->isKeydown('l'))
		{
			camVel.x += camSpeed;
		}
		camera().body()->ApplyLinearImpulseToCenter(camVel, false);
	}
}
