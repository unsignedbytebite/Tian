#include "Engine.h"
#include "ScreenMenu.h"
#include "ScreenGame.h"

namespace DemoShooter
{
	// Game params
	ui Engine::gScore{ 0 };

	void Engine::loadAssets()
	{
		// Load the assts into the shared manager used by the engine loader
		loadAsset(new Font{ GFXFont, 10 });
		loadAsset(new TextureAnimated{ GFXPlayer, mEngine.getRenderer() });
		loadAsset(new TextureAnimated{ GFXBullet, mEngine.getRenderer() });
		loadAsset(new TextureAnimated{ GFXBomber, mEngine.getRenderer() });
		loadAsset(new TextureAnimated{ GFXCharger, mEngine.getRenderer() });
		loadAsset(new TextureAnimated{ GFXShotgun, mEngine.getRenderer() });
	}
	void Engine::registerScreens()
	{
		// Register screens with the screen manager used by the loader
		addScreen<ScreenMenu>();	
		addScreen<ScreenGame>();
	}
	void Engine::loadParams()
	{
		// Load any properties
		try
		{
			FileIO file{ "assets\\engine" };
			file.start();
			mWindowWidth = file.read<us>();
			mWindowHeight = file.read<us>();
			mName = file.read<str>();
			file.end();
		}
		catch (Error& e)
		{
			e.print();
		}
	}
}
