#pragma once
#include "../Engine.h"
#include "BulletManager.h"

namespace DemoShooter
{
	class GameWorld :
		public World
	{
	public:
		GameWorld();

		// Create the world
		void create(Input* inputController, Camera2D& cam);

		// Update the world
		void update();

		// Draw the world
		void draw();
	protected:
		// World units to viewport pixels
		const us mUnitsToPixels = 8;

		// The actor list
		vector<Actor*> mDrawList;

		// Layer collection of canvases
		vector<shared_ptr<Canvas>> mLayers;

		// The current manager
		BulletManager mBulletManager;

		// Handle to the camera
		Camera2D* mCam;
	};
}

