#pragma once
#include "Ship.h"

namespace DemoShooter
{
	class Player :
		public Ship
	{
	public:
		Player(Input* input, Camera2D& cam);

		// Inherited functions
		void created() override;
		virtual void eventInBounds() override;
	private:
		// Input controller
		Input* mInput;
	};
}

