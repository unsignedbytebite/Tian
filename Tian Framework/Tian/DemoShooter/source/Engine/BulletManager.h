#pragma once
#include "../Engine.h"
#include "Bullet.h"

namespace DemoShooter
{
	class BulletManager
	{
	public:
		BulletManager();
		~BulletManager();

		// Add a handle to bullet to the managment buffer
		void add(Bullet* bullet);

		// Launch a bullet
		void launch(const b2Vec2& positon, const b2Vec2& impluse, const GameTypes owner, const GameTypes targets);
	private:
		// Handles of all the created bullets in the world
		Buffer<Bullet*> mBullets;
	};
}

