#pragma once
#include "Ship.h"

namespace DemoShooter
{
	class Bomber :
		public Ship
	{
	public:
		Bomber(Camera2D& cam);
		~Bomber();

		// Inherited functions
		void eventInBounds();
		void created() override;
	private:
		// Think counter to take action
		Counter<uc> mThink;
	};
}

