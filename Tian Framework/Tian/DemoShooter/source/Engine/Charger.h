#pragma once
#include "Ship.h"

namespace DemoShooter
{
	class Charger :
		public Ship
	{
	public:
		Charger(Camera2D& cam);
		~Charger();

		// Inherited functions
		void eventInBounds();
		void created() override;
	};
}

