#include "Ship.h"

namespace DemoShooter
{
	Ship::Ship(Camera2D& cam, const us id, const TextureAnimated texture, const Vector2 gunPoint) : GameActor(cam, id, texture),
		mLaunchDelay(18, 16, 1, 0, 19, 0),
		mBurstDelay(150, 100, 1, 0, 200, 0),
		mGunPoint(gunPoint)
	{}
	Ship::~Ship()
	{}
	void Ship::fire(const GameTypes owner, const GameTypes targets, const Vector2 impluse)
	{
		mBulletManager->launch(
		{ (mBody->GetPosition().x + mGunPoint.x),
			(mBody->GetPosition().y + mGunPoint.y) },
			impluse, owner, targets);
	}
	void Ship::attachBulletManager(BulletManager & bulletManager)
	{
		mBulletManager = &bulletManager;
	}
	void Ship::hit()
	{
		mHP--;

		if (mHP <= 0)
		{
			kill();
		}

		mState |= TAKING_DAMAGE;
	}
	void Ship::kill()
	{
		b2Filter filter;
		filter.maskBits = NONE;
		filter.categoryBits = NONE;
		body()->GetFixtureList()->SetFilterData(filter);
		body()->SetAwake(false);
		body()->SetActive(false);
		mState = NONE;
	}
	void Ship::contactEvent(Object2D* objA, Object2D*)
	{
		// If contact with camera then inbounds
		if (objA->getID() == CAMERA)
		{
			mState |= INBOUNDS;
		}
		else
		{
			kill();
		}
	}
}
