#pragma once
#include "Ship.h"

namespace DemoShooter
{
	class Shotgun :
		public Ship
	{
	public:
		Shotgun(Camera2D& cam);
		~Shotgun();

		// Inherited functions
		void eventInBounds();
		void created() override;
	private:
		// Think counter to take action
		Counter<uc> mThink;
	};
}

