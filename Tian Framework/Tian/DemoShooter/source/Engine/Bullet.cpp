#include "Bullet.h"
#include "Ship.h"

namespace DemoShooter
{
	Bullet::Bullet(Camera2D& cam) : GameActor(cam, NONE, Engine::get->getAsset<TextureAnimated>(DemoShooter::GFXBullet))
	{
	}
	void Bullet::created()
	{
		body()->SetType(b2_dynamicBody);

		b2CircleShape shape;
		shape.m_radius = 0.1f;
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 10.8f;
		body()->CreateFixture(&fixtureDef);

		body()->GetFixtureList()->SetSensor(true);
		body()->SetAwake(false);
		body()->SetActive(false);
	}
	void Bullet::eventOutofBounds()
	{
		kill();
	}
	void Bullet::contactEvent(Object2D* objA, Object2D*)
	{
		if (objA->getID() == WALL)
			return;
		
     	if (objA->getID() != CAMERA)
		{
			auto& actor = *(Ship*)objA;
			kill();
			actor.hit();
		}
		else
		{
			// If contact with camera then inbounds
			mState |= INBOUNDS;
		}
	}
	void Bullet::fire(const b2Vec2& positon, const b2Vec2& impluse, const GameTypes owner, const GameTypes targets)
	{
		body()->SetTransform(positon, 0);
		body()->SetAwake(true);
		body()->SetActive(true);
		body()->SetLinearVelocity({ 0, 0 });
		body()->ApplyLinearImpulseToCenter(impluse, true);
		setID((us)owner);

		setFilter((us)owner, (us)(WALL | CAMERA | targets));

		mState |= INBOUNDS | ALIVE;
	}
	void Bullet::kill()
	{
		setID(NONE);
		body()->SetAwake(false);
		body()->SetActive(false);

		b2Filter filter;
		filter.categoryBits = NONE;
		filter.maskBits = NONE;
		body()->GetFixtureList()->SetFilterData(filter);
		mState = NONE;
	}
}
