#pragma once
#include "../Engine.h"

namespace DemoShooter
{
	class GameActor :
		public Actor
	{
	public:
		GameActor(Camera2D& cam, const us id, const TextureAnimated texture);
		~GameActor();

		// Update the actor
		virtual void update() override;

		// Draw the actor in the world
		virtual void drawInWorld(const float worldScale) override;

		// Move the actor by a vector
		virtual void move(const Vector2& dir) override;

		// Called when the GameActor is killed
		virtual void kill() = 0;

		// Event that occues when out of bounds
		virtual void eventOutofBounds();

		// Event occues when in bounds
		virtual void eventInBounds();
	protected:
		// The states of an GameActor
		enum State 
		{ 
			NONE = 0, 
			ALIVE = 2, 
			INBOUNDS = 4, 
			TAKING_DAMAGE = 8,
		};

		// The current state
		uc mState{ NONE };
	};
}

