#include "GameActor.h"

namespace DemoShooter
{
	GameActor::GameActor(Camera2D& cam, const us id, const TextureAnimated texture) : Actor(id, texture)
	{
		mState = ALIVE;
		mCam = &cam;
	}
	GameActor::~GameActor()
	{}
	void GameActor::update()
	{
		if (~mState & ALIVE)
			return;

		// Reset the inbounds state before testing if in bounds
		mState &= ~INBOUNDS;

		// Process all contacts including if in bounds of the camera
		Actor::processContacts();

		// Fire the bound state
		if (~mState & INBOUNDS)
		{
			eventOutofBounds();
		}
		else
		{
			eventInBounds();
		}
	}
	void GameActor::drawInWorld(const float worldScale)
	{
		if (~mState & ALIVE)
			return;

		// Turn red when taking damage
		if (mState & TAKING_DAMAGE)
		{
			mTex.textureData()->setColour(Tian::Colours::Red);
			mState &= ~TAKING_DAMAGE;
		}

		// Draw the textured sprite in the world
		Sprite::drawInWorld(worldScale);

		// Reset texture clour
		mTex.textureData()->setColour(Tian::Colours::White);
	}
	void GameActor::move(const Vector2& dir)
	{
		// Awake the body
		mBody->SetAwake(true);

		// Apply movment
		mBody->ApplyLinearImpulseToCenter({ dir.x, dir.y }, false);
	}
	void GameActor::eventOutofBounds()
	{}
	void GameActor::eventInBounds()
	{}
}
