#include "Bomber.h"
#include "../Engine.h"

namespace DemoShooter
{
	Bomber::Bomber(Camera2D& cam) :
		Ship(cam, ENEMY, Engine::get->getAsset<TextureAnimated>(DemoShooter::GFXBomber), { -1, 0.f })
	{
		mHP = 6;
		mLaunchDelay = { 18, 16, 1, 0, 23, 0 };
		mBurstDelay = { 60, 50, 1, 0, 240, 0 };
		mThink = { 100, 90, 1, 0, 250, 0 };
	}

	Bomber::~Bomber()
	{}

	void Bomber::eventInBounds()
	{
		move({ -0.05f, 0.f });

		// When ready to take action
		if (mThink.count())
		{
			const auto rads = (Maths::random<float>(2000) * 0.001f) * pi;
			move(Maths::radiansToVector2(rads) * 0.5f);
		}

		if (mBurstDelay.count())
		{
			if (mLaunchDelay.count())
			{
				// For on whole circle fire bullets
				for (float i = 0; i < 2.0f; i += 0.2f)
				{
					const float rads = i * pi;
					fire(ENEMY_BULLET, PLAYER, Maths::radiansToVector2(rads)*2.5f);
				}
			}
		}
	}
	void Bomber::created()
	{
		body()->SetType(b2_dynamicBody);
		body()->SetTransform({ 0, 0 }, 0);
		body()->SetLinearDamping(2);
		body()->SetAngularDamping(20);

		b2PolygonShape shape;
		shape.SetAsBox(0.8f, 0.8f);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.0f;
		body()->CreateFixture(&fixtureDef);

		setLocal({ -12, -4 });
		body()->GetFixtureList()->SetSensor(true);

		setFilter(ENEMY, WALL | PLAYER_BULLET | CAMERA);
	}
}
