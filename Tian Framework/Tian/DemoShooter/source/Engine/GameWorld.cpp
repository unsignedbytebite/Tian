#include "GameWorld.h"
#include "../Engine.h"
#include "Player.h"
#include "Charger.h"
#include "Bomber.h"
#include "Shotgun.h"

namespace DemoShooter
{
	GameWorld::GameWorld() : World()
	{}

	void GameWorld::create(Input* inputController, Camera2D& cam)
	{
		// Setup the world camera
		mCam = &cam;
		mCam->createBody(*mPhysWorld.get(), 320 / (float)mUnitsToPixels, 240 / (float)mUnitsToPixels);
		mCam->setID(CAMERA);

		// Set the camera collission filters
		mCam->setFilter(CAMERA, PLAYER | ENEMY | ENEMY_BULLET | PLAYER_BULLET);

		// Apply the camera movement
		mCam->body()->ApplyLinearImpulseToCenter({ 3.f, 0 }, false);
	
		// Create the bullet pool
		{
			for (us i = 0; i < 64; ++i)
			{
				auto& bullet = createObject<Bullet>(new Bullet(cam));
				mBulletManager.add(&bullet);
				mDrawList.push_back(&bullet);
			}
		}

		// Add players
		{
			auto& player = createObject<Player>(new Player(inputController, cam));
			player.attachBulletManager(mBulletManager);
			mDrawList.push_back(&player);
		}
		{
			auto& player = createObject<Player>(new Player(inputController, cam));
			player.attachBulletManager(mBulletManager);
			mDrawList.push_back(&player);

			// Push away from player 1
			player.body()->ApplyLinearImpulseToCenter({ -4.f, 4.2f }, false);
		}
		{
			auto& player = createObject<Player>(new Player(inputController, cam));
			player.attachBulletManager(mBulletManager);
			mDrawList.push_back(&player);

			// Push away from player 1
			player.body()->ApplyLinearImpulseToCenter({ -4.f, -4.2f }, false);
		}

		// Create the walls
		{
			// Wall parameters
			const float wallWidth = 4;
			const ui ungelation = 5;
			const float minHeight = 1;
			const float maxHeight = 30;
			const us levelLength = 96;

			// Create the background layers
			const us canvasChunkWidth = 512;
			const us canvasWidth = (us)((levelLength * wallWidth) * mUnitsToPixels);
			const us canvasDividesX = (canvasWidth / canvasChunkWidth);
			const us canvasDividesY = 1;
			const us layersCount = 1;

			for (us i = 0; i < layersCount; ++i)
			{
				auto* canvas = new Canvas{ &cam, canvasWidth, (us)(maxHeight * mUnitsToPixels), canvasDividesX, canvasDividesY };
				//canvas->fillWithCanvasTexture();
				mLayers.push_back(shared_ptr<Canvas>(canvas));
			}

			// Get the layer to paint the walls onto
			auto& logicLayer = *mLayers.at(0);

			// Create the walls
			for (us i = 0; i < levelLength; ++i)
			{
				// Get the current chunk on the canvas
				const auto x = (int)(i * wallWidth * mUnitsToPixels);
				const us chunkIndex = (us)x / canvasChunkWidth;

				// Start the canvas for exposure on the current chunk
				logicLayer.startExposure(chunkIndex, 0);
				{
					// Height of the wall
					const float height = minHeight + Maths::random<float>(ungelation);

					// The points of the wall
					Vector2Points points;
					points.push_back({ 0, 0 });
					points.push_back({ wallWidth, 0 });
					points.push_back({ wallWidth, height });
					points.push_back({ 0, height });
					
					// Create the wall
					auto& wall = createStatic(WALL, (float)i * wallWidth, 0.f, points);
					wall.setFilter(WALL);

					// Draw the wall graphics
					const auto y = 0;
					const auto w = (int)(wallWidth * mUnitsToPixels);
					const auto h = (int)(height * mUnitsToPixels);
					SDL_Rect r{ x % canvasChunkWidth, y, w, h };
					cam.drawPrimitiveBoxFill(r, { 110, 100, 110, 255 });
				}
				{
					// Height of the wall
					const float height = minHeight + Maths::random<float>(ungelation);

					// The points of the wall
					Vector2Points points;
					points.push_back({ 0, 0 });
					points.push_back({ wallWidth, 0 });
					points.push_back({ wallWidth, -height });
					points.push_back({ 0, -height });

					// Create the wall
					auto& wall = createStatic(WALL, (float)i * wallWidth, maxHeight, points);
					wall.setFilter(WALL);

					// Draw the wall graphics
					const auto y = maxHeight * mUnitsToPixels;
					const auto w = (int)(wallWidth * mUnitsToPixels);
					const auto h = -(int)(height * mUnitsToPixels);
					SDL_Rect r{ (int)(x % canvasChunkWidth), (int)y, w, h };
					cam.drawPrimitiveBoxFill(r, { 110, 100, 110, 255 });
				}

				// End exposing the canvas for drawing
				logicLayer.stopExposure(i / canvasChunkWidth, 0);
			}

			// Add in some bad guys
			for (us i = 8; i < levelLength; i += Maths::random<us>(4))
			{
				// Random select a badguy
				const auto enemyType = Maths::random<uc>(3);
				auto& enemy = 
					(enemyType == 0) ? createObject<Ship>(new Charger(cam)) :
					(enemyType == 1) ? createObject<Ship>(new Shotgun(cam)) :
					(enemyType == 2) ? createObject<Ship>(new Bomber(cam)) :
					createObject<Ship>(new Charger(cam));
				
				// Setup the badguy
				enemy.attachBulletManager(mBulletManager);
				enemy.setBodyLocation(i * wallWidth, (float)(Maths::random<us>(15) + 5));
				mDrawList.push_back(&enemy);
			}
		}
	}

	void GameWorld::update()
	{
		// Update actors
		for (auto& obj : mDrawList)
		{
			obj->update();
		}
		World::update();
	}

	void GameWorld::draw()
	{
		// Set the camera render position the same as the camera
		mCam->setLocalToBodyPosition(mUnitsToPixels);

		// Draw layers
		for (auto& obj : mLayers)
		{
			obj->draw();
		}

		// Draw all the objects in the world
		for (auto& obj : mDrawList)
		{
			obj->drawInWorld(mUnitsToPixels);
		}
	}
}
