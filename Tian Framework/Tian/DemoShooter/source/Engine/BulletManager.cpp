#include "BulletManager.h"

namespace DemoShooter
{
	BulletManager::BulletManager()
	{}
	BulletManager::~BulletManager()
	{}
	void BulletManager::add(Bullet* bullet)
	{
		mBullets.add(bullet);
	}
	void BulletManager::launch(const b2Vec2& positon, const b2Vec2& impluse, const GameTypes owner, const GameTypes targets)
	{
		auto& bullet = mBullets.get();
		bullet->fire(positon, impluse, owner, targets);
		++mBullets;
	}
}
