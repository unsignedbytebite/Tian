#pragma once
#include "../Engine.h"
#include "GameActor.h"

namespace DemoShooter
{
	class Bullet : public GameActor
	{
	public:
		Bullet(Camera2D& cam);

		// Inherited functions
		virtual void created() override;
		virtual void contactEvent(Object2D* objA, Object2D* objB) override;
		virtual void eventOutofBounds() override;
		virtual void kill() override;

		// Apply a launch impluse to the bullet
		virtual void fire(const b2Vec2& positon, const b2Vec2& impluse, const GameTypes owner, const GameTypes targets);
	};
}

