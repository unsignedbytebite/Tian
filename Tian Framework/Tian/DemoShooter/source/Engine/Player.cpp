#include "Player.h"
#include "../Engine.h"

namespace DemoShooter
{
	Player::Player(Input* input, Camera2D& cam) :
		Ship(cam, PLAYER,Engine::get->getAsset<TextureAnimated>(DemoShooter::GFXPlayer),	{ 1, 0.f }), mInput(input)
	{
		mHP = 1;
	}
	void Player::eventInBounds()
	{
		const auto vel = 0.5f;
		move({ 0.1f, 0.f });

		// Player input control
		if (mInput->isKeydown('w'))
		{
			move({ 0, -vel });
		}
		else if (mInput->isKeydown('s'))
		{
			move({ 0, vel });
		}

		if (mInput->isKeydown('a'))
		{
			move({ -vel, 0 });
		}
		else if (mInput->isKeydown('d'))
		{
			move({ vel, 0 });
		}

		// Space to fire!
		if (mInput->isKeydown(SDLK_SPACE))
		{
			if (mLaunchDelay.count())
			{
				fire(PLAYER_BULLET, ENEMY, { 5, 0 });
			}
		}

		// Increase the score whilst alive
		++Engine::gScore;
	}
	void Player::created()
	{
		body()->SetType(b2_dynamicBody);
		body()->SetTransform({ 5, 15 }, 0);
		body()->SetLinearDamping(2 + (Maths::random<float>(3) * 0.1f));

		b2PolygonShape shape;
		shape.SetAsBox(0.8f, 0.5f);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.0f;
		body()->CreateFixture(&fixtureDef);

		setLocal({ -12, -4 });
		body()->GetFixtureList()->SetSensor(true);
		body()->SetSleepingAllowed(false);

		setFilter(PLAYER, CAMERA | WALL | ENEMY_BULLET | ENEMY);
	}
}
