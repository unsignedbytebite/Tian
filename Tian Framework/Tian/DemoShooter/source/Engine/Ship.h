#pragma once
#include "../Engine.h"
#include "GameActor.h"
#include "BulletManager.h"

namespace DemoShooter
{
	class Ship :
		public GameActor
	{
	public:
		Ship(Camera2D& cam, const us id, const TextureAnimated texture, const Vector2 gunPoint = { 0, 0 });
		~Ship();
		
		// Fire a bullet
		virtual void fire(const GameTypes owner, const GameTypes targets, const Vector2 impluse);

		// Atttach a bullet managet to this ship
		void attachBulletManager(BulletManager& bulletManager);

		// Kill
		virtual void kill() override;

		// Triggered when hit
		virtual void hit();

		// Event action when contact with another event
		virtual void contactEvent(Object2D* objA, Object2D* objB);
	protected:
		// Fire launch delay counter
		Counter<uc> mLaunchDelay;

		// Fire burst delay counter
		Counter<uc> mBurstDelay;

		// Handle to the bullet manager
		BulletManager* mBulletManager;

		// Hitpoints
		uc mHP;

		// The location of the gun to fire bullets from
		Vector2 mGunPoint;
	};
}

