#include "Charger.h"
#include "../Engine.h"

namespace DemoShooter
{
	Charger::Charger(Camera2D& cam) :
		Ship(cam, ENEMY, Engine::get->getAsset<TextureAnimated>(DemoShooter::GFXCharger), { -1, 0.f })
	{
		mHP = 3;
	}

	Charger::~Charger()
	{}

	void Charger::eventInBounds()
	{
		move({ -0.25f, 0.f });

		// Fire bullets when counters are active
		if (mBurstDelay.count())
		{
			if (mLaunchDelay.count())
			{
				fire(ENEMY_BULLET, PLAYER, { -5, 0 });
			}
		}
	}
	void Charger::created()
	{
		body()->SetType(b2_dynamicBody);
		body()->SetTransform({ 0, 0 }, 0);
		body()->SetLinearDamping(2);
		body()->SetAngularDamping(20);

		b2PolygonShape shape;
		shape.SetAsBox(0.8f, 0.5f);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.0f;
		body()->CreateFixture(&fixtureDef);

		setLocal({ -12, -4 });
		body()->GetFixtureList()->SetSensor(true);

		setFilter(ENEMY, WALL | PLAYER_BULLET | CAMERA);
	}
}
