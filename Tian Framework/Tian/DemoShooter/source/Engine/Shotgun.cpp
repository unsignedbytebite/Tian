#include "Shotgun.h"
#include "../Engine.h"

namespace DemoShooter
{
	Shotgun::Shotgun(Camera2D& cam) :
		Ship(cam, ENEMY, Engine::get->getAsset<TextureAnimated>(DemoShooter::GFXShotgun), { -1, 0.f })
	{
		mHP = 4;
		mThink = { 100, 90, 1, 0, 150, 0 };
	}

	Shotgun::~Shotgun()
	{}

	void Shotgun::eventInBounds()
	{
		move({ -0.15f, 0.f });

		// When ready to take action
		if (mThink.count())
		{
			const auto rads = (Maths::random<float>(2000) * 0.001f) * pi;
			move(Maths::radiansToVector2(rads) * 1.5f);
		}

		// Fire bullets when counters are active
		if (mBurstDelay.count())
		{
			if (mLaunchDelay.count())
			{
				fire(ENEMY_BULLET, PLAYER, { -4, 0 });
				fire(ENEMY_BULLET, PLAYER, { -4, 0.5f });
				fire(ENEMY_BULLET, PLAYER, { -4, -0.5f });
			}
		}
	}
	void Shotgun::created()
	{
		body()->SetType(b2_dynamicBody);
		body()->SetTransform({ 0, 0 }, 0);
		body()->SetLinearDamping(2);
		body()->SetAngularDamping(20);

		b2PolygonShape shape;
		shape.SetAsBox(0.8f, 0.6f);

		b2FixtureDef fixtureDef;
		fixtureDef.shape = &shape;
		fixtureDef.density = 0.0f;
		body()->CreateFixture(&fixtureDef);

		setLocal({ -12, -4 });
		body()->GetFixtureList()->SetSensor(true);

		setFilter(ENEMY, WALL | PLAYER_BULLET | CAMERA);
	}
}
