@echo off

echo [ TianPacker ]
TianPacker.exe -p scripts\engine.structure scripts\engine.values assets\engine
TianPacker.exe -p scripts\player.structure scripts\player.values assets\player.gfx
TianPacker.exe -p scripts\bullet.structure scripts\bullet.values assets\bullet.gfx
TianPacker.exe -p scripts\bad_runner.structure scripts\bad_runner.values assets\bad_runner.gfx
TianPacker.exe -p scripts\bad_shotgun.structure scripts\bad_shotgun.values assets\bad_shotgun.gfx
TianPacker.exe -p scripts\bad_bomber.structure scripts\bad_bomber.values assets\bad_bomber.gfx
echo [ Bake Complete! ]

